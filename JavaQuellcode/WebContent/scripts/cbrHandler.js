/**
 * Created by Diana Lange.
 */

function callCBRServer(fct, servlet, data, callback) {
	fct({
		url: servlet,
        type: "post",
        data : data,
        dataType: "text",
        success: text => {
        	callback(JSON.parse(text));
        },
        error : error => callback(JSON.parse(error)),
	});

}
