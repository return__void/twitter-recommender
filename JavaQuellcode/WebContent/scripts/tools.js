/**
 * Created by Diana Lange.
 */

/**
 * Creates a div element which holds / displays four user twitter objects. These
 * user objects are the resulting recommendations.
 * 
 * @param users {
 *            	shared : { user : userObj, behavior : userObj, content : userObj, language : userObj },
 *            	clustering :  { user : userObj, behavior : userObj, content : userObj, language : userObj }
 *          }<br>
 *        	each userObj looks like this:{ 
 *        		user : twitterUserObject, 
 *        		similarity : float,
 *        		friends : int,
 *        		id : String
 *         }
 *         id is the twitter user id of the user, friends is the number of common friends, shared users are recommendations
 *         that are based on people that are often friends with existing friends, clusering users are recommendations,
 *         that are similar to features of the friends of the input user.
 * @returns The div element which can then be used to append the html page.
 <........*/ 
function createResult(users) {
	
	// users.shared.behavior.similarity
	// users.shared.behavior.id
	// users.shared.behavior.user
	// users.shared.behavior.friends

    let div = createElement('div').attr('class', 'container');
    let spacer = createElement('div').attr('class', 'spacer').append(createElement('span').append('&raquo;'));
    let ul = createElement('ul');
    
    let lis = [];

    // recommendations by clusters
    
    if (users.clustering.user.id != -1) {
    	 let li = createElement('li');
    	 let txt = users.clustering.user.user.name + " is similar to you.";
    	 createResultUser(li, users.clustering.user, txt);
    	 lis.push(li);
    }
    if (users.clustering.behavior.id != -1) {
    	let li = createElement('li');
    	let txt = users.clustering.behavior.user.name + " is similar to the behavior feature of your friends.";
    	createResultUser(li, users.clustering.behavior, txt);
    	lis.push(li);
    }
    if (users.clustering.language.id != -1) {
    	let li = createElement('li');
    	let txt = users.clustering.language.user.name + " is similar to the language feature of your friends.";
    	createResultUser(li, users.clustering.language, txt);
    	lis.push(li);
    }
    
    if (users.clustering.content.id != -1) {
    	let li = createElement('li');
    	let txt = users.clustering.content.user.name + " is similar to the content feature of your friends.";
    	createResultUser(li, users.clustering.content, txt);
    	lis.push(li);
    }
    
    // recommendations by shared friends
    
    if (users.shared.user.id != -1) {
	   	let li = createElement('li');
	   	let txt = users.shared.user.friends + " of your friends follow this user and " + users.shared.user.user.name + " is similar to you.";
	   	createResultUser(li, users.shared.user, txt);
	   	lis.push(li);
    }
    if (users.shared.behavior.id != -1) {
    	let li = createElement('li');
    	let txt = users.shared.behavior.friends + " of your friends follow this user and " + users.shared.behavior.user.name + " is similar to the behavior feature of your friends.";
    	createResultUser(li, users.shared.behavior, txt);
    	lis.push(li);
    }
    if (users.shared.language.id != -1) {
    	let li = createElement('li');
    	let txt = users.shared.language.friends + " of your friends follow this user and " + users.shared.language.user.name + " is similar to the language feature of your friends.";
    	createResultUser(li, users.shared.language, txt);
    	lis.push(li);
    }
   
    if (users.shared.content.id != -1) {
    	let li = createElement('li');
    	let txt = users.shared.content.friends + " of your friends follow this user and " + users.shared.content.user.name + " is similar to the content feature of your friends.";
    	createResultUser(li, users.shared.content, txt);
    	lis.push(li);
    }

    for (let i in lis) {
    	if (i < 4 && lis.length > 4) {
    		lis[i].attr("style", "padding-bottom: 0.4rem");
    	}
    	
    	ul.append(lis[i]);
    	if (i % 4 == 3) {
    		let br = createElement('br');
    		br.attr("style", "clear:both")
    		ul.append(br);
    	}
    }

    div.append(spacer);
    div.append(ul);

    return div;
}

/**
 * Appends a container with the information of a specific twitter user.
 * 
 * @param container
 *            An html element.
 * @param userWrapper {
 *            user : twitterUserObject, similarity : float }
 * @param info
 *            A text (String).
 */
function createResultUser(container, userWrapper, info) {
	let user = userWrapper.user;

    let imgURL = user.profile_image_url_https.replace(/_normal/i, "");
    let userName = "@" + user.screen_name + " (" + user.name + ")";
    let userURL = "https://twitter.com/" + user.screen_name;
    let status = (user.status.text ? user.status.text : user.status.full_text);
    let description = user.description;

    let img = createElement('img').attr('src', imgURL);
    let p = createElement('p');
    let b = createElement('b');
    let a = createElement('a').attr("href", userURL).attr("target", "_blank").append("Find " + user.name + " on Twitter >>");
    let i = createElement('i').append("Description: " + description + ".<br>Latest tweet: " + status);

    let txt = info + " The detected similarity is " + (userWrapper.similarity * 100).toFixed(2) + "%.";

    b.append(userName);
    p.append(b);
    p.append(txt);
    p.append(i);
    p.append(a);

    container.append(img);
    container.append(p);
}

/**
 * Takes an array and shuffles the elements (randomly reorders the elements).
 * 
 * @param array
 *            An array.
 * @returns The reordered array.
 */
function shuffle(array) {
    for (let i in array) {
        let j = Math.floor(Math.random() * array.length);

        let tmp = array[i];
        array[i] = array[j];
        array[j] = tmp;
    }

    return array;
}

/**
 * Creates a random sample of size n from the original array. Elements are
 * ordered randomly in returned array.
 * 
 * @param someArray
 *            An array.
 * @param n
 *            Size of the returned array.
 * @returns The new array with n elements or - if n > someArray.length -
 *          someArray.length elements.
 */
function randomSample(someArray, n) {

    if (n >= someArray.length) {
        let newArray = [];
        for (let key in someArray) {
            newArray.push(someArray[key]);
        }

        return shuffle(newArray);
    }

    // create random sample
    let entries = {};
    let sample = [];

    let count = 0;
    let j = 0;
    while (count < n && j < n + 100) {
        let i = Math.floor(Math.random() * someArray.length);


        if (!entries[i]) {
            entries[i] = someArray[i];
            count++;

        }
        j++;
    }

    for (let key in entries) {
        sample.push(entries[key]);
    }

    return sample;
}

/**
 * Create div element which holds / displays error(s).
 * 
 * @param errors
 *            An array with error objects.
 * @returns A div element which contains the error message(s).
 */
function userNotFoundError(errors) {

    let txt = [];
    for (let i in errors) {
        if (errors[i].message) {
            txt.push(errors[i].message);
        }
    }

    txt = "Error: " + txt.join(" ");

    let div = createElement('div').attr('class', 'container error');
    let p = createElement('p').append(txt);
    div.append(p);

    return div;
}

/**
 * Finds the top n elements in an array.
 * 
 * @param someArray
 *            An array with objects.
 * @param n
 *            (Max) number of elements which should be returned. If n >
 *            someArray.length, then someArray.length elements will be returned.
 * @param addFct
 *            (containerToStoreElement, arrayKey, arrayElementOfKey) Describes
 *            which information of each element of the array should be stored
 *            into the container.
 * @param compFct
 *            (container1, container2) Describes the order of the elements of
 *            the array. Should return true / false.
 * @param filterFct
 *            (container) Describes which information the returned array should
 *            have. Optional. If undefined, then all information that is set in
 *            addFct will be contained in the array.
 * @returns The new array.
 */
function getTop(someArray, n, addFct, compFct, filterFct) {

    // array which will be returned
    let top = [];

    for (let key in someArray) {

        // store information in current object
        let obj = {key: key};
        addFct(obj, key, someArray[key]);

        // add object to new array, sort array
        top.push(obj);
        top.sort(compFct);

        // remove last element if array is too long
        if (top.length > n) {
            top.splice(n, 1);
        }
    }

    // remove information when needed
    if (filterFct) {
        for (let i in top) {
            top[i] = filterFct(top[i]);
        }
    }

    return top;
}

/**
 * Creates an html element
 * 
 * @param type
 *            the html tag name, e.g. 'div'
 * @returns {*|jQuery|HTMLElement}
 */
function createElement(type) {
    return $(document.createElement(type));
}

/**
 * Creates an array which holds sentences of the input user's analysis.
 * 
 * @param analysis
 *            The analysis object of a user.
 * @param userName
 *            The name of the user.
 * @returns An array with texts about the behavior of the input user.
 */
function getBehaviorText(analysis, userName) {

    // get data
    let tweetsperday = analysis.behavior.general.tweetfrequency.latest.toFixed(2);
    let listed = analysis.behavior.general.listed;
    let friendsPerFollower = analysis.behavior.general.friendsPerFollower.toFixed(2);
    let doingFaves = analysis.behavior.interaction.favorites.toFixed(2);
    let doingRetweets = (100 * analysis.behavior.interaction.retweets.percentage).toFixed(2);
    let doingReplies = (100 * analysis.behavior.interaction.replies.percentage).toFixed(2);
    let doingAtMentions = (100 * analysis.behavior.interaction.atMentions.percentage).toFixed(2);
    let peopleCount = analysis.behavior.interaction.people.average.toFixed(2);
    let peopleRecurring = (100 * analysis.behavior.interaction.people.recurring).toFixed(2);
    let gettingRetweets = (100 * analysis.behavior.reaction.retweets.percentage).toFixed(2);
    let gettingRetweetsNum = analysis.behavior.reaction.retweets.average.toFixed(2);
    let gettingFavorites = (100 * analysis.behavior.reaction.favorites.percentage).toFixed(2);
    let gettingFavoritesNum = analysis.behavior.reaction.favorites.average.toFixed(2);

    // build array
    let behaviorText = [];
    behaviorText.push(userName + " creates " + tweetsperday + " tweets per day.");
    behaviorText.push(userName + " is listed " + listed + " times.");
    behaviorText.push(userName + " has " + friendsPerFollower + " friends for each follower.");
    behaviorText.push(userName + " marks " + doingFaves + " tweets per day as his / her favorite.");
    behaviorText.push(doingRetweets + "% of the tweets of " + userName + " are retweets.");
    behaviorText.push(doingReplies + "% of the tweets are replies to other tweets.");
    behaviorText.push(doingAtMentions + "% of the tweets contain at-mentions.");
    behaviorText.push(peopleCount + " people are mentioned in tweets with at-mentions in average.");
    behaviorText.push(peopleRecurring + "% of all mentioned people are mentioned more than once.");
    behaviorText.push(gettingRetweets + "% of the tweets of " + userName + " are getting retweeted.");
    behaviorText.push(gettingFavorites + "% of the tweets are favorites of other people.");
    behaviorText.push("Each tweet of " + userName + " is getting retweeted " + gettingRetweetsNum + " times in average.");
    behaviorText.push("Each tweet is marked as a favorite " + gettingFavoritesNum + " times in average.");
    return behaviorText;
}

/**
 * Creates an array which holds sentences of the input user's analysis.
 * 
 * @param analysis
 *            The analysis object of a user.
 * @param userName
 *            The name of the user.
 * @returns An array with texts about the language of the input user.
 */
function getLanguageText(analysis, userName) {

    // get data
    let styleEmoticon = (100 * analysis.language.style.emoticon).toFixed(2);
    let quotesEmoticon = (100 * analysis.language.style.quotes).toFixed(2);
    let styleQuestions = (100 * analysis.language.style.questions).toFixed(2);
    let styleExclamations = (100 * analysis.language.style.exclamations).toFixed(2);

    let tweetlengthEmpty = (100 * analysis.language.words.tweetlength.empty).toFixed(2);
    let tweetlengthShort = (100 * analysis.language.words.tweetlength.short).toFixed(2);
    let tweetlengthAvg = analysis.language.words.tweetlength.average.toFixed(2);

    let semanticsNouns = analysis.language.words.semantics.noun ? (100 * analysis.language.words.semantics.noun).toFixed(2) : 0;
    let semanticsAdjectives = analysis.language.words.semantics.adjective ? (100 * analysis.language.words.semantics.adjective).toFixed(2) : 0;
    let semanticsVerbs = analysis.language.words.semantics.verb ? (100 * analysis.language.words.semantics.verb).toFixed(2) : 0;
    let semanticsNumber = analysis.language.words.semantics.number ? (100 * analysis.language.words.semantics.number).toFixed(2) : 0;

    let hashtagCount = (100 * analysis.language.outline.hashtags.percentage).toFixed(2);
    let hashtagAvg = analysis.language.outline.hashtags.average.toFixed(2);

    let detectedLanguages = getTop(analysis.language.outline.languages, 2, (container, key, value) => container.p = value, (o, v) => o.p < v.p);

    let detectedLanguagesTxt = "";
    if (detectedLanguages.length > 0) {
        detectedLanguagesTxt += (100 * detectedLanguages[0].p).toFixed(2) + "% " + detectedLanguages[0].key;
    }
    if (detectedLanguages.length == 2) {
        detectedLanguagesTxt += ", " + (100 * detectedLanguages[1].p).toFixed(2) + "% " + detectedLanguages[1].key;
    }

    // build array
    let languageText = [];
    languageText.push(styleEmoticon + "% of the tweets of " + userName + " contain emoticon, " + quotesEmoticon + "% contain quotes, " + styleQuestions + "% contain questions and " + styleExclamations + "% contain exclamations.");
    languageText.push(tweetlengthEmpty + "% of the tweets have no textual content besides hashtags, at-mentions or urls.");
    languageText.push(tweetlengthShort + "% of the tweets are short (1 - 3 words).");
    languageText.push("The tweets consist of " + tweetlengthAvg + " words in average.");
    if ((semanticsNouns + semanticsAdjectives + semanticsVerbs + semanticsNumber) == 0) {
        languageText.push("No English words were detected (no pos-tagging possible).");
    } else {
        languageText.push("The tweet texts of " + userName + " consists of " + semanticsNouns + "% nouns, " + semanticsVerbs + "% verbs, " + semanticsAdjectives + "% adjectives and " + semanticsNumber + "% numbers.");
    }
    languageText.push(hashtagCount + "% of the tweets include hashtags.");
    languageText.push("In average " + hashtagAvg + " hashtags are included in tweets with hashtags.");
    languageText.push(userName + " tweets in following languages: " + detectedLanguagesTxt + ".");

    return languageText;
}

/**
 * Creates an array which holds sentences of the input user's analysis.
 * 
 * @param analysis
 *            The analysis object of a user.
 * @param userName
 *            The name of the user.
 * @returns An array with texts about the content of the input user.
 */
function getContentText(analysis, userName) {

    let contentText = [];

    // get data
    // 5 most used keywords / hashtags
    let topKeywords = getTop(analysis.content.about.keywords, 5, (container, key, value) => {
        container.count = value.count;
        container.word = key;
    }, (o, v) => o.count < v.count, o => o.word);
    let topHashtags = getTop(analysis.content.about.hashtags, 5, (container, key, value) => {
        container.count = value.count;
        container.word = key;
    }, (o, v) => o.count < v.count, o => "#" + o.word);
    let urls = (100 * analysis.content.media.urls).toFixed(2);
    let videos = (100 * analysis.content.media.videos).toFixed(2);
    let photos = (100 * analysis.content.media.photos).toFixed(2);
    let gifs = (100 * analysis.content.media.gifs).toFixed(2);

    let websites = "";
    for (let i = 0; i < analysis.content.media.domains.length && i < 10; i++) {
    	websites += analysis.content.media.domains[i] + ", ";
    }
    websites = websites.slice(0, websites.length - 2);
    
    // build array
    if (topKeywords.length == 0 && topHashtags.length == 0) {
        contentText.push("Neither keywords nor hashtags could be detected.");
    } else if (topKeywords.length == 0 && topHashtags.length > 0) {
        contentText.push(userName + " uses following hashtag(s): " + topHashtags.join(", ") + ".");
    } else if (topKeywords.length > 0 && topHashtags.length == 0) {
        contentText.push(userName + " uses following keyword(s): " + topKeywords.join(", ") + ".");
    } else {
        contentText.push(userName + " uses following keyword(s) and hashtag(s): " + topKeywords.join(", ") + ", " + topHashtags.join(", ") + ".");
    }
    contentText.push(urls + "% of the tweets include urls, " + videos + "% include videos, " + photos + "% include photos and " + gifs + "% include gifs.");


    if (!websites || websites.length == 0) {
        contentText.push("The tweets of " + userName + " don't include any websites");
    } else {
        contentText.push("The tweets of " + userName + " include following website(s): " + websites + ".");
    }

    return contentText;
}

/**
 * Creates a div html element with the results of a user analysis.
 * 
 * @param user
 *            The twitter user object or undefined.
 * @param analysis
 *            The analysis object
 * @returns The div element with the results of the user's analysis.
 */
function getUserHTML(user, analysis) {
	
	// elements
    let div = createElement('div').attr('class', 'container');
    let spacer = createElement('div').attr('class', 'spacer').append(createElement('span').append('&raquo;'));
    let list = createElement('ul');
    let listElements = [createElement('li'), createElement('li'), createElement('li'), createElement('li')];
	
	let surName = "unknown";
	
    // data of user
	if (user != null) {
		let name = "@" + user.screen_name + " / " + user.name + " / " + user.id;
    	surName = user.name.split(/ /)[0];
    	let url = user.profile_image_url_https.replace(/_normal/i, "");
    	
    	// elements of user
    	let userImg = createElement('img').attr('src', url);
        let userName = createElement('p');
        
        // add to list item
        userName.append(name);
        listElements[0].append(userImg);
        listElements[0].append(userName);
	} else {
		let noUser = createElement('p');
		noUser.html("&nbsp;");
		listElements[0].append(noUser);
	}
  
    // all other elements
	
    let behaviorHeadline = createElement('h3').append('behavior');
    let behaviorP = createElement('p').append(getBehaviorText(analysis, user != null ? surName : "Behavior cluster").join(" "));
    let languageHeadline = createElement('h3').append('language');
    let languageP = createElement('p').append(getLanguageText(analysis, user != null ? surName : "Language cluster").join(" "));
    let contentHeadline = createElement('h3').append('content');
    let contentP = createElement('p').append(getContentText(analysis, user != null ? surName : "Content cluster").join(" "));

    // put everything together
   
    listElements[1].append(behaviorHeadline);
    listElements[1].append(behaviorP);
    listElements[2].append(languageHeadline);
    listElements[2].append(languageP);
    listElements[3].append(contentHeadline);
    listElements[3].append(contentP);
    list.append(listElements[0]);
    list.append(listElements[1]);
    list.append(listElements[2]);
    list.append(listElements[3]);

    div.append(spacer);
    div.append(list);

    return div;
}

/**
 * Returns the dimension of the viewport.
 * 
 * @returns {{width: *, height: *}}
 */
function viewport() {
    var e = window, a = 'inner';
    if (!('innerWidth' in window)) {
        a = 'client';
        e = document.documentElement || document.body;
    }
    return {
        width: e[a + 'Width'],
        height: e[a + 'Height']
    };
}