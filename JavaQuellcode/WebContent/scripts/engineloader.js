/**
 * Created by Diana Lange.
 */

$(document).ready(() => {
	
	let callback = json => {
		if (json.message == 'ok') {
			$("#engine_state").text("Engine is running (" + json.size + "+ twitter users in case base)");
		} else {
			$("#engine_state").text("Engine is offline");
		}
	};
	
	callCBRServer($.ajax, "LoadEngineServlet", {}, callback);
	
});