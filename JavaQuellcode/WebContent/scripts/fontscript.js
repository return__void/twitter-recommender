/**
 * Created by Diana Lange.
 */

/**
 * Sets the base font size (size * zoom).
 * @param size
 *      The new font size.
 * @param zoom
 *      The zoom.
 */
function updateFontSize(size, zoom) {
    $('html').css({fontSize: (size * zoom) + "px"});
}

/**
 * Saves font related information to session. Updates all 3 parameters or just one(s) that are not undefined.
 * @param sizes
 *      A mapping from screen resolution to desired font size or undefined.
 * @param current
 *      The currently used font size or undefined.
 * @param zoom
 *      The currently used zoom or undefined.
 */
function setSessionFontSize(sizes, current, zoom) {

    if (sizes) {
        let resolutions = window.sessionStorage.getItem("resolutions") || JSON.stringify({});
        resolutions = JSON.parse(resolutions);
        for (let key in sizes) {
            resolutions[key] = sizes[key];
        }
        window.sessionStorage.setItem("resolutions", JSON.stringify(resolutions));
    }

    if (current) {
        window.sessionStorage.setItem("fontSize", "" + current);
    }

    if (zoom) {
        window.sessionStorage.setItem("zoom", "" + zoom);
    }
}

/**
 * Finds the font size which is mapped to the screen resolution in session object.
 * @param viewportWidth
 *      The screen resolution (just width).
 * @returns
 *      The font size to the resolution or undefined if resolution is not found in session.
 */
function findFontSize(viewportWidth) {
    viewportWidth = viewportWidth + '';
    var resolutions = window.sessionStorage.getItem("resolutions") || JSON.stringify({});
    resolutions = JSON.parse(resolutions);
    if (resolutions.hasOwnProperty(viewportWidth)) {
        return resolutions[viewportWidth];
    } else {
        return undefined;
    }
}

/**
 * Return current (visible) base font size.
 * @returns The font size.
 */
function getCurrentZoom() {
    let zoom = window.sessionStorage.getItem("zoom") || "1";
    return parseFloat(zoom);
}

/**
 * Returns current (visible) zoom.
 * @returns The zoom.
 */
function getCurrentFontSize() {
    let size = window.sessionStorage.getItem("fontSize") || "12";
    return parseFloat(size);
}

/**
 * Calculates an optimal base font size based on the screen resolution and stores the information in session object.
 */
function calulateFontSize() {

    let body = $('body');
    let view = viewport();

    // check if font size for current resolution is already known
    let size = findFontSize(view.width);

    // current zoom
    let zoom = getCurrentZoom();

    // if resolution not found yet in session, calculate it
    if (!size) {

        // Create div with test text.
        // Start with font size = 12. Incrementally increase font size until div has the same width as screen.

        size = 12;
        let txt = "The quick brown fox jumps over the lazy dog. Lorem ipsum dolor sit amet, consetetur sadipscing elitr.";

        let oldSize = size;
        let id = "font-sizer";
        let div;
        if ($('#' + id).length == 0) {
            let div = createElement('div').attr('id', id);
            div.attr("style", "font-family: Arial, Helvetica, sans-serif; position: absolute; white-space: nowrap; left: 0; top: 0; border: 1px solid #000; display: inline-block");
            div.append(txt);
            body.append(div);
        }
        div = $('#' + id);
        div.css({fontSize: size + "px"});

        var oldWidth = div.width();

        while (div.width() < view.width) {
            size += 0.5;
            div.css({fontSize: size + "px"});

            if (div.width() >= view.width) {

                var oldVal = Math.abs(view.width - div.width());
                var newVal = Math.abs(view.width - oldWidth);
                if (oldVal < newVal) {

                    size = oldSize;
                    div.css({fontSize: size + "px"});
                }
                break;
            }

            oldSize = size;
            oldWidth = div.width();
        }

        // remove test div and stor information
        div.remove();
        var s = {};
        s[view.width] = size;
        setSessionFontSize(s, size, undefined);

    }
    updateFontSize(size, zoom);
    return size;
}