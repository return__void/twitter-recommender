/**
 * Created by Diana Lange.
 */

/**
 * Makes a request for any given id in list and informs observer with response. Request is defined by the given url which
 * should be in form of urlPrefix + id + urlPostfix. The postfix is optional. Example:
 * http://localhost:8889/twitdoc/11111/friends with http://localhost:8889/twitdoc/ being the prefix, 11111 being a member
 * of the (id) list and "/friends" being the postfix.
 * @param reqFct
 *          $.ajax()
 * @param options
 *          { list : array width twitter ids, observer : object with update(data) function, url : { prefix : first part of url, postfox : last part of url }}
 * @constructor
 */
function AnalysisCaller(reqFct, options) {

    /**
     * $.ajax().
     */
    this.reqFct = reqFct;

    this.options = options;

    /**
     * Number of requests / responses.
     */
    this.number = options.list.length;

    /**
     * Some kind of to do list. In the beginning, all ids are stored in that stack. Everytime a request is send,
     * the element is removed from the stack.
     */
    this.requestStack = [];

    /**
     * Number of received responses.
     */
    this.responseCounter = 0;

    /**
     * Element which will be updated with responses.
     */
    this.observer = options.observer;

    /**
     * Copy options.list to to-do stack
     */
    this.setRequestStack = function(ids, container) {
        for (let i in ids) {
            container.push(ids[i]);
        }
    }(this.options.list, this.requestStack);

    /**
     * Sends on request for a given id
     * @param id The twitter id for the current request
     */
    this.makeRequest = function (id) {

        this.reqFct({
            url: this.options.url.prefix + id + this.options.url.postfix,
            type: "get",
            dataType: "json",
            success: json => {
                this.responseCounter++;
                this.observer.update(json);
            },
            error: err => {
                this.responseCounter++;
                this.observer.update(err);
            }
        });
    };

    /**
     * Loops through the stack and makes request calls. Waits a little
     * between the request to avoid spamming the server.
     */
    this.loop = function () {

        // get an element / id from stack
        let element = this.requestStack.pop();

        if (element != undefined) {
            // make request and do next loop
            setTimeout(() => {
                this.makeRequest(element);
                return this.loop();
            }, 10);
        } // else  stack is empty, stop loop
    };

    // start requests
    this.loop();

}

/**
 * Creates a loading bar, updates it whenever the update function of the loading bar is called and calls the callback
 * when the loading bar has reached 100%.
 * @param options
 *      { parent : the element which should be appended by the bar,
 *      steps : number of updating steps,
 *      Selector : $,
 *      id : id selector of the bar,
 *      callback : function that should be called with data when bar has 100%}
 * @constructor
 */
function LoadingBar(options) {

    /**
     * HTML element to append the bar with
     */
    this.parentSelector = options.parent;

    /**
     * Number of updating steps (e.g. when steps=10, 10 updates are needed to reach 100%).
     */
    this.steps = options.steps;

    /**
     * Current number of performed updates.
     */
    this.count = 0;

    /**
     * $
     */
    this.Selector = options.Selector;

    /**
     * Called with data when 100%.
     */
    this.callback = options.callback;

    /**
     * Received data.
     */
    this.data = {};
    
    /**
     * id for selecting the bar
     */
    this.id = options.id;

    /**
     * Initalized the bar and adds it to html.
     */
    this.buildBar = function(parent, id) {

        let div = createElement('div').attr('id', id);
        let bar = createElement('div').attr('class', 'bar').append("&nbsp;");
        let p = createElement('p').append('0%');
        let spacer = createElement('div').attr('class', 'spacer').append(createElement('span').append('&raquo;'));

        div.append(bar);
        div.append(p);

        parent.append(spacer);
        parent.append(div);

    }(this.parentSelector, this.id);

    /**
     * Stores received data to local data variable. Stores only non errors.
     * @param newData
     *      { user : someUserObject, analysis : analysisObject } or error
     */
    this.update = function(newData) {

        // store only non errors
        if (newData.user) {
            this.data[newData.user.id] = newData;
        }

        // keep track how much data is received
        this.count++;

        // update bar
        let p = (100 * parseFloat(this.count) / this.steps).toFixed(2);
        this.Selector('#' + this.id + ' p').text(p + "%");
        this.Selector('#' + this.id + ' .bar').css({width : "calc(" + p + "% - 0.45rem)"});

        // call callback when needed finished
        if (this.count == this.steps) {
            this.callback(this.data);
        }

    }
}