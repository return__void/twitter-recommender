/**
 * Created by Diana Lange.
 */

$(document).ready(() => {

	const nodePort = $('meta[port]').attr('port') || "8888";

    // url for making request concerning the twitter api
    const baseURL = "http://localhost:" + nodePort + "/";
    const userAnalysis = baseURL + "twitdoc/";
    const getUsers = baseURL + "users/";

    // colors for the front end... just for reference purpose
    // source:
	// https://color.adobe.com/de/Kopi-af-vintage-card-color-theme-10067792/?showPublished=true
    const colors = {
        brown: {
            hex: '#5C4B51',
            rgb: [92, 75, 81]
        },
        blue: {
            hex: '#8CBeB2',
            rgb: [140, 190, 178]
        },
        light: {
            hex: '#F2EBBF',
            rgb: [242, 235, 191]
        },
        organge: {
            hex: '#F3B562',
            rgb: [243, 181, 98]
        },
        red: {
            hex: '#F06060',
            rgb: [240, 96, 96]
        }
    };

    /**
	 * stores received information.
	 * 
	 * @type {{user: undefined, userAnalysis: undefined, userFriends: undefined,
	 *       userFriendsAnalysis: undefined}}
	 */
    let localStorage = {
        user: undefined,                    // Twitter user object
        userAnalysis: undefined,            // Analysis object
        userFriendsList: undefined,         // array with ids
        userFriends : undefined,            // array / object with twitter user
											// objects
        userFriendsAnalysis : undefined,    // array / object with analysis
											// objects
        clusteringResults : undefined,		// object with language, content,
											// behavior keys, each contains a
											// whole TwitDoc, represents a
											// Clustering of similar users
											// concerning one of the 3 features
        friendsOfFriendsAnalysis : undefined, // object with analysis objects,
												// keys are the ids of the users
        recommendations : undefined			// array with Recommendations, each
											// element of array has two key:
											// "id" (String) and "similarity"
											// (float)
    };


    /**
	 * Needs that need to be done before page is displayed.
	 */
    this.setup = function () {
    	let z = getCurrentZoom();
    	let s = findFontSize(viewport().width);
    	if (s == undefined) {
    		calulateFontSize();
    	} else {
    		updateFontSize(s, z);
    	}
        
    }();

    /**
	 * Removes all information from page that are correlated to a previous
	 * analysis cycle.
	 * 
	 * @param steps
	 *            Defines which elements are removed from page.
	 */
    function clearPage(steps) {
    	if (steps == undefined) {
    		steps = 4;
    	}
    	
	    // recommendations
    	
    	$('#new-people .container').remove();
	    $('#new-people .spacer').remove();
	    $("#friends-friends-bar").remove();
  
	    if (steps >= 1) {
	    	 $('#results').removeAttr('style');
	    }
        if (steps >= 2) {
	        // friends & clusterings
        	$('#friends .container').remove();
	        $('#friends-bar').remove();
	        $('#friends .spacer').remove();
        }
        if (steps >= 3) {
	        $('#clustering').removeAttr('style');
        }
        if (steps >= 4) {
	        // input user
	        $("#user .container").remove();
	        $('header').removeAttr('style');
        }
    }

    // ---- EVENTS -------------------------------------

    /*
	 * Event which is called whenever the user changes the size of the browser =>
	 * calculate font size for that resolution.
	 */
    $(window).resize(() => calulateFontSize());

    /*
	 * Event for changing the zoom (e.g. user wants font size bigger or
	 * smaller).
	 */
    $('#font-size li').click(evt => {

        // get current setting
        let zoom = getCurrentZoom();
        let fontSize = getCurrentFontSize();

        // change zoom setting
        let txt = $(evt.target).text();
        if (txt.indexOf('-') >= 0) {
            zoom *= 0.9;
        } else if (txt.indexOf('+') >= 0) {
            zoom *= 1.1;
        } else {
            zoom = 1;
        }

        // save changes and apply to page
        setSessionFontSize(undefined, undefined, zoom);
        updateFontSize(fontSize, zoom);
    });

    /*
	 * Event when user typed in an input twitter user (input twitter user = user
	 * for which to make recommendations). Finds user information and user
	 * analysis (when input is found on twitter)
	 */
    $('#get_user').submit(evt => {

        evt.preventDefault();

        $.ajax({
            url: userAnalysis + $('#username').val() + "/friends",
            type: "get",
            dataType: "json",
            success: json => {

                // clear previous data from page
                clearPage();

                // show request results
                let p = json.user ? getUserHTML(json.user, json.analysis) : userNotFoundError(json);
                $("#user").append(p);

                if (json.user) {

					let updateUserCallback = function(res) {
					
						if (res.message == 'ok') {
							// store results
		                    localStorage.user = json.user;
		                    localStorage.userAnalysis = json.analysis;
		                    localStorage.userFriendsList = json.user ? json.analysis.general.friends : undefined;
		
		                    // blend in next step
		                    $('#clustering').css({display: 'inherit'});
		
		                    // show banner from user
		                    if (json.user.profile_banner_url) {
		                        let brown = colors.brown.rgb.join(",");
		                        $('header').css({backgroundImage : "linear-gradient(to top, rgba(" + brown + ", 1.0) 0%, rgba(" + brown + ", 0.4) 100%), url(" + json.user.profile_banner_url + ")", backgroundSize : 100 + "%", backgroundPosition: "center center",  backgroundBlendMode: "dodge"})
		                    }
		                    
		                   
						} else {
							userNotFoundError([{ message : "Could not update case base" }]);
						}
					}
					
					let requestObj = {};
					requestObj[json.user.id_str] = json.analysis;
					
					callCBRServer($.ajax, "UserUpdateServlet", { data : JSON.stringify(requestObj) }, updateUserCallback);  
                }

                $(document.body).animate({
                    'scrollTop': $('a[name=n01]').offset().top
                }, 2000);


            },
            error: err => {
                clearPage()

                let p = userNotFoundError([err]);
                $("#user").append(p);
                $(document.body).animate({
                    'scrollTop': $('a[name=n01]').offset().top
                }, 2000);
            }
        });
    });

    /*
	 * Event which will get analysises of the input user's friends
	 */
    $('#start_friends_analysis').click(evt => {

    	if (localStorage.userFriendsList != undefined && localStorage.userFriendsList.length > 0) {
	        // get some friends ids
	    	let intendetLength = 10;
	        let ids = randomSample(localStorage.userFriendsList, Math.min(intendetLength, localStorage.user.friends_count));
	        
	        // remove previous shown results from page
	        clearPage(2);
	
	        // store them when everything is finished and display next step on
			// html
			// page
	        let callback = data => {
	            
	            localStorage.userFriendsAnalysis = {};
	            localStorage.userFriends = {};
	            for (let key in data) {
	                localStorage.userFriendsAnalysis[key] = data[key].analysis;
	                localStorage.userFriends[key] = data[key].user;
	            }
	            
	            let cbrCallback = response => {
	            	if (response.message == 'ok') {
		            	// console.log(response);
		            	localStorage.clusteringResults = response.data;
		            	
		            	
		            	let language = JSON.parse(localStorage.clusteringResults.language);
		            	let content = JSON.parse(localStorage.clusteringResults.content);
		            	let behavior = JSON.parse(localStorage.clusteringResults.behavior);
		            	
		            	let div = getUserHTML(undefined, {content : content.content, behavior : behavior.behavior, language : language.language});
		            	$('#friends').append(div);
	            	} else {
	            		// user has no friends
	            		localStorage.clusteringResults = undefined;
	            	}
	            	$('#results').css({display : "inherit"});
	            };
	            
	            callCBRServer($.ajax, "FriendsUpdateAndClusteringServlet", { user : localStorage.user.id_str, data : JSON.stringify(localStorage.userFriendsAnalysis)}, cbrCallback);
	        };
	
	        // make progress bar
	        let bar = new LoadingBar({Selector : $, parent : $('#friends'), steps : ids.length, id : "friends-bar", callback : callback});
	
	        // it's not possible to do many getFriends() calls due the
			// restrictions
			// of the twitter api
	        // therefor get some friends without their friends and some with
			// them
	
	        let requestCut = ids.length < intendetLength / 2 ? ids.length : intendetLength / 2;
	        // requests with getFriends()
	        new AnalysisCaller($.ajax, {
	            list: ids.slice(0, requestCut),
	            url: {
	                prefix: userAnalysis,
	                postfix: "/friends"
	            },
	            observer : bar
	        });
	
	        if (ids.length > requestCut) {
		        // requests without getFriends()
		        new AnalysisCaller($.ajax, {
		            list: ids.slice(requestCut),
		            url: {
		                prefix: userAnalysis,
		                postfix: ""
		            },
		            observer : bar
		        });
	        }
    	} else {
    		$('#results').css({display : "inherit"});
    	}
    });

    /*
	 * Event which is triggered when recommendations should be displayed.
	 */
    $('#get-results').click(evt => {
    	let callback = data => {

    		let updateUserCallback = userResponse => {
    			
    			let recommenderCallback = recommenderResponse => {
 
	    			localStorage.recommendations = recommenderResponse.ids;
	        		let clustering = localStorage.recommendations.clustering;
	        		let shared = localStorage.recommendations.shared;
	        		let lookupIDs = [clustering.user.id, clustering.behavior.id, clustering.content.id, clustering.language.id, 
	        		                 shared.user.id, shared.behavior.id, shared.content.id, shared.language.id];
	        		$.ajax({
	        	        url: getUsers,
	        	        data : {"ids" : lookupIDs},
	        	        type: "post",
	        	        dataType: "json",
	        	        success: recommendetUsers => {
	        	           	clustering.user.user = recommendetUsers[0];
	        	           	clustering.behavior.user = recommendetUsers[1];
	        	           	clustering.content.user = recommendetUsers[2];
	        	           	clustering.language.user = recommendetUsers[3];
	        	           	shared.user.user = recommendetUsers[4];
	        	           	shared.behavior.user = recommendetUsers[5];
	        	           	shared.content.user = recommendetUsers[6];
	        	           	shared.language.user = recommendetUsers[7];
	        	            	
	        	           	let div = createResult(localStorage.recommendations);
	        	            $('#new-people').append(div);
	        	        },
	        	        error : error => {}
	        		});
    				
	        	};
	        	

	        	if (localStorage.clusteringResults != undefined) {
	        		callCBRServer($.ajax, "RecommenderServlet", { user : localStorage.user.id_str, data : JSON.stringify(localStorage.clusteringResults) }, recommenderCallback);
	        	} else {
	        		callCBRServer($.ajax, "RecommenderServlet", { user : localStorage.user.id_str }, recommenderCallback);
	        	}
	        };
	        	
	        localStorage.friendsOfFriendsAnalysis = {};
	        	
	        for (let key in data) {
	        	localStorage.friendsOfFriendsAnalysis[key] = data[key].analysis;
	        }
	        	
	        if (localStorage.userFriendsList != undefined &&  localStorage.userFriendsList.length > 0) {
	        	callCBRServer($.ajax, "UserUpdateServlet", { data : JSON.stringify(localStorage.friendsOfFriendsAnalysis) }, updateUserCallback);
	        } else {
	        	updateUserCallback(undefined);
	        }
    	};
	        
    	// get all friends ids of all currently known friends
    	let lookupIDs = {};
	        
    	for (let key in localStorage.userFriendsAnalysis) {
    		let analysis = localStorage.userFriendsAnalysis[key];
	        	
    		let friends = analysis.general.friends;
	        	
    		for (let i in friends) {
    			lookupIDs[friends[i]] = friends[i];
    		}
	    }
	        
	    // get random sample of friends of friends
	    lookupIDs = shuffle(Object.keys(lookupIDs));
	    lookupIDs = lookupIDs.slice(0, Math.min(lookupIDs.length, 20));
	        
	    // remove previous shown recommendations from page
	    clearPage(0);
	        
	    if (localStorage.userFriendsList != undefined &&  localStorage.userFriendsList.length > 0) {
	        
	    	// make progress bar
	        let bar = new LoadingBar({Selector : $, parent : $('#new-people'), id : "friends-friends-bar", steps : lookupIDs.length, callback : callback});
	
	        // find and analyse friends of friends
	        new AnalysisCaller($.ajax, {
	            list: lookupIDs,
	            url: {
	                prefix: userAnalysis,
	                postfix: ""
	            },
	            observer : bar
	        });
	    } else {
	    	callback({});
	    }
	        
	});
    
});




