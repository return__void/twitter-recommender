<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="language" content="en">
<title>Twitter Recommender</title>


<!-- Im JSP - dadurch wird doGet() vom Servlet aufgerufen -->
<jsp:include page="ConfigServlet" flush="true" />
<meta port="${port}">

<link rel="shortcut icon" href="img/favicon.png">
<link
	href="https://fonts.googleapis.com/css?family=Amatic+SC|Roboto+Slab|PT+Serif:400i"
	rel="stylesheet">
<link rel="stylesheet" href="style/format.css" type="text/css"
	media="all">
<script src="https://code.jquery.com/jquery-3.2.1.min.js"
	integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
	crossorigin="anonymous"></script>
<script src="scripts/loadingbar.js"></script>
<script src="scripts/fontscript.js"></script>
<script src="scripts/tools.js"></script>
<script src="scripts/cbrHandler.js"></script>
<script src="scripts/script.js"></script>
</head>

<body>
	<header>
		<h1>&raquo; Twitter Recommender &laquo;</h1>
		<nav id="font-size">
			<ul>
				<li>A-</li>
				<li>A</li>
				<li>A+</li>
			</ul>
		</nav>
	</header>
	<main>
	<section id="about">
		<h2>
			<p class="section_id">
				<a name="n00"></a><span>00.</span>
			</p>
			&nbsp;About
		</h2>
		<div class="content">
			<p class="columns">
				<b>Goals:</b> This project tries to find recommendations for whom to
				follow on Twitter. Twitter itself doesn't provide a proper
				recommender. Therefore a system is needed to find better
				recommendations that are not only based on existing social
				connections but on similarities between users. This recommender
				analyses the Twitter pages of users and extracts three features:
				"language" (e.g. emoticon usage), "content" (e.g. keywords and
				hashtags) and "behavior" (e.g. number of tweets per day). These
				features are used to detect similarities using the case-based
				reasoning engine "myCBR".<br>
				<b>Instructions:</b> (1) CBR engine loading - When this page loads
				for the first time, the CBR engine starts automatically. Please wait
				for your first input until the engine is running. This may take a
				while when the case base is large. You can check the state of the
				CBR engine at the very bottom of the page in the footer. (2) User
				Input - Go to section "01. Who are you?". Type in the Twitter user
				name you want a recommendation for, e.g. "@tonkuhle" or "tonkuhle",
				and hit "submit". If the user was found, the next section will be
				revealed. (3) Feature extraction - Go to section "02. Who do you
				follow?" and hit "Start feature extraction". Wait until the loading
				bar is finished and the last section is displayed. This step will
				update the case base with the friends of the input Twitter user. In
				addition a cluster of friends of the input user is found that
				contains people that are similar to each other concerning one of the
				three main features "language", "behavior" or "content". The results
				of the cluster process are displayed. If the clusters are not
				satisfying, please press again "Start feature extraction" button. (4)
				Recommendations - Go to section "03. Find new people to follow!""
				and hit the "Find new friends" button. Wait again for the loading
				bar to finish. After that, up to eight recommendations are
				displayed. For every recommendation a link is provided that will
				target you to the Twitter pager of the recommended user. The results
				are bases on detected similarities between the input user, the found
				clusters of section two and common friends of the input users
				friends.<br>
				<b>Limitations:</b> The recommendations are based on a content
				analysis of the public Twitter page of the input user and of his or
				her friends. Therefore the recommendation will only work if the
				input user a) is public, b) has tweeted something that are not
				retweets and c) has at least one friend (one user the he or she
				follows) that matches the restrictions of a) and b). The
				recommendations are limited to the known cases of the CBR case base.
				For every recommendation cycle the case base is extended with new
				Twitter users and (some) existing cases are updated. As the case
				base grows, the recommendations will be more accurate.

			</p>
		</div>

	</section>
	<section id="who">
		<h2>
			<p class="section_id">
				<a name="n01"></a><span>01.</span>
			</p>
			&nbsp;Who are you?
		</h2>
		<div id="user" class="content">
			<p>Please, type in your twitter user name or your twitter user
				id:</p>
			<form method="get" id="get_user">
				<input type="text" id="username"> <input value="Submit"
					type="submit">
			</form>
		</div>
	</section>
	<section id="clustering">
		<h2>
			<p class="section_id">
				<a name="n02"></a><span>02.</span>
			</p>
			&nbsp;Who do you follow?
		</h2>
		<div id="friends" class="content">
			<p>During this step similarities between the people you follow
				are extracted. For each feature - behavior, language and content - a
				subset of friends is found which represents the feature in a uniform
				way.</p>
			<p class="center">
				<button id="start_friends_analysis">Start feature
					extraction</button>
			</p>
		</div>
	</section>
	<section id="results">
		<h2>
			<p class="section_id">
				<a name="n03"></a><span>03.</span>
			</p>
			&nbsp;Find new people to follow!
		</h2>
		<div id="new-people" class="content">
			<p>Finding new friends based on the people you already follow.</p>
			<p class="center">
				<button id="get-results">Find new friends</button>
			</p>
		</div>
	</section>

	</main>
	<footer>
		<div class="content">
			<p>
				<span id="engine_state">Engine is starting</span>&nbsp;&bull;&nbsp;
				Copyright by Diana Lange 2017&nbsp;&bull;&nbsp; <a
					href="http://www.diana-lange.de/content/impressum.html"
					target="_blank">Imprint (German, external)</a>&nbsp;&bull;&nbsp;
				Color theme found at <a
					href="https://color.adobe.com/de/Kopi-af-vintage-card-color-theme-10067792/?showPublished=true"
					target="_blank">Adobe Color (external)</a>
			</p>
		</div>
	</footer>
	<script src="scripts/engineloader.js"></script>

</body>
</html>
