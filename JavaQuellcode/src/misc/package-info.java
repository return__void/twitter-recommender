/**
 * A collection of classes and helpers for various tasks.
 * 
 * @author Diana Lange
 *
 */
package misc;