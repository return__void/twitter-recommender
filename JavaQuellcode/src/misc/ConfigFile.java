package misc;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * A class that loads the config file of the CBR engine. The config folder
 * should be named ".twitterRecommender". This folder should be placed at the
 * user's home location. e.g. C:\Users\*TheUserName*\.twitterRecommender. The
 * config file itself should be named "config.txt" and it is placed within the
 * ".twitterRecommender" folder.
 * 
 * The default values are: PORT : 8888 PROJECT : twitterRecommender
 * 
 * @author Diana Lange
 *
 */
public class ConfigFile {

	/**
	 * Map - keys : name of the setting, values : value of the setting.
	 */
	private static HashMap<String, Object> settings = null;

	/**
	 * Key for finding the port which is used for the node server. Use this for
	 * getConfig().
	 */
	public final static String PORT = "NODE-PORT";

	/**
	 * Key for finding the file containing the myCBR project. Use this for
	 * getConfig().
	 */
	public final static String PROJECT = "PROJECT";

	/**
	 * Sets defaults for config or loads settings from config file.
	 */
	private static void initConfig() {
		HashMap<String, Object> settings = new HashMap<String, Object>();

		// defaults
		settings.put(ConfigFile.PORT, "8888");
		settings.put(ConfigFile.PROJECT, "twitterRecommender");

		// possible existing config file
		File staticLocation = new File(System.getProperty("user.home"), ".twitterRecommender");

		// load settings from file of it exists
		if (staticLocation.exists()) {
			File configFile = new File(staticLocation, "config.txt");

			if (configFile.exists()) {
				ArrayList<String> fileLines = Tools.loadFileLines(configFile);

				for (String line : fileLines) {
					if (line.length() > 0) {
						String[] keyValue = line.split("=");
						settings.put(keyValue[0], keyValue[1]);
					}
				}
			}
		}

		// store config settings.
		ConfigFile.settings = settings;
	}

	/**
	 * Gets a HashMap which contains all configurations of the project.
	 * 
	 * @return The map with the configuatios.
	 */
	public static HashMap<String, Object> get() {
		if (ConfigFile.settings == null) {
			initConfig();
		}

		return settings;
	}

	/**
	 * Get a config with a known name.
	 * 
	 * @param input
	 *            One of the constants of ConfigFile.
	 * @return "unknown" for wrong input or setting value.
	 */
	public static Object getConfig(String input) {

		if (ConfigFile.get().containsKey(input)) {
			return ConfigFile.get().get(input);
		} else {
			return "unknown";
		}

	}

}
