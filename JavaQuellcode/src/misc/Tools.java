package misc;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Collection of tools that make debugging easier (and some other general
 * purpose stuff).
 * 
 * @author Diana Lange
 *
 */
public class Tools {
	/**
	 * No instances allowed.
	 */
	private Tools() {
	}

	/**
	 * Checks if a String is part of a String array.
	 * 
	 * @param e
	 *            The lookup String.
	 * @param list
	 *            A String array.
	 * @return True if e is in list.
	 */
	public static boolean contains(String e, String[] list) {
		boolean contains = false;

		for (String l : list) {
			if (l.equals(e)) {
				return true;
			}
		}

		return contains;
	}

	/**
	 * Checks if a String is part of an array.
	 * 
	 * @param e
	 *            The lookup String.
	 * @param list
	 *            An array.
	 * @param parser
	 *            A parser that parse elements of the input array to String.
	 * @param <T>
	 *            The type of the array entries of the array.
	 * @return True if e is in list.
	 */
	public static <T> boolean contains(String e, T[] list, Parser<T, String> parser) {
		boolean contains = false;

		for (T l : list) {
			if (parser.parse(l).equals(e)) {
				return true;
			}
		}

		return contains;
	}

	/**
	 * Joins all elements of an array and returns a joined text version of the
	 * array.
	 * 
	 * @param list
	 *            The input array.
	 * @param delim
	 *            The String between the elements of the array.
	 * @param <T>
	 *            The type of the array entries of the array.
	 * @return The String version of the array.
	 */
	public static <T> String join(T[] list, String delim) {
		return Tools.join(list, delim, null);
	}

	/**
	 * Joins all elements of an array and returns a joined text version of the
	 * array.
	 * 
	 * @param list
	 *            The input array.
	 * @param delim
	 *            The String between the elements of the array.
	 * @param parser
	 *            Null or a parser that parses the values of the array in a
	 *            suitable (text) format.
	 * @param <T>
	 *            The type of the array entries of the array.
	 * @return The String version of the array.
	 */
	public static <T> String join(T[] list, String delim, Parser<T, ?> parser) {
		String txt = "";
		String d = "";

		for (T element : list) {

			if (parser != null) {
				txt += d + parser.parse(element);
			} else {
				txt += d + element;
			}
			d = delim;
		}

		return txt;
	}

	/**
	 * Joins all elements of a ArrayList and returns a joined text version of
	 * the ArrayList.
	 * 
	 * @param list
	 *            The input ArrayList.
	 * @param delim
	 *            The String between the elements of the ArrayList.
	 * @param <T>
	 *            The type of the array entries of the list.
	 * @return The String version of the ArrayList.
	 */
	public static <T> String join(ArrayList<T> list, String delim) {
		return Tools.join(list, delim, null);
	}

	/**
	 * Joins all elements of a ArrayList and returns a joined text version of
	 * the ArrayList.
	 * 
	 * @param list
	 *            The input ArrayList.
	 * @param delim
	 *            The String between the elements of the ArrayList.
	 * @param parser
	 *            Null or a parser that parses the values of the ArrayList in a
	 *            suitable (text) format.
	 * @param <T>
	 *            The type of the array entries of the list.
	 * @return The String version of the ArrayList.
	 */
	public static <T> String join(ArrayList<T> list, String delim, Parser<T, ?> parser) {
		String txt = "";
		String d = "";

		for (T element : list) {

			if (parser != null) {
				txt += d + parser.parse(element);
			} else {
				txt += d + element;
			}
			d = delim;
		}

		return txt;
	}

	/**
	 * Joins all elements of a HashMap (keys and values) and returns a joined
	 * text version of the HashMap.
	 * 
	 * @param map
	 *            The input HashMap.
	 * @param delim
	 *            The String between the elements of the HashMap.
	 * @param <T>
	 *            The type of the values of the HashMap.
	 * @return The String version of the HashMap.
	 */
	public static <T> String join(HashMap<?, T> map, String delim) {
		return Tools.join(map, delim, null);
	}

	/**
	 * Joins all elements of a HashMap (keys and values) and returns a joined
	 * text version of the HashMap.
	 * 
	 * @param map
	 *            The input HashMap.
	 * @param delim
	 *            The String between the elements of the HashMap.
	 * @param parser
	 *            Null or a parser that parses the values of the HashMap in a
	 *            suitable (text) format.
	 * @param <T>
	 *            The type of the values of the HashMap.
	 * @return The String version of the HashMap.
	 */
	public static <T> String join(HashMap<?, T> map, String delim, Parser<T, ?> parser) {
		String txt = "";
		String d = "";

		for (Object key : map.keySet()) {

			T obj = map.get(key);

			// key and value are identical, just add one of them
			String keyText = obj.equals(key) ? "" : key + ": ";

			if (parser != null) {
				txt += d + keyText + parser.parse(map.get(key));
			} else {
				txt += d + keyText + map.get(key);
			}
			d = delim;

		}

		return txt;
	}

	/**
	 * Loads the content of a (text) file.
	 * 
	 * @param file
	 *            The file (path) to the (existing) file.
	 * @return A list containing the content the text file.
	 */
	public static ArrayList<String> loadFileLines(File file) {

		ArrayList<String> tempLines = new ArrayList<String>();

		FileReader fr = null;

		try {
			fr = new FileReader(file);
		} catch (FileNotFoundException e) {

			// should not happen.
			System.out.println("File doesn't exist");
			System.out.println(file.getAbsolutePath());
		}

		if (fr != null) {
			BufferedReader br = new BufferedReader(fr);

			String line = "";

			// read lines and add them to list

			while (line != null) {

				try {
					line = br.readLine();
				} catch (IOException e) {
					System.out.println("something's wrong with a line in file");
				}

				if (line != null && line.length() > 0) {
					tempLines.add(line);
				}
			}

			// close reader

			try {
				br.close();
			} catch (IOException e) {
				System.out.println("closing file went wrong");
			}
		}

		return tempLines;
	}
}
