package misc;

import processing.data.JSONObject;

/**
 * An interface for JSON generators, e.g. classes that can produce a JSON
 * version of an instance.
 * 
 * @author Diana Lange
 *
 */
public interface JSONCreator {

	/**
	 * Creates a JSON object.
	 * 
	 * @return The JSON object.
	 */
	JSONObject json();

	/**
	 * String version of the JSON object.
	 * 
	 * @return The String version of the json().
	 */
	public default String text() {
		return json() + "";
	}
}
