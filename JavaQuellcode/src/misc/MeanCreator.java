package misc;

import java.util.ArrayList;

/**
 * An interface to create a mean value from a set of input values.
 * 
 * @author Diana Lange
 *
 * @param <Mean>
 *            Type of the returned mean value.
 * @param <Input>
 *            Type of the input values.
 */
public interface MeanCreator<Mean, Input> {

	/**
	 * Calculates the mean of the input.
	 * 
	 * @param elements
	 *            Input set which defines the mean value.
	 * @return The mean value.
	 */
	Mean mean(ArrayList<Input> elements);

	/**
	 * Calculates the mean of the input.
	 * 
	 * @param elements
	 *            Input set which defines the mean value.
	 * @return The mean value.
	 */
	Mean mean(Input[] elements);
}
