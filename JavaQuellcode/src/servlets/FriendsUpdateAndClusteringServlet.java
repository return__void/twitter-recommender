package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import processing.data.JSONObject;

import data.TwitDoc;
import mycbr.CBREngine;

/**
 * A servlet for updating / saving a set of twitter users analysises (TwitDocs)
 * to case base. These users are the friends of an input user. After the case
 * base is updated, all (known) friends of an input user are retrieved from case
 * base. These friends are clustered concerning the three features (Behavior,
 * Language and Content). For each clustering a random chosen cluster is picked
 * and the mean of the cluster is returned.
 * 
 * @author Diana Lange
 *
 */
@WebServlet("/FriendsUpdateAndClusteringServlet")
public class FriendsUpdateAndClusteringServlet extends HttpServlet {

	/**
	 * The id.
	 */
	private static final long serialVersionUID = 185608939405817299L;

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.
	 * HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.
	 * HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) {

		// System.out.println("update frieds");
		// String analysis = request.getParameter("user");

		String userID = request.getParameter("user");
		String analysis = request.getParameter("data");

		JSONObject clusteringResults = null;

		// System.out.println(analysis);

		if (analysis != null && analysis.length() > 0 && userID != null && userID.length() > 0) {

			JSONObject users = JSONObject.parse(analysis);

			Iterator<?> iterator = users.keyIterator();
			TwitDoc[] newUsers = new TwitDoc[users.size()];

			int i = 0;
			while (iterator.hasNext()) {
				Object key = iterator.next();
				newUsers[i] = new TwitDoc(users.getJSONObject("" + key));
				i++;
			}

			CBREngine.get().updateCB(newUsers);

			clusteringResults = CBREngine.get().getClusterings(userID, 100);

		}

		response.setContentType("application/json");
		PrintWriter out = null;
		try {
			out = response.getWriter();
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (out != null && clusteringResults != null) {

			JSONObject ok = JSONObject.parse(UserUpdateServlet.ok + "");
			ok.setJSONObject("data", clusteringResults);
			out.println(ok);
			out.close();
		} else if (clusteringResults == null) {
			// friends have no friends
			out.println(UserUpdateServlet.error);
			out.close();
		} else if (out != null) {
			out.println(UserUpdateServlet.error);
			out.close();
		}
	}
}
