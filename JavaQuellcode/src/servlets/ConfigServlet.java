package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import misc.ConfigFile;

/**
 * A servlet which triggers the loading of the config file of the project.
 * Writes the port number of the node server (which is loaded from config file)
 * to the attributes.
 * 
 * @author Diana Lange
 *
 */
@WebServlet("/ConfigServlet")
public class ConfigServlet extends HttpServlet {

	/**
	 * The id.
	 */
	private static final long serialVersionUID = -2034356219363678084L;

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.
	 * HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.
	 * HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) {

		request.setAttribute("port", ConfigFile.getConfig(ConfigFile.PORT));
	}
}
