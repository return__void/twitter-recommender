package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mycbr.CBREngine;
import processing.data.JSONObject;

/**
 * A servlet that will start the CBR engine on call of get or post. Will reply
 * with an "ok"-message when loading was successful.
 * 
 * @author Diana Lange
 *
 */
@WebServlet("/LoadEngineServlet")
public class LoadEngineServlet extends HttpServlet {

	/**
	 * The ID
	 */
	private static final long serialVersionUID = 435794924383720538L;

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.
	 * HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.
	 * HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) {
		CBREngine engine = CBREngine.get();

		response.setContentType("application/json");
		PrintWriter out = null;
		try {
			out = response.getWriter();
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (out != null && engine != null) {
			JSONObject ok = JSONObject.parse(UserUpdateServlet.ok + "");
			ok.setInt("size", engine.size());
			out.println(ok);
			out.close();
		} else if (out != null) {
			out.println(UserUpdateServlet.error);
			out.close();
		}
	}
}
