/**
 * A collection of servlets for the Twitter Recommender. The Servlets server as
 * a connection between the front end / user interface and the CBR engine.
 * 
 * @author Diana Lange
 *
 */
package servlets;
