package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import processing.data.JSONObject;

import data.TwitDoc;
import mycbr.CBREngine;


/**
 * Sets or updates the TwitDoc of one or more users in the case base.
 * 
 * @author Diana Lange
 *
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {

	/**
	 * The id.
	 */
	private static final long serialVersionUID = -8289656797422821821L;

	/**
	 * Response if no error occurred.
	 */
	public final static JSONObject ok = UserUpdateServlet.getOK();

	/**
	 * Response if error occurred.
	 */
	public final static JSONObject error = UserUpdateServlet.getError();

	/**
	 * Builds the no error response.
	 * 
	 * @return Okay message.
	 */
	private static JSONObject getOK() {
		JSONObject obj = new JSONObject();
		obj.setString("message", "ok");

		return obj;
	}

	/**
	 * Builds the error response.
	 * 
	 * @return Error message.
	 */
	private static JSONObject getError() {
		JSONObject obj = new JSONObject();
		obj.setString("message", "error");

		return obj;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.
	 * HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.
	 * HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) {

		String analysis = request.getParameter("data");

		if (analysis != null && analysis.length() > 0) {

			JSONObject users = JSONObject.parse(analysis);

			Iterator<?> iterator = users.keyIterator();
			TwitDoc[] newUsers = new TwitDoc[users.size()];

			int i = 0;
			while (iterator.hasNext()) {
				Object key = iterator.next();
				// System.out.println(key + ": " + users.getJSONObject("" +
				// key));
				newUsers[i] = new TwitDoc(users.getJSONObject("" + key));
				i++;
			}

			CBREngine.get().updateCB(newUsers);

			/*
			 * TwitDoc newUser = new TwitDoc(analysis);
			 * 
			 * 
			 * CBREngine.get().updateCB(newUser);
			 */

		}

		response.setContentType("application/json");
		PrintWriter out = null;
		try {
			out = response.getWriter();
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (out != null && analysis != null && analysis.length() > 0) {

			out.println(ok);
			out.close();
		} else if (out != null) {
			out.println(error);
			out.close();
		}
	}
}
