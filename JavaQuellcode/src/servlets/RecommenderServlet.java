package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import processing.data.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import data.TwitDoc;
import mycbr.CBREngine;

/**
 * Gets as input four TwitDocs (as JSON-String) and find for each TwitDoc a
 * similar case in the case base.
 * 
 * @author Diana Lange
 *
 */
@WebServlet("/RecommenderServlet")
public class RecommenderServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8762862972983613632L;

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.
	 * HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.
	 * HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) {
		String userID = request.getParameter("user");
		String analysis = request.getParameter("data");
		JSONObject ids = null;

		if (analysis != null && userID != null && analysis.length() > 0 && userID.length() > 0) {
			JSONObject clusterings = JSONObject.parse(analysis);
			// System.out.println(clusterings);

			String languageJSON = clusterings.getString("language");
			String contentJSON = clusterings.getString("content");
			String behaviorJSON = clusterings.getString("behavior");

			TwitDoc languageUser = new TwitDoc(languageJSON);
			TwitDoc contentUser = new TwitDoc(contentJSON);
			TwitDoc behaviorUser = new TwitDoc(behaviorJSON);
			TwitDoc inputUser = CBREngine.get().getUser(userID);

			ids = CBREngine.get().findRecommendations(inputUser, behaviorUser.getBehavior(), contentUser.getContent(),
					languageUser.getLanguage());

		} else if ((analysis == null || analysis.length() == 0) && (userID != null && userID.length() > 0)) {			
			TwitDoc inputUser = CBREngine.get().getUser(userID);
		
			ids = CBREngine.get().findRecommendations(inputUser, inputUser.getBehavior(), inputUser.getContent(),
					inputUser.getLanguage());
		}

		response.setContentType("application/json");

		PrintWriter out = null;
		try {
			out = response.getWriter();
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (out != null && ids != null) {

			JSONObject ok = JSONObject.parse(UserUpdateServlet.ok + "");
			
			ok.setJSONObject("ids", ids);

			out.println(ok);
			out.close();
		} else if (out != null) {
			out.println(UserUpdateServlet.error);
			out.close();
		}
	}
}
