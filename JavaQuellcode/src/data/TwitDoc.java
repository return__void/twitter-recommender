package data;

import java.util.ArrayList;
import java.util.List;

import processing.data.JSONObject;

import de.dfki.mycbr.core.DefaultCaseBase;
import de.dfki.mycbr.core.casebase.Attribute;
import de.dfki.mycbr.core.casebase.Instance;
import de.dfki.mycbr.core.model.Concept;
import de.dfki.mycbr.core.model.StringDesc;
import de.dfki.mycbr.core.retrieval.Retrieval;
import de.dfki.mycbr.core.similarity.Similarity;
import de.dfki.mycbr.util.Pair;

import misc.JSONCreator;
import misc.MeanCreator;
import mycbr.CBRConstants;
import mycbr.CBREngine;
import mycbr.similarity.AbstractDistance;
import mycbr.similarity.Distance;

/**
 * A class that represents the analysis results of a twitter user. The analysis
 * are extracted from the user's twitter timeline and some other features. Each
 * analysis contains four features: general, behavior, content and language.
 * Instances from this class can be saved to the myCBR casebase via save()
 * method or similar cases can be found via "retrieve()" method. The class also
 * contains methods to calculate the distance between two TwitDocs, which can be
 * used for clustering processes and retrieving the similar cases.
 * 
 * @author Diana Lange
 *
 */
public class TwitDoc extends AbstractDistance<TwitDoc> implements MeanCreator<TwitDoc, TwitDoc>, JSONCreator {

	/**
	 * Behavior feature of the user analysis.
	 */
	private Behavior behavior;

	/**
	 * Language feature of the user analysis.
	 */
	private Language language;

	/**
	 * Content feature of the user analysis.
	 */
	private Content content;

	/**
	 * General feature of the user analysis.
	 */
	private General general;

	/**
	 * Creates a new empty TwitDoc (e.g. to create a MeanCreator for TwitDocs).
	 */
	private TwitDoc() {
		behavior = new Behavior();
		language = new Language();
		content = new Content();
		general = new General();
	}

	/**
	 * Creates a new TwitDoc.
	 * 
	 * @param behavior
	 *            Behavior feature of the user analysis.
	 * @param language
	 *            Language feature of the user analysis.
	 * @param content
	 *            Content feature of the user analysis.
	 * @param general
	 *            General feature of the user analysis.
	 */
	private TwitDoc(Behavior behavior, Language language, Content content, General general) {
		this.behavior = behavior;
		this.language = language;
		this.content = content;
		this.general = general;
	}

	/**
	 * Creates a new TwitDoc.
	 * 
	 * @param json
	 *            A JSONObject that contains the four features behavior,
	 *            language, content and general.
	 */
	public TwitDoc(JSONObject json) {
		this();
		parse(json);
	}

	/**
	 * Creates a new TwitDoc.
	 * 
	 * @param jsonText
	 *            A text representing a JSONObject that contains the four
	 *            features behavior, language, content and general.
	 */
	public TwitDoc(String jsonText) {
		parse(JSONObject.parse(jsonText));
	}

	/**
	 * Adds the attributes creator.json() and id to the input instance.
	 * 
	 * @param creator
	 *            The object that should be added as the "json" attribute to the
	 *            instance.
	 * @param id
	 *            The String that should be added as the "id" attribute to the
	 *            instance.
	 * @param i
	 *            The instance which will contain the attributes "json" and
	 *            "id".
	 * @param concept
	 *            The concept which contains the two desciptors "json" and "id".
	 */
	public static void setInstance(JSONCreator creator, String id, Instance i, Concept concept) {
		try {

			StringDesc jsonDesc = (StringDesc) concept.getAttributeDesc("json");
			StringDesc idDesc = (StringDesc) concept.getAttributeDesc("id");

			Attribute jsonAttr = jsonDesc.getAttribute(creator.text());
			Attribute idAttr = idDesc.getAttribute(id);

			i.addAttribute(jsonDesc, jsonAttr);
			i.addAttribute(idDesc, idAttr);

		} catch (Exception e) {

		}
	}

	/**
	 * Finds cases that are similar to the current instance of TwitDoc.
	 * 
	 * @return A list of results (similar cases) sorted by their similarity
	 *         (beginning with the most similar case). The list may include also
	 *         the case that is exactly the same as the input instance of the
	 *         TwitDoc.
	 */
	public List<Pair<Instance, Similarity>> retrieve() {
		Concept concept = CBREngine.get().getConcept(CBRConstants.Concepts.USER);
		DefaultCaseBase cb = CBREngine.get().getCasebase(CBRConstants.CB.USER);

		Retrieval ret = new Retrieval(concept, cb);
		ret.setRetrievalMethod(Retrieval.RetrievalMethod.RETRIEVE_SORTED);

		Instance query = ret.getQueryInstance();
		TwitDoc.setInstance(this, general.getID(), query, concept);
		ret.start();

		return ret.getResult();
	}

	/**
	 * Stores the TwitDoc in the casebase. The name of the new instance is
	 * "user_####" where #### stands for the twitter user id.
	 */
	public void save() {
		try {
			Concept concept = CBREngine.get().getConcept(CBRConstants.Concepts.USER);
			DefaultCaseBase cb = CBREngine.get().getCasebase(CBRConstants.CB.USER);

			Instance newInstance = concept.addInstance(TwitDoc.name() + "_" + general.getID());
			TwitDoc.setInstance(this, general.getID(), newInstance, concept);

			cb.addCase(newInstance);

		} catch (Exception e) {
			System.err.println(e);
		}
	}

	/**
	 * Creates a JSON object that represents this instance of a TwitDoc.
	 * 
	 * @return The JSON.
	 */
	@Override
	public JSONObject json() {
		JSONObject obj = new JSONObject();

		obj.setJSONObject("general", general.json());
		obj.setJSONObject("behavior", behavior.json());
		obj.setJSONObject("language", language.json());
		obj.setJSONObject("content", content.json());

		return obj;
	}

	/**
	 * Parses a JSONObject to the attributes of this TwitDoc.
	 * 
	 * @param analysis
	 *            The JSONObject containing the information of the new instance.
	 */
	private void parse(JSONObject analysis) {

		try {
			JSONObject generalJSON = analysis.getJSONObject("general");
			JSONObject behaviorJSON = analysis.getJSONObject("behavior");
			JSONObject languageJSON = analysis.getJSONObject("language");
			JSONObject contentJSON = analysis.getJSONObject("content");

			general = new General(generalJSON);

			String id = general.getID();

			behavior = new Behavior(id, behaviorJSON);
			language = new Language(id, languageJSON);
			content = new Content(id, contentJSON);
		} catch (Exception e) {
			System.err.println("Problems parsing JSONObject to TwitDoc");
			System.err.println(analysis);
		}
	}

	/**
	 * Getter.
	 * 
	 * @return "user"
	 */
	public static String name() {
		return "user";
	}

	/**
	 * Getter.
	 * 
	 * @return The behavior feature of this user's twitter analysis.
	 */
	public Behavior getBehavior() {
		return behavior;
	}

	/**
	 * Getter.
	 * 
	 * @return The language feature of this user's twitter analysis.
	 */
	public Language getLanguage() {
		return language;
	}

	/**
	 * Getter.
	 * 
	 * @return The content feature of this user's twitter analysis.
	 */
	public Content getContent() {
		return content;
	}

	/**
	 * Getter.
	 * 
	 * @return The general feature of this user's twitter analysis.
	 */
	public General getGeneral() {
		return general;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see misc.MeanCreator#mean(java.util.ArrayList)
	 */
	@Override
	public TwitDoc mean(ArrayList<TwitDoc> elements) {
		return mean(elements.toArray(new TwitDoc[elements.size()]));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see misc.MeanCreator#mean(java.lang.Object[])
	 */
	@Override
	public TwitDoc mean(TwitDoc[] elements) {
		Behavior[] behaviors = new Behavior[elements.length];
		Language[] languages = new Language[elements.length];
		Content[] contents = new Content[elements.length];

		for (int i = 0; i < elements.length; i++) {
			behaviors[i] = elements[i].behavior;
			languages[i] = elements[i].language;
			contents[i] = elements[i].content;
		}
		return new TwitDoc(behavior.mean(behaviors), language.mean(languages), content.mean(contents), new General());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mycbr.similarity.AbstractDistance#get()
	 */
	@Override
	public TwitDoc get() {
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mycbr.similarity.AbstractDistance#distance(java.lang.Object,
	 * java.lang.Object)
	 */
	@Override
	public float distance(TwitDoc o, TwitDoc v) {
		return (o.behavior.distance(v.behavior) + o.content.distance(v.content) + o.language.distance(v.language)) / 3f;
	}

	/**
	 * Gets a MeanCreator that can calculate the mean of a set of TwitDocs. The
	 * returned mean will also be a TwitDoc.
	 * 
	 * @return The mean creator for TwitDocs.
	 */
	public static MeanCreator<TwitDoc, TwitDoc> getMeanFct() {
		return new TwitDoc();
	}

	/**
	 * Creates a weighted sum distance calculator for TwitDocs.
	 * 
	 * @param weights
	 *            Up to three positive values (including 0) which define the
	 *            weights for a TwitDoc. First value is the weight for behavior,
	 *            second is weight for content, third for language feature.
	 *            Default weights are 1.
	 * @return the TwitDoc Distance.
	 */
	public static Distance<TwitDoc> getWeightedDistanceFct(float... weights) {

		// map the input weights to values from 0...1 with sumOfAllWeights=1
		float[] localWeights = { 1, 1, 1 };
		if (weights.length > 0) {
			localWeights[0] = weights[0];
		}
		if (weights.length > 1) {
			localWeights[1] = weights[1];
		}
		if (weights.length > 2) {
			localWeights[2] = weights[2];
		}

		float sum = localWeights[0] + localWeights[1] + localWeights[2];

		localWeights[0] /= sum;
		localWeights[1] /= sum;
		localWeights[2] /= sum;

		return (o, v) -> {
			float d1 = localWeights[0] == 0 ? 0 : localWeights[0] * o.behavior.distance(v.behavior);
			float d2 = localWeights[1] == 0 ? 0 : localWeights[1] * o.content.distance(v.content);
			float d3 = localWeights[2] == 0 ? 0 : localWeights[2] * o.language.distance(v.language);
			return d1 + d2 + d3;
		};
	}

	/**
	 * Creates a distance calculator for TwitDocs.
	 * 
	 * @return The TwitDoc distance calculator.
	 */
	public static Distance<TwitDoc> getDistanceFct() {
		return (o, v) -> (o.behavior.distance(v.behavior) + o.content.distance(v.content)
				+ o.language.distance(v.language)) / 3f;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		String txt = "";
		txt += general + "\n";
		txt += behavior + "\n";
		txt += language + "\n";
		txt += content + "\n";

		return txt;
	}

}
