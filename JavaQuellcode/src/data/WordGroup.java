package data;

import processing.data.JSONArray;
import processing.data.JSONObject;

/**
 * A class that represent a group of words which are similar to each other.
 * 
 * @author Diana Lange
 *
 */
public class WordGroup {

	/**
	 * On sample word of the group.
	 */
	private String representant;

	/**
	 * All words that are similar to each other / all words that are part of
	 * this group.
	 */
	private String[] alternatives;

	/**
	 * Amount of which this word group has appeared in a certain context (e.g.
	 * one of the words of the group has appeared count-times in a document).
	 */
	private int count;

	/**
	 * Builds a new WordGroup.
	 * 
	 * @param representant
	 *            On sample word of the group.
	 * @param alternatives
	 *            Words that are similar to each other.
	 * @param count
	 *            Amount.
	 */
	public WordGroup(String representant, String[] alternatives, int count) {
		this.representant = representant;
		this.alternatives = alternatives;
		this.count = count;
	}

	/**
	 * Builds a new WordGroup from a JSONObject.
	 * 
	 * @param representant
	 *            The representator of this group.
	 * @param obj
	 *            Object with "word" (JSONArray) and "count" (int) as keys.
	 */
	public WordGroup(String representant, JSONObject obj) {
		this.representant = representant;
		count = obj.getInt("count");
		JSONArray word = obj.getJSONArray("word");
		alternatives = new String[word.size()];
		for (int i = 0; i < word.size(); i++) {
			alternatives[i] = word.getString(i);
		}

	}

	/**
	 * JSON version of this object (Doesn't include the representant).
	 * 
	 * @return The JSONObject version of this instance.
	 */
	public JSONObject json() {
		JSONObject obj = new JSONObject();
		JSONArray arr = new JSONArray();

		for (String s : alternatives) {
			arr.append(s);
		}
		obj.setInt("count", count);
		obj.setJSONArray("word", arr);

		return obj;
	}

	/**
	 * Getter.
	 * 
	 * @return The representator / one sample word of this group.
	 */
	public String getRepresentant() {
		return this.representant;
	}

	/**
	 * Getter.
	 * 
	 * @return All members of this group.
	 */
	public String[] getAlternatives() {
		return alternatives;
	}

	/**
	 * Getter.
	 * 
	 * @return The amount of this word group. E.g. how often has any of the
	 *         words of this group appeared in a document.
	 */
	public int getCount() {
		return count;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		String txt = "{" + representant + ": " + String.join(", ", alternatives) + " [" + count + "x]}";

		return txt;
	}
}
