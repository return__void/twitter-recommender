package data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import misc.MeanCreator;
import mycbr.similarity.Distance;

/**
 * A class that performs agglomerative hierarchical clustering with complete
 * linkage and Lance-Williams algorithm for optimization.
 * 
 * @author Diana Lange
 *
 * @param <T>
 *            Type of elements which will be clustered.
 */
public class Clustering<T> {

	/**
	 * A value that defines at which point elements are too different to be put
	 * together in one clustering.
	 */
	public static float stopDistance = 0.4f;

	/**
	 * The elements that should be clustered.
	 */
	private T[] elements;

	/**
	 * A distance function which is valid for the elements of the clustering.
	 */
	private Distance<T> distanceFct;

	/**
	 * Desired number of clusters. May not be reached because of stop criterion
	 * set by Clustering.stopDistance.
	 */
	private int k;

	/**
	 * A list with all created clusters.
	 */
	private ArrayList<Cluster<T>> clusters;

	/**
	 * A function that can create a mean value from a set of cluster members.
	 */
	private MeanCreator<T, T> meanFct;

	/**
	 * Creates a new clustering. Clustering will performed directly after
	 * instantiation.
	 * 
	 * @param elements
	 *            The elements which will be clustered.
	 * @param distanceFct
	 *            A distance function which is valid for the elements of the
	 *            clustering.
	 * @param meanFct
	 *            A function that can create a mean value from a set of cluster
	 *            members.
	 * @param k
	 *            Desired number of clusters. May not be reached because of stop
	 *            criterion set by Clustering.stopDistance.
	 */
	public Clustering(T[] elements, Distance<T> distanceFct, MeanCreator<T, T> meanFct, int k) {
		this.meanFct = meanFct;
		this.k = k;
		this.elements = elements;
		this.distanceFct = distanceFct;
		performClustering();
		setMeans();
	}

	/**
	 * Inits clusters with the each cluster containing one element of the given
	 * set of elements.
	 * 
	 * @return A map with key=index [0, elements.length) and value=cluster.
	 *         Therefore behaves like a sorted list (indices are numbered), but
	 *         will later give the opportunity to have holes in it (missing
	 *         indices).
	 */
	private HashMap<Integer, Cluster<T>> getClusters() {
		HashMap<Integer, Cluster<T>> clusters = new HashMap<Integer, Cluster<T>>();
		for (int i = 0; i < elements.length; i++) {
			Cluster<T> cluster = new Cluster<T>(elements[i]);
			clusters.put(i, cluster);
		}

		return clusters;
	}

	/**
	 * Creates a map which includes for every pair of clusters a distance value
	 * for the cluster combination. The form of the map is like this:
	 * key: value (0, 1, 2 are the indices of the clusters).
	 * 0: [HashMap<Integer, Float>]
	 * 1: [HashMap<Integer, Float>]
	 * 2: [HashMap<Integer, Float>]
	 * ...
	 * 
	 * For the second map the form is (e.g. for index 0):
	 * 0: [1:distanceValue, 2:distanceValue, 3:distanceValue,...]
	 * 
	 * @param clusters
	 *            The initial clusters. With key=index, value=cluster.
	 * @return A map which includes the distances between all clusters.
	 */
	private HashMap<Integer, HashMap<Integer, Float>> getClusterDistances(HashMap<Integer, Cluster<T>> clusters) {

		HashMap<Integer, HashMap<Integer, Float>> distances = new HashMap<Integer, HashMap<Integer, Float>>();
		for (int i = 0; i < clusters.size() - 1; i++) {

			HashMap<Integer, Float> indexDistanceMap = new HashMap<Integer, Float>();

			for (int j = i + 1; j < clusters.size(); j++) {

				// distances are no metrics, hence calculate both distances
				// and take the max. value into account
				float d1 = distanceFct.distance(clusters.get(i).get(0), clusters.get(j).get(0));
				float d2 = distanceFct.distance(clusters.get(j).get(0), clusters.get(i).get(0));
				indexDistanceMap.put(j, Math.max(d1, d2));
			}

			distances.put(i, indexDistanceMap);
		}

		return distances;
	}

	/**
	 * Performs the clustering until stable clusters are created.
	 */
	private void performClustering() {

		// get clusters and their distances
		HashMap<Integer, Cluster<T>> clusters = getClusters();
		HashMap<Integer, HashMap<Integer, Float>> distances = getClusterDistances(clusters);

		// do until stable
		while (clusters.size() > k) {
			int minI = 0;
			int minJ = 0;
			float minD = -1;

			// find currently smallest distance in map
			for (Map.Entry<Integer, HashMap<Integer, Float>> entryDistances : distances.entrySet()) {
				Integer i = entryDistances.getKey();
				HashMap<Integer, Float> indexDistanceMap = entryDistances.getValue();
				for (Map.Entry<Integer, Float> entry : indexDistanceMap.entrySet()) {
					Integer j = entry.getKey();

					if (j <= i) {
						continue;
					}
					Float d = entry.getValue();

					if (distances.containsKey(j) && distances.get(j).containsKey(i)) {
						d = Math.max(d, distances.get(j).get(i));
						// d /= 2;
					}

					if (minD == -1 || minD > d) {
						minD = d;
						minI = i;
						minJ = j;
					}
				}
			}

			// merge the two most similar clusters (if distance is still smaller than stopDistance)
			if (minD < Clustering.stopDistance) {
				// merge two clusters, remover second one from active list (since it got merged with the other one=
				clusters.get(minI).merge(clusters.get(minJ));
				clusters.remove(minJ);

				// clean and update the distance map
				// updates are max distance from both merged clusters
				ArrayList<int[]> removers = new ArrayList<int[]>();

				// look for indexes that have to be removed from map
				// and update the distance values
				for (Map.Entry<Integer, HashMap<Integer, Float>> entryDistances : distances.entrySet()) {
					Integer i = entryDistances.getKey();
					HashMap<Integer, Float> indexDistanceMap = entryDistances.getValue();

					if (indexDistanceMap.containsKey(minI) && indexDistanceMap.containsKey(minJ)) {
						float d = Math.max(indexDistanceMap.get(minI), indexDistanceMap.get(minJ));
						indexDistanceMap.put(minI, d);
						removers.add(new int[] { i, minJ });
					} else if (i > minI && indexDistanceMap.containsKey(minJ)) {

						Float jd = distances.get(minI).get(i);
						Float id = distances.get(i).get(minJ);

						float d = Math.max(id, jd);

						distances.get(minI).put(i, d);
						removers.add(new int[] { i, minJ });
					} else if (i == minJ) {

						for (Map.Entry<Integer, Float> entry : indexDistanceMap.entrySet()) {
							Integer j = entry.getKey();
							Float jd = entry.getValue();
							Float id = distances.get(minI).get(j);

							float d = Math.max(id, jd);
							distances.get(minI).put(j, d);

						}
					}

					if (indexDistanceMap.isEmpty()) {
						removers.add(new int[] { i });
					}
				}
				
				// actually remove from the values from map
				distances.remove(minJ);
				distances.get(minI).remove(minJ);

				for (int[] rem : removers) {
					if (rem.length == 1) {
						distances.remove(rem[0]);
					} else {
						distances.get(rem[0]).remove(rem[1]);
					}
				}

			} else {
				// left clusters are not similar enough. Therefore stop clustering.
				break;
			}

		}

		// build list from final cluster map
		this.clusters = new ArrayList<Cluster<T>>();
		this.clusters.addAll(clusters.values());
	}

	/**
	 * Computes the error / loss for each cluster.
	 */
	public void computeClusterErrors() {

		for (Cluster<T> c : this.clusters) {
			T mean = c.getMean();

			ArrayList<T> elements = c.getMembers();

			float error = 0;
			for (T element : elements) {
				error += distanceFct.distance(mean, element);
			}

			error /= elements.size();

			c.setError(error);
			// System.out.println("cluster with " + c.getMembers().size() + "
			// elements has error of " + error + ".");

			if (error == 0 && elements.size() > 1) {
				// c.printCluster();
			}
		}
	}

	/**
	 * Finds a cluster randomly.
	 * @return The random picked cluster.
	 */
	public Cluster<T> getRandomCluster() {
		Cluster<T> cluster = null;

		int index = (int) (Math.random() * clusters.size());
		cluster = clusters.get(index);
		
		return cluster;
	}

	/**
	 * Finds the cluster with the most members.
	 * @return The biggest cluster.
	 */
	public Cluster<T> getBiggestCluster() {
		Cluster<T> big = null;
		int memberSize = -1;

		for (Cluster<T> c : clusters) {
			if (c.size() > memberSize) {
				memberSize = c.size();
				big = c;
			}
		}

		return big;
	}

	/**
	 * Calculates the mean for each cluster.
	 */
	private void setMeans() {

		for (Cluster<T> c : this.clusters) {

			if (c.getMembers().size() == 1) {
				c.setMean(c.getMembers().get(0));
			} else {
				c.setMean(meanFct.mean(c.getMembers()));
			}
		}
	}
}
