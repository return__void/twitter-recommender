package data;

import java.util.ArrayList;
import java.util.List;

import processing.data.JSONObject;

import de.dfki.mycbr.core.DefaultCaseBase;
import de.dfki.mycbr.core.casebase.Instance;
import de.dfki.mycbr.core.model.Concept;
import de.dfki.mycbr.core.retrieval.Retrieval;
import de.dfki.mycbr.core.similarity.Similarity;
import de.dfki.mycbr.util.Pair;

import misc.JSONCreator;
import misc.MeanCreator;
import mycbr.CBRConstants;
import mycbr.CBREngine;
import mycbr.CBRConstants.Distances.BehaviorDistances;
import mycbr.similarity.AbstractDistance;
import mycbr.similarity.Distance;
import mycbr.similarity.DistanceFactory;

/**
 * A class that represents the behavior feature of a TwitDoc. The behavior
 * includes information about how often a user tweets, replies, retweets, with
 * how many other user is he / she in contact through the tweets and how well
 * are the tweets perceived (e.g. how often do they get retweeted).
 * 
 * @author Diana Lange
 *
 */
public class Behavior extends AbstractDistance<Behavior> implements MeanCreator<Behavior, Behavior>, JSONCreator {

	/**
	 * The general behavior of the user (tweetfrequency,...).
	 */
	private General general;

	/**
	 * The interaction behavior of the user (how does the input user interact
	 * with other people, e.g. how often does he / she retweet tweets of
	 * others).
	 */
	private Interaction interaction;

	/**
	 * The reaction behavior to the user's tweets (e.g. how many tweets of the
	 * user gets favorited).
	 */
	private Reaction reaction;

	/**
	 * The twitter id of the user.
	 */
	private String id;

	/**
	 * JSON version of this instance.
	 */
	private JSONObject json;

	/**
	 * Generates an empty behavior feature.
	 */
	protected Behavior() {
		this.json = null;
		this.id = "unknown";
		this.general = new General();
		this.interaction = new Interaction();
		this.reaction = new Reaction();
	}

	/**
	 * Creates a new behavior feature.
	 * 
	 * @param general
	 *            General feature of the user's behavior.
	 * @param interaction
	 *            Interaction feature of the user's behavior.
	 * @param reaction
	 *            Reaction feature of the user's behavior.
	 */
	private Behavior(General general, Interaction interaction, Reaction reaction) {
		this.json = null;
		this.id = "unknown";
		this.general = general;
		this.interaction = interaction;
		this.reaction = reaction;
	}

	/**
	 * Creates a new behavior feature.
	 * 
	 * @param behavior
	 *            A JSONObject that contains the features general, interaction
	 *            and reaction.
	 */
	public Behavior(JSONObject behavior) {
		this("unknown", behavior);
	}

	/**
	 * Creates a new behavior feature.
	 * 
	 * @param id
	 *            The twitter id of the user.
	 * @param behavior
	 *            A JSONObject that contains the features general, interaction
	 *            and reaction.
	 */
	public Behavior(String id, JSONObject behavior) {
		this();
		try {
			this.id = id;
			general = new General(behavior.getJSONObject("general"));
			interaction = new Interaction(behavior.getJSONObject("interaction"));
			reaction = new Reaction(behavior.getJSONObject("reaction"));
		} catch (Exception e) {
			System.err.println("Problems parsing JSONObject to Behavior");
			System.err.println(behavior);
		}
	}

	/**
	 * Stores the Behavior in the casebase. The name of the new instance is
	 * "behavior_####" where #### stands for the twitter user id.
	 */
	public void save() {
		try {
			Concept concept = CBREngine.get().getConcept(CBRConstants.Concepts.BEHAVIOR);
			DefaultCaseBase cb = CBREngine.get().getCasebase(CBRConstants.CB.BEHAVIOR);

			Instance newInstance = concept.addInstance(Behavior.name() + "_" + getID());
			TwitDoc.setInstance(this, getID(), newInstance, concept);

			cb.addCase(newInstance);

		} catch (Exception e) {
			System.err.println(e);
		}
	}

	/**
	 * Finds cases that are similar to the current instance of Behavior.
	 * 
	 * @return A list of results (similar cases) sorted by their similarity
	 *         (beginning with the most similar case). The list may include also
	 *         the case that is exactly the same as the input instance of the
	 *         Behavior.
	 */
	public List<Pair<Instance, Similarity>> retrieve() {
		Concept concept = CBREngine.get().getConcept(CBRConstants.Concepts.BEHAVIOR);
		DefaultCaseBase cb = CBREngine.get().getCasebase(CBRConstants.CB.BEHAVIOR);

		Retrieval ret = new Retrieval(concept, cb);
		ret.setRetrievalMethod(Retrieval.RetrievalMethod.RETRIEVE_SORTED);

		Instance query = ret.getQueryInstance();
		TwitDoc.setInstance(this, getID(), query, concept);
		ret.start();

		return ret.getResult();
	}

	/**
	 * Creates a JSON object that represents this instance of a Behavior.
	 * 
	 * @return The JSON.
	 */
	public JSONObject json() {

		if (json == null) {

			json = new JSONObject();

			json.setJSONObject("general", general.json());
			json.setJSONObject("interaction", interaction.json());
			json.setJSONObject("reaction", reaction.json());
		}

		return json;
	}

	/**
	 * Getter.
	 * 
	 * @return The twitter user id or "unknown".
	 */
	public String getID() {
		return id;
	}

	/**
	 * Getter.
	 * 
	 * @return The general feature of the behavior.
	 */
	public General getGeneral() {
		return general;
	}

	/**
	 * Getter.
	 * 
	 * @return The interaction feature of the behavior.
	 */
	public Interaction getInteraction() {
		return interaction;
	}

	/**
	 * Getter.
	 * 
	 * @return The reaction feature of the behavior.
	 */
	public Reaction getReaction() {
		return reaction;
	}

	/**
	 * Getter.
	 * 
	 * @return "behavior"
	 */
	public static String name() {
		return "behavior";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see misc.MeanCreator#mean(java.util.ArrayList)
	 */
	@Override
	public Behavior mean(ArrayList<Behavior> elements) {
		return mean(elements.toArray(new Behavior[elements.size()]));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see misc.MeanCreator#mean(java.lang.Object[])
	 */
	@Override
	public Behavior mean(Behavior[] elements) {
		General g = general.mean(elements);
		Interaction i = interaction.mean(elements);
		Reaction r = reaction.mean(elements);
		return new Behavior(g, i, r);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mycbr.similarity.AbstractDistance#get()
	 */
	@Override
	public Behavior get() {
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mycbr.similarity.AbstractDistance#distance(java.lang.Object,
	 * java.lang.Object)
	 */
	@Override
	public float distance(Behavior o, Behavior v) {
		return (o.general.distance(v.general) + o.interaction.distance(v.interaction) + o.reaction.distance(v.reaction))
				/ 3f;
	}

	/**
	 * Creates a distance calculator for Behaviors.
	 * 
	 * @return The Behavior distance calculator.
	 */
	public static Distance<Behavior> getDistanceFct() {
		return (o, v) -> (o.general.distance(v.general) + o.interaction.distance(v.interaction)
				+ o.reaction.distance(v.reaction)) / 3f;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {

		String txt = "behavior: { \n";
		txt += "\t" + general + "\n";
		txt += "\t" + interaction + "\n";
		txt += "\t" + reaction + "\n";
		txt += "}";
		return txt;
	}

	/**
	 * A class that represents the reaction feature of a behavior.
	 * 
	 * @author Diana Lange
	 *
	 */
	public static class Reaction extends AbstractDistance<Reaction> implements MeanCreator<Reaction, Behavior>, JSONCreator {

		/**
		 * (Average) number of retweets which every tweet of the user gets.
		 */
		private double avgRetweets;

		/**
		 * (Percentaged) number of tweets which get retweets.
		 */
		private double pRetweets;

		/**
		 * (Average) number of favorites which every tweet of the user gets.
		 */
		private double avgFavorites;

		/**
		 * (Percentaged) number of favorites which get retweets.
		 */
		private double pFavorites;

		/**
		 * JSON version of this instance.
		 */
		private JSONObject json;

		/**
		 * Generates an empty reaction feature.
		 */
		private Reaction() {
			json = null;
			avgRetweets = 0;
			pRetweets = 0;
			avgFavorites = 0;
			pFavorites = 0;
		}

		/**
		 * Creates a new reaction feature.
		 * 
		 * @param reaction
		 *            A JSONObject that contains the attributes for the reaction
		 *            feature.
		 */
		public Reaction(JSONObject reaction) {
			this();
			try {
				avgRetweets = reaction.getJSONObject("retweets").getDouble("average");
				pRetweets = reaction.getJSONObject("retweets").getDouble("percentage");
				avgFavorites = reaction.getJSONObject("favorites").getDouble("average");
				pFavorites = reaction.getJSONObject("favorites").getDouble("percentage");
			} catch (Exception e) {
				System.err.println("Problems parsing JSONObject to Behavior.Reaction");
				System.err.println(reaction);
			}
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see misc.JSONCreator#json()
		 */
		public JSONObject json() {

			if (json == null) {
				json = new JSONObject();

				JSONObject retweets = new JSONObject();
				JSONObject favorites = new JSONObject();

				retweets.setDouble("average", avgRetweets);
				retweets.setDouble("percentage", pRetweets);

				favorites.setDouble("average", avgFavorites);
				favorites.setDouble("percentage", pFavorites);

				json.setJSONObject("retweets", retweets);
				json.setJSONObject("favorites", favorites);

			}
			return json;
		}

		/**
		 * Getter.
		 * 
		 * @return (Average) number of retweets which every tweet of the user
		 *         gets.
		 */
		public float getAverageRetweets() {
			return (float) avgRetweets;
		}

		/**
		 * Getter.
		 * 
		 * @return (Percentaged) number of tweets which get retweets
		 */
		public float getPercentageRetweets() {
			return (float) pRetweets;
		}

		/**
		 * Getter.
		 * 
		 * @return (Average) number of favorites which every tweet of the user
		 *         gets.
		 */
		public float getAverageFavorites() {
			return (float) avgFavorites;
		}

		/**
		 * Getter.
		 * 
		 * @return (Percentaged) number of favorites which get retweets.
		 */
		public float getPercentageFavorites() {
			return (float) pFavorites;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see misc.MeanCreator#mean(java.util.ArrayList)
		 */
		@Override
		public Reaction mean(ArrayList<Behavior> elements) {
			return mean(elements.toArray(new Behavior[elements.size()]));
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see misc.MeanCreator#mean(java.lang.Object[])
		 */
		@Override
		public Reaction mean(Behavior[] elements) {

			Reaction newElement = new Reaction();

			for (Behavior e : elements) {
				newElement.avgFavorites += e.reaction.avgFavorites;
				newElement.avgRetweets += e.reaction.avgRetweets;
				newElement.pFavorites += e.reaction.pFavorites;
				newElement.pRetweets += e.reaction.pRetweets;
			}

			if (elements.length == 0) {
				return newElement;
			}

			newElement.avgFavorites /= elements.length;
			newElement.avgRetweets /= elements.length;
			newElement.pFavorites /= elements.length;
			newElement.pRetweets /= elements.length;

			return newElement;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see mycbr.similarity.AbstractDistance#get()
		 */
		@Override
		public Reaction get() {
			return this;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see mycbr.similarity.AbstractDistance#distance(java.lang.Object,
		 * java.lang.Object)
		 */
		@Override
		public float distance(Reaction o, Reaction v) {
			
			// get distance function for each attribute & calculate distance for the attribute
			float dAvgRetweets = DistanceFactory.getSimpleFloatDistance(BehaviorDistances.REACTION_AVGRETWEETS_DISTANCE)
					.distance(o.getAverageRetweets(), v.getAverageRetweets());
			float dPRetweets = DistanceFactory.getSimpleFloatDistance(BehaviorDistances.REACTION_PRETWEETS_DISTANCE)
					.distance(o.getPercentageRetweets(), v.getPercentageRetweets());
			float dAvgFavorites = DistanceFactory
					.getSimpleFloatDistance(BehaviorDistances.REACTION_AVGFAVORITES_DISTANCE)
					.distance(o.getAverageFavorites(), v.getAverageFavorites());
			float dPFavorites = DistanceFactory.getSimpleFloatDistance(BehaviorDistances.REACTION_PFAVORITES_DISTANCE)
					.distance(o.getPercentageFavorites(), v.getPercentageFavorites());

			return 0.25f * (dAvgRetweets + dPRetweets + dAvgFavorites + dPFavorites);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#toString()
		 */
		public String toString() {
			String txt = "";
			txt += "reaction: { ";
			txt += "favorites percentage : " + getPercentageFavorites() + ", ";
			txt += "favorites average : " + getAverageFavorites() + ", ";
			txt += "retweets percentage : " + getPercentageRetweets() + ", ";
			txt += "retweets average : " + getAverageRetweets() + " }";
			return txt;
		}

	}

	/**
	 * A class that represents the interaction feature of a behavior.
	 * 
	 * @author Diana Lange
	 *
	 */
	public static class Interaction extends AbstractDistance<Interaction>
			implements MeanCreator<Interaction, Behavior>, JSONCreator {

		/**
		 * Number of favorites the user makes per day.
		 */
		private double favorites;

		/**
		 * (Percentage) Proportion of the tweets of the user which are retweets
		 * of others.
		 */
		private double pRetweets;

		/**
		 * (Percentage) Proportion of the tweets of the user which are replies
		 * to other tweets.
		 */
		private double pReplies;

		/**
		 * (Percentage) Proportion of the tweets which contain at-mentions.
		 */
		private double pAtMentions;

		/**
		 * (Average) Number of people which are contained in one tweet.
		 */
		private double avgPeople;

		/**
		 * (Percentage) Proportion of people which are mentioned more than once
		 * by the user.
		 */
		private double reccuringPeople;

		/**
		 * JSON version of this instance.
		 */
		private JSONObject json;

		/**
		 * Generates an empty interaction feature.
		 */
		private Interaction() {
			json = null;
			favorites = 0;
			pRetweets = 0;
			pReplies = 0;
			pAtMentions = 0;
			avgPeople = 0;
			reccuringPeople = 0;
		}

		/**
		 * Creates a new interaction feature.
		 * 
		 * @param interaction
		 *            A JSONObject that contains the attributes for the
		 *            interaction feature.
		 */
		public Interaction(JSONObject interaction) {
			this();
			try {
				favorites = interaction.getDouble("favorites");
				pRetweets = interaction.getJSONObject("retweets").getDouble("percentage");
				pReplies = interaction.getJSONObject("replies").getDouble("percentage");
				pAtMentions = interaction.getJSONObject("atMentions").getDouble("percentage");
				avgPeople = interaction.getJSONObject("people").getDouble("average");
				reccuringPeople = interaction.getJSONObject("people").getDouble("recurring");
			} catch (Exception e) {
				System.err.println("Problems parsing JSONObject to Behavior.Interaction");
				System.err.println(interaction);
			}
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see misc.JSONCreator#json()
		 */
		public JSONObject json() {

			if (json == null) {
				json = new JSONObject();
				JSONObject retweets = new JSONObject();
				JSONObject replies = new JSONObject();
				JSONObject atMentions = new JSONObject();
				JSONObject people = new JSONObject();

				retweets.setDouble("percentage", pRetweets);
				replies.setDouble("percentage", pReplies);
				atMentions.setDouble("percentage", pAtMentions);
				people.setDouble("average", avgPeople);
				people.setDouble("recurring", reccuringPeople);

				json.setDouble("favorites", favorites);
				json.setJSONObject("retweets", retweets);
				json.setJSONObject("replies", replies);
				json.setJSONObject("atMentions", atMentions);
				json.setJSONObject("people", people);
			}
			return json;
		}

		/**
		 * Getter.
		 * 
		 * @return Number of favorites the user makes per day.
		 */
		public float getFavorites() {
			return (float) favorites;
		}

		/**
		 * Getter.
		 * 
		 * @return (Percentage) Proportion of the tweets of the user which are
		 *         replies to other tweets.
		 */
		public float getReplies() {
			return (float) pReplies;
		}

		/**
		 * Getter.
		 * 
		 * @return (Percentage) Proportion of the tweets of the user which are
		 *         retweets of others.
		 */
		public float getRetweets() {
			return (float) pRetweets;
		}

		/**
		 * Getter.
		 * 
		 * @return (Percentage) Proportion of the tweets which contain
		 *         at-mentions.
		 */
		public float getAtMentions() {
			return (float) pAtMentions;
		}

		/**
		 * Getter.
		 * 
		 * @return (Average) Number of people which are contained in one tweet.
		 */
		public float getAvgPeople() {
			return (float) avgPeople;
		}

		/**
		 * Getter.
		 * 
		 * @return (Percentage) Proportion of people which are mentioned more
		 *         than once by the user.
		 */
		public float getReccuringPeople() {
			return (float) reccuringPeople;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see misc.MeanCreator#mean(java.util.ArrayList)
		 */
		@Override
		public Interaction mean(ArrayList<Behavior> elements) {
			return mean(elements.toArray(new Behavior[elements.size()]));
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see misc.MeanCreator#mean(java.lang.Object[])
		 */
		@Override
		public Interaction mean(Behavior[] elements) {

			Interaction newElement = new Interaction();

			for (Behavior e : elements) {
				newElement.pAtMentions += e.interaction.pAtMentions;
				newElement.avgPeople += e.interaction.avgPeople;
				newElement.favorites += e.interaction.favorites;
				newElement.pReplies += e.interaction.pReplies;
				newElement.pRetweets += e.interaction.pRetweets;
				newElement.reccuringPeople += e.interaction.reccuringPeople;
			}

			if (elements.length == 0) {
				return newElement;
			}

			newElement.pAtMentions /= elements.length;
			newElement.avgPeople /= elements.length;
			newElement.favorites /= elements.length;
			newElement.pReplies /= elements.length;
			newElement.pRetweets /= elements.length;
			newElement.reccuringPeople /= elements.length;

			return newElement;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see mycbr.similarity.AbstractDistance#get()
		 */
		@Override
		public Interaction get() {
			return this;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see mycbr.similarity.AbstractDistance#distance(java.lang.Object,
		 * java.lang.Object)
		 */
		@Override
		public float distance(Interaction o, Interaction v) {
			// get distance function for each attribute & calculate distance for the attribute
			float dFavorites = DistanceFactory.getSimpleFloatDistance(BehaviorDistances.INTERACTION_FAVORITES_DISTANCE)
					.distance(o.getFavorites(), v.getFavorites());
			float dPRetweets = DistanceFactory.getSimpleFloatDistance(BehaviorDistances.INTERACTION_RETWEETS_DISTANCE)
					.distance(o.getRetweets(), v.getRetweets());
			float dPReplies = DistanceFactory.getSimpleFloatDistance(BehaviorDistances.INTERACTION_REPLIES_DISTANCE)
					.distance(o.getReplies(), v.getReplies());
			float dAtMentions = DistanceFactory
					.getSimpleFloatDistance(BehaviorDistances.INTERACTION_ATMENTIONS_DISTANCE)
					.distance(o.getAtMentions(), v.getAtMentions());
			float dAvgPeople = DistanceFactory.getSimpleFloatDistance(BehaviorDistances.INTERACTION_AVGPEOPLE_DISTANCE)
					.distance(o.getAvgPeople(), v.getAvgPeople());
			float dRecurringPeople = DistanceFactory
					.getSimpleFloatDistance(BehaviorDistances.INTERACTION_RECURRINGPEOPLE_DISTANCE)
					.distance(o.getReccuringPeople(), v.getReccuringPeople());

			return (dFavorites + dPRetweets + dPReplies + dAtMentions + dAvgPeople + dRecurringPeople) / 6f;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#toString()
		 */
		public String toString() {
			String txt = "";
			txt += "interaction: { ";
			txt += "favorites : " + getFavorites() + ", ";
			txt += "retweets : " + getRetweets() + ", ";
			txt += "replies : " + getReplies() + ", ";
			txt += "atMentions : " + getAtMentions() + ", ";
			txt += "avgPeople : " + getAvgPeople() + ", ";
			txt += "reccuringPeople : " + getReccuringPeople() + " }";
			return txt;
		}

	}

	/**
	 * A class that represents the general feature of a behavior.
	 * 
	 * @author Diana Lange
	 *
	 */
	public static class General extends AbstractDistance<General>
			implements MeanCreator<General, Behavior>, JSONCreator {

		/**
		 * Number of tweets that are produced by the user per day.
		 */
		private double tweetfrequency;

		/**
		 * How many times is the user listed.
		 */
		private int listed;

		/**
		 * Ratio of friends / follower.
		 */
		private double friendsPerFollower;

		/**
		 * JSON version of this instance.
		 */
		private JSONObject json;

		/**
		 * Generates an empty general feature.
		 */
		private General() {
			json = null;
			tweetfrequency = 0;
			listed = 0;
			friendsPerFollower = 0;
		}

		/**
		 * Creates a new general feature.
		 * 
		 * @param general
		 *            A JSONObject that contains the attributes for the general
		 *            feature.
		 */
		public General(JSONObject general) {
			this();
			try {
				tweetfrequency = general.getJSONObject("tweetfrequency").getDouble("latest");
				listed = general.getInt("listed");
				friendsPerFollower = general.getDouble("friendsPerFollower");
			} catch (Exception e) {
				System.err.println("Problems parsing JSONObject to Behavior.General");
				System.err.println(general);
			}
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see misc.JSONCreator#json()
		 */
		public JSONObject json() {
			if (json == null) {
				json = new JSONObject();

				JSONObject tweetfreq = new JSONObject();
				tweetfreq.setDouble("latest", tweetfrequency);

				json.setJSONObject("tweetfrequency", tweetfreq);
				json.setInt("listed", listed);
				json.setDouble("friendsPerFollower", friendsPerFollower);

			}
			return json;
		}

		/**
		 * Getter.
		 * 
		 * @return Number of tweets that are produced by the user per day.
		 */
		public float getTweetfrequency() {
			return (float) tweetfrequency;
		}

		/**
		 * Getter.
		 * 
		 * @return How many times is the user listed.
		 */
		public int getListed() {
			return listed;
		}

		/**
		 * Getter.
		 * 
		 * @return Ratio of friends / follower.
		 */
		public float getFriendsPerFollower() {
			return (float) friendsPerFollower;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see misc.MeanCreator#mean(java.util.ArrayList)
		 */
		@Override
		public General mean(ArrayList<Behavior> elements) {
			return mean(elements.toArray(new Behavior[elements.size()]));
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see misc.MeanCreator#mean(java.lang.Object[])
		 */
		@Override
		public General mean(Behavior[] elements) {

			General newElement = new General();

			for (Behavior e : elements) {
				newElement.friendsPerFollower += e.general.friendsPerFollower;
				newElement.listed += e.general.listed;
				newElement.tweetfrequency += e.general.tweetfrequency;
			}

			if (elements.length == 0) {
				return newElement;
			}

			newElement.friendsPerFollower /= elements.length;
			newElement.listed /= elements.length;
			newElement.tweetfrequency /= elements.length;

			return newElement;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see mycbr.similarity.AbstractDistance#get()
		 */
		@Override
		public General get() {
			return this;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see mycbr.similarity.AbstractDistance#distance(java.lang.Object,
		 * java.lang.Object)
		 */
		@Override
		public float distance(General o, General v) {
			// get distance function for each attribute & calculate distance for the attribute
			float dTweetfreq = DistanceFactory.getSimpleFloatDistance(BehaviorDistances.GENERAL_TWEETFREQUENCY_DISTANCE)
					.distance(o.getTweetfrequency(), v.getTweetfrequency());
			float dListed = DistanceFactory.getSimpleFloatDistance(BehaviorDistances.GENERAL_LISTED_DISTANCE)
					.distance((float) o.getListed(), (float) v.getListed());
			float dFriendsPerFollower = DistanceFactory
					.getSimpleFloatDistance(BehaviorDistances.GENERAL_FRIENDSPERFOLLOWER_DISTANCE)
					.distance(o.getFriendsPerFollower(), v.getFriendsPerFollower());

			return (dTweetfreq + dListed + dFriendsPerFollower) / 3f;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#toString()
		 */
		public String toString() {
			String txt = "";
			txt += "general: { ";
			txt += "tweetfrequency : " + getTweetfrequency() + ", ";
			txt += "listed : " + getListed() + ", ";
			txt += "friendsPerFollower : " + getFriendsPerFollower() + " }";
			return txt;
		}

	}

}
