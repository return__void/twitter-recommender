package data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import processing.data.JSONObject;

import de.dfki.mycbr.core.DefaultCaseBase;
import de.dfki.mycbr.core.casebase.Instance;
import de.dfki.mycbr.core.model.Concept;
import de.dfki.mycbr.core.retrieval.Retrieval;
import de.dfki.mycbr.core.similarity.Similarity;
import de.dfki.mycbr.util.Pair;

import misc.JSONCreator;
import misc.MeanCreator;
import misc.Tools;
import mycbr.CBRConstants;
import mycbr.CBREngine;
import mycbr.CBRConstants.Distances.LanguageDistances;
import mycbr.similarity.AbstractDistance;
import mycbr.similarity.Distance;
import mycbr.similarity.DistanceFactory;

/**
 * A class that represents the language feature of a TwitDoc. The language
 * includes information about the distribution of pos-tags within the tweets of
 * the user, the use of emoticon, questions and exclamations and so on.
 * 
 * @author Diana Lange
 *
 */
public class Language extends AbstractDistance<Language> implements MeanCreator<Language, Language>, JSONCreator {

	/**
	 * The language style of the user (use of emoticon, questions, exclamations
	 * and quotes in tweets).
	 */
	private Style style;

	/**
	 * The word usage of the user (distribution of pos-tags in tweets, average
	 * word number per tweet, percentage of short tweets).
	 */
	private Words words;

	/**
	 * The outline of the user's language (distribution of languages in which
	 * the user tweets, number of tweets that contain hashtags).
	 */
	private Outline outline;

	/**
	 * The twitter id of the user.
	 */
	private String id;

	/**
	 * JSON version of this instance.
	 */
	private JSONObject json;

	/**
	 * Generates an empty language feature.
	 */
	protected Language() {
		this.json = null;
		this.id = "unkown";
		this.style = new Style();
		this.words = new Words();
		this.outline = new Outline();
	}

	/**
	 * Creates a new language feature.
	 * 
	 * @param style
	 *            Style feature of the user's language.
	 * @param words
	 *            Words feature of the user's language.
	 * @param outline
	 *            Outline feature of the user's language.
	 */
	private Language(Style style, Words words, Outline outline) {
		this.json = null;
		this.id = "unkown";
		this.style = style;
		this.outline = outline;
		this.words = words;
	}

	/**
	 * Creates a new language feature.
	 * 
	 * @param language
	 *            A JSONObject that contains the features style, words and
	 *            outline.
	 */
	public Language(JSONObject language) {
		this("unknown", language);
	}

	/**
	 * Creates a new language feature.
	 * 
	 * @param id
	 *            The twitter id of the user.
	 * @param language
	 *            A JSONObject that contains the features style, words and
	 *            outline.
	 */
	public Language(String id, JSONObject language) {
		this();
		try {
			this.id = id;
			style = new Style(language.getJSONObject("style"));
			words = new Words(language.getJSONObject("words"));
			outline = new Outline(language.getJSONObject("outline"));
		} catch (Exception e) {
			System.err.println("Problems parsing JSONObject to Language");
			System.err.println(language);
		}
	}

	/**
	 * Stores the Language in the casebase. The name of the new instance is
	 * "language_####" where #### stands for the twitter user id.
	 */
	public void save() {
		try {
			Concept concept = CBREngine.get().getConcept(CBRConstants.Concepts.LANGUAGE);
			DefaultCaseBase cb = CBREngine.get().getCasebase(CBRConstants.CB.LANGUAGE);

			Instance newInstance = concept.addInstance(Language.name() + "_" + getID());
			TwitDoc.setInstance(this, getID(), newInstance, concept);

			cb.addCase(newInstance);

		} catch (Exception e) {
			System.err.println(e);
		}
	}

	/**
	 * Finds cases that are similar to the current instance of Language.
	 * 
	 * @return A list of results (similar cases) sorted by their similarity
	 *         (beginning with the most similar case). The list may include also
	 *         the case that is exactly the same as the input instance of the
	 *         Language.
	 */
	public List<Pair<Instance, Similarity>> retrieve() {
		Concept concept = CBREngine.get().getConcept(CBRConstants.Concepts.LANGUAGE);
		DefaultCaseBase cb = CBREngine.get().getCasebase(CBRConstants.CB.LANGUAGE);

		Retrieval ret = new Retrieval(concept, cb);
		ret.setRetrievalMethod(Retrieval.RetrievalMethod.RETRIEVE_SORTED);

		Instance query = ret.getQueryInstance();
		TwitDoc.setInstance(this, getID(), query, concept);
		ret.start();

		return ret.getResult();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see misc.JSONCreator#json()
	 */
	public JSONObject json() {

		if (json == null) {
			json = new JSONObject();

			json.setJSONObject("style", style.json());
			json.setJSONObject("words", words.json());
			json.setJSONObject("outline", outline.json());
		}
		return json;
	}

	/**
	 * Getter.
	 * 
	 * @return The twitter user id or "unknown".
	 */
	public String getID() {
		return id;
	}

	/**
	 * Getter.
	 * 
	 * @return The language style of the user (use of emoticon, questions,
	 *         exclamations and quotes in tweets).
	 */
	public Style getStyle() {
		return style;
	}

	/**
	 * Getter.
	 * 
	 * @return The word usage of the user (distribution of pos-tags in tweets,
	 *         average word number per tweet, percentage of short tweets).
	 */
	public Words getWords() {
		return words;
	}

	/**
	 * Getter.
	 * 
	 * @return The outline of the user's language (distribution of languages in
	 *         which the user tweets, number of tweets that contain hashtags).
	 */
	public Outline getOutline() {
		return outline;
	}

	/**
	 * Getter.
	 * 
	 * @return "language".
	 */
	public static String name() {
		return "language";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see misc.MeanCreator#mean(java.util.ArrayList)
	 */
	@Override
	public Language mean(ArrayList<Language> elements) {
		return mean(elements.toArray(new Language[elements.size()]));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see misc.MeanCreator#mean(java.lang.Object[])
	 */
	@Override
	public Language mean(Language[] elements) {
		Outline o = outline.mean(elements);
		Words w = words.mean(elements);
		Style s = style.mean(elements);
		return new Language(s, w, o);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mycbr.similarity.AbstractDistance#get()
	 */
	@Override
	public Language get() {
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mycbr.similarity.AbstractDistance#distance(java.lang.Object,
	 * java.lang.Object)
	 */
	@Override
	public float distance(Language o, Language v) {
		return (o.outline.distance(v.outline) + o.words.distance(v.words) + o.style.distance(v.style)) / 3f;
	}

	/**
	 * Creates a distance calculator for Languages.
	 * 
	 * @return The Languages distance calculator.
	 */
	public static Distance<Language> getDistanceFct() {
		return (o, v) -> (o.outline.distance(v.outline) + o.words.distance(v.words) + o.style.distance(v.style)) / 3f;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		String txt = "language: { \n";
		txt += "\t" + style + "\n";
		txt += "\t" + words + "\n";
		txt += "\t" + outline + "\n";
		txt += "}";

		return txt;
	}

	/**
	 * A class that represents the outline feature of a Language.
	 * 
	 * @author Diana Lange
	 *
	 */
	public static class Outline extends AbstractDistance<Outline> implements MeanCreator<Outline, Language>, JSONCreator {

		/**
		 * (Percentaged) number of tweets which include hashtags.
		 */
		private double pHashtags;

		/**
		 * (Average) number of hashtags per tweet.
		 */
		private double avgHashtags;

		/**
		 * Map of languages used in tweets of the users. The key is the language
		 * (e.g. en, fr, ...) and the value is the percentage of tweets that are
		 * detected as this language.
		 */
		private HashMap<String, Double> languages;

		/**
		 * JSON version of this instance.
		 */
		private JSONObject json;

		/**
		 * Generates an empty outline feature.
		 */
		public Outline() {
			json = null;
			pHashtags = 0;
			avgHashtags = 0;
			languages = new HashMap<String, Double>();
		}

		/**
		 * Creates a new outline feature.
		 * 
		 * @param outline
		 *            A JSONObject that contains the features for this outline
		 *            object.
		 */
		public Outline(JSONObject outline) {
			this();
			try {

				JSONObject langs = outline.getJSONObject("languages");

				for (Object key : langs.keys()) {
					String k = (String) key;
					languages.put(k, langs.getDouble(k));
				}

				pHashtags = outline.getJSONObject("hashtags").getDouble("percentage");
				avgHashtags = outline.getJSONObject("hashtags").getDouble("average");
			} catch (Exception e) {
				System.err.println("Problems parsing JSONObject to Language.Outline");
				System.err.println(outline);
			}
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see misc.JSONCreator#json()
		 */
		public JSONObject json() {

			if (json == null) {
				json = new JSONObject();

				JSONObject hashtags = new JSONObject();
				JSONObject languages = new JSONObject();

				hashtags.setDouble("percentage", pHashtags);
				hashtags.setDouble("average", avgHashtags);

				for (Map.Entry<String, Double> entry : this.languages.entrySet()) {
					languages.setDouble(entry.getKey(), entry.getValue());
				}

				json.setJSONObject("hashtags", hashtags);
				json.setJSONObject("languages", languages);

			}
			return json;
		}

		/**
		 * Getter.
		 * 
		 * @return (Percentaged) number of tweets which include hashtags.
		 */
		public float getPercentageHashtags() {
			return (float) pHashtags;
		}

		/**
		 * Getter.
		 * 
		 * @return (Average) number of hashtags per tweet.
		 */
		public float getAverageHashtags() {
			return (float) avgHashtags;
		}

		/**
		 * Getter.
		 * 
		 * @return Map of languages used in tweets of the users. The key is the
		 *         language (e.g. en, fr, ...) and the value is the percentage
		 *         of tweets that are detected as this language.
		 */
		public HashMap<String, Double> getLanguages() {
			return languages;
		}

		/**
		 * Getter.
		 * 
		 * @return List of detected languages in the tweets of the user.
		 */
		public ArrayList<String> getLanguageNames() {
			return new ArrayList<String>(languages.keySet());
		}

		/**
		 * Gets percentage of tweets that are detected as the lookup language.
		 * 
		 * @param lang
		 *            The lookup language.
		 * @return The percentage of tweets that are detected as the lookup
		 *         language.
		 */
		public float getLanguagePercentage(String lang) {

			if (!languages.containsKey(lang)) {
				return 0f;
			}

			return languages.get(lang).floatValue();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see misc.MeanCreator#mean(java.util.ArrayList)
		 */
		@Override
		public Outline mean(ArrayList<Language> elements) {
			return mean(elements.toArray(new Language[elements.size()]));
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see misc.MeanCreator#mean(java.lang.Object[])
		 */
		@Override
		public Outline mean(Language[] elements) {
			Outline newElement = new Outline();

			for (Language element : elements) {
				newElement.avgHashtags += element.outline.avgHashtags;
				newElement.pHashtags += element.outline.pHashtags;

				for (Map.Entry<String, Double> entry : element.outline.languages.entrySet()) {
					String l = entry.getKey();
					Double p = entry.getValue() / elements.length;

					if (newElement.languages.containsKey(l)) {
						p += newElement.languages.get(l);
					}
					newElement.languages.put(l, p);
				}
			}

			if (elements.length == 0) {
				return newElement;
			}

			newElement.avgHashtags /= elements.length;
			newElement.pHashtags /= elements.length;

			return newElement;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see mycbr.similarity.AbstractDistance#get()
		 */
		@Override
		public Outline get() {
			return this;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see mycbr.similarity.AbstractDistance#distance(java.lang.Object,
		 * java.lang.Object)
		 */
		@Override
		public float distance(Outline o, Outline v) {
			
			// get distance function for each attribute & calculate distance for the attribute
			float dPHashtags = DistanceFactory.getSimpleFloatDistance(LanguageDistances.OUTLINE_PHASHTAGS_DISTANCE)
					.distance(o.getPercentageHashtags(), v.getPercentageHashtags());
			float dAvgHashtags = DistanceFactory.getSimpleFloatDistance(LanguageDistances.OUTLINE_AVGHASHTAGS_DISTANCE)
					.distance(o.getAverageHashtags(), v.getAverageHashtags());

			// Distance<String> langD =
			// DistanceFactory.getSimpleStringDistance(LanguageDistances.OUTLINE_LANGUAGES_COUNTRY_DISTANCE);
			Distance<Float> langPD = DistanceFactory
					.getSimpleFloatDistance(LanguageDistances.OUTLINE_LANGUAGES_P_DISTANCE);
			int keysNum = 0;
			float dlanguages = 0;

			for (Map.Entry<String, Double> entry : o.languages.entrySet()) {
				String key = entry.getKey();
				Double value = entry.getValue();

				if (v.languages.containsKey(key)) {
					dlanguages += langPD.distance(value.floatValue(), v.languages.get(key).floatValue());
				} else {
					dlanguages += 1;
				}
				keysNum++;
			}

			dlanguages /= keysNum;

			return 0.25f * dPHashtags + 0.25f * dAvgHashtags + 0.5f * dlanguages;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#toString()
		 */
		public String toString() {

			String txt = "";
			txt += "outline: { ";
			txt += "languages : " + Tools.join(languages, ", ", d -> d.floatValue()) + ", ";
			txt += "hashtags percentage : " + getPercentageHashtags() + ", ";
			txt += "hashtags average : " + getAverageHashtags();
			txt += " }";
			return txt;
		}
	}

	/**
	 * A class that represents the word feature of a Language.
	 * 
	 * @author Diana Lange
	 *
	 */
	public static class Words extends AbstractDistance<Words> implements MeanCreator<Words, Language>, JSONCreator {

		/**
		 * (Percentaged) number of tweets that are empty if at-mentions, urls,
		 * hashtags and other media elements are filtered out.
		 */
		private double pEmptyTweets;

		/**
		 * (Percentaged) number of tweets that are short (contain up to three
		 * words).
		 */
		private double pShortTweets;

		/**
		 * (Percentaged) number of words / tokens in a tweet.
		 */
		private double avgTweetLength;

		/**
		 * (Percentaged) number nouns per tweet.
		 */
		private double pNoun;

		/**
		 * (Percentaged) number adjectives per tweet.
		 */
		private double pAdjective;

		/**
		 * (Percentaged) number verbs per tweet.
		 */
		private double pVerb;

		/**
		 * (Percentaged) number adverbs per tweet.
		 */
		private double pAdverb;

		/**
		 * (Percentaged) number numbers per tweet.
		 */
		private double pNumber;

		/**
		 * (Percentaged) number whs (e.g. "who", "how", ...) per tweet.
		 */
		private double pWh;

		/**
		 * JSON version of this instance.
		 */
		private JSONObject json;

		/**
		 * Generates an empty word feature.
		 */
		private Words() {
			json = null;
			pEmptyTweets = 0;
			pShortTweets = 0;
			avgTweetLength = 0;
			pNoun = 0;
			pAdjective = 0;
			pVerb = 0;
			pAdverb = 0;
			pNumber = 0;
			pWh = 0;
		}

		/**
		 * Creates a new word feature.
		 * 
		 * @param words
		 *            A JSONObject that contains the features for this word
		 *            object.
		 */
		public Words(JSONObject words) {
			this();
			try {
				pEmptyTweets = words.getJSONObject("tweetlength").getDouble("empty");
				pShortTweets = words.getJSONObject("tweetlength").getDouble("short");
				avgTweetLength = words.getJSONObject("tweetlength").getDouble("average");

				JSONObject semantics = words.getJSONObject("semantics");

				pNoun = semantics.hasKey("noun") ? semantics.getDouble("noun") : 0d;
				pAdjective = semantics.hasKey("adjective") ? semantics.getDouble("adjective") : 0d;
				pVerb = semantics.hasKey("verb") ? semantics.getDouble("verb") : 0d;
				pAdverb = semantics.hasKey("adverb") ? semantics.getDouble("adverb") : 0d;
				pNumber = semantics.hasKey("number") ? semantics.getDouble("number") : 0d;
				pWh = semantics.hasKey("wh") ? semantics.getDouble("wh") : 0d;
			} catch (Exception e) {
				System.err.println("Problems parsing JSONObject to Language.Words");
				System.err.println(words);
			}
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see misc.JSONCreator#json()
		 */
		public JSONObject json() {
			if (json == null) {
				json = new JSONObject();

				JSONObject tweetlength = new JSONObject();
				JSONObject semantics = new JSONObject();

				tweetlength.setDouble("empty", pEmptyTweets);
				tweetlength.setDouble("short", pShortTweets);
				tweetlength.setDouble("average", avgTweetLength);
				semantics.setDouble("noun", pNoun);
				semantics.setDouble("adjective", pAdjective);
				semantics.setDouble("verb", pVerb);
				semantics.setDouble("adverb", pAdverb);
				semantics.setDouble("number", pNumber);
				semantics.setDouble("wh", pWh);

				json.setJSONObject("tweetlength", tweetlength);
				json.setJSONObject("semantics", semantics);
			}
			return json;
		}

		/**
		 * Getter.
		 * 
		 * @return (Percentaged) number of tweets that are empty if at-mentions,
		 *         urls, hashtags and other media elements are filtered out.
		 */
		public float getPercentageEmptyTweets() {
			return (float) pEmptyTweets;
		}

		/**
		 * Getter.
		 * 
		 * @return (Percentaged) number of tweets that are short (contain up to
		 *         three words).
		 */
		public float getPercentageShortTweets() {
			return (float) pShortTweets;
		}

		/**
		 * Getter.
		 * 
		 * @return (Percentaged) number of words / tokens in a tweet.
		 */
		public float getAverageTweetLength() {
			return (float) avgTweetLength;
		}

		/**
		 * Getter.
		 * 
		 * @return (Percentaged) number nouns per tweet.
		 */
		public float getPercentageNoun() {
			return (float) pNoun;
		}

		/**
		 * Getter.
		 * 
		 * @return (Percentaged) number adjectives per tweet.
		 */
		public float getPercentageAdjective() {
			return (float) pAdjective;
		}

		/**
		 * Getter.
		 * 
		 * @return (Percentaged) number verbs per tweet.
		 */
		public float getPercentageVerb() {
			return (float) pVerb;
		}

		/**
		 * Getter.
		 * 
		 * @return (Percentaged) number adverbs per tweet.
		 */
		public float getPercentageAdverb() {
			return (float) pAdverb;
		}

		/**
		 * Getter.
		 * 
		 * @return (Percentaged) number numbers per tweet.
		 */
		public float getPercentageNumber() {
			return (float) pNumber;
		}

		/**
		 * Getter.
		 * 
		 * @return (Percentaged) number whs (e.g. "who", "how", ...) per tweet.
		 */
		public float getPercentageWh() {
			return (float) pWh;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see misc.MeanCreator#mean(java.util.ArrayList)
		 */
		@Override
		public Words mean(ArrayList<Language> elements) {
			return mean(elements.toArray(new Language[elements.size()]));
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see misc.MeanCreator#mean(java.lang.Object[])
		 */
		@Override
		public Words mean(Language[] elements) {
			Words newElement = new Words();

			for (Language element : elements) {
				newElement.pEmptyTweets += element.words.pEmptyTweets;
				newElement.pShortTweets += element.words.pShortTweets;
				newElement.avgTweetLength += element.words.avgTweetLength;
				newElement.pNoun += element.words.pNoun;
				newElement.pAdjective += element.words.pAdjective;
				newElement.pVerb += element.words.pVerb;
				newElement.pAdverb += element.words.pAdverb;
				newElement.pNumber += element.words.pNumber;
				newElement.pWh += element.words.pWh;
			}

			if (elements.length == 0) {
				return newElement;
			}

			newElement.pEmptyTweets /= elements.length;
			newElement.pShortTweets /= elements.length;
			newElement.avgTweetLength /= elements.length;
			newElement.pNoun /= elements.length;
			newElement.pAdjective /= elements.length;
			newElement.pVerb /= elements.length;
			newElement.pAdverb /= elements.length;
			newElement.pNumber /= elements.length;
			newElement.pWh /= elements.length;
			return newElement;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see mycbr.similarity.AbstractDistance#get()
		 */
		@Override
		public Words get() {
			return this;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see mycbr.similarity.AbstractDistance#distance(java.lang.Object,
		 * java.lang.Object)
		 */
		@Override
		public float distance(Words o, Words v) {
			// get distance function for each attribute & calculate distance for the attribute
			float dPEmptyTweets = DistanceFactory.getSimpleFloatDistance(LanguageDistances.WORDS_EMPTY_TWEETS_DISTANCE)
					.distance(o.getPercentageEmptyTweets(), v.getPercentageEmptyTweets());
			float dPShortTweets = DistanceFactory.getSimpleFloatDistance(LanguageDistances.WORDS_SHORT_TWEETS_DISTANCE)
					.distance(o.getPercentageShortTweets(), v.getPercentageShortTweets());
			float dAvgTweets = DistanceFactory.getSimpleFloatDistance(LanguageDistances.WORDS_AVGWORDS_DISTANCE)
					.distance(o.getAverageTweetLength(), v.getAverageTweetLength());
			float dNouns = DistanceFactory.getSimpleFloatDistance(LanguageDistances.WORDS_SEMANTICS_NOUN_DISTANCE)
					.distance(o.getPercentageNoun(), v.getPercentageNoun());
			float dAdjectives = DistanceFactory
					.getSimpleFloatDistance(LanguageDistances.WORDS_SEMANTICS_ADJECTIVE_DISTANCE)
					.distance(o.getPercentageAdjective(), v.getPercentageAdjective());
			float dVerbs = DistanceFactory.getSimpleFloatDistance(LanguageDistances.WORDS_SEMANTICS_VERB_DISTANCE)
					.distance(o.getPercentageVerb(), v.getPercentageVerb());
			float dAdverbs = DistanceFactory.getSimpleFloatDistance(LanguageDistances.WORDS_SEMANTICS_ADVERB_DISTANCE)
					.distance(o.getPercentageAdverb(), v.getPercentageAdverb());
			float dNumbers = DistanceFactory.getSimpleFloatDistance(LanguageDistances.WORDS_SEMANTICS_NUMBER_DISTANCE)
					.distance(o.getPercentageNumber(), v.getPercentageNumber());
			float dWHs = DistanceFactory.getSimpleFloatDistance(LanguageDistances.WORDS_SEMANTICS_WH_DISTANCE)
					.distance(o.getPercentageWh(), v.getPercentageWh());

			return 0.5f * ((dPEmptyTweets + dPShortTweets + dAvgTweets) / 3f
					+ (dNouns + dAdjectives + dVerbs + dAdverbs + dNumbers + dWHs) / 6f);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#toString()
		 */
		public String toString() {
			String txt = "";
			txt += "words: { ";
			txt += "tweetlength : " + getPercentageEmptyTweets() + " (empty), " + getPercentageShortTweets()
					+ " (short), " + getAverageTweetLength() + " (average word number), ";
			txt += "semantics : " + getPercentageNoun() + " (nouns), " + getPercentageAdjective() + " (adjectives), "
					+ getPercentageVerb() + " (verb), " + getPercentageAdverb() + " (adverb), " + getPercentageNumber()
					+ " (number), " + getPercentageWh() + " (wh)";
			txt += " }";
			return txt;
		}

	}

	/**
	 * A class that represents the style feature of a Language.
	 * 
	 * @author Diana Lange
	 *
	 */
	public static class Style extends AbstractDistance<Style> implements MeanCreator<Style, Language>, JSONCreator {

		/**
		 * (Percentaged) number of tweets that contain emoticon (smileys or
		 * emojis).
		 */
		private double emoticon;

		/**
		 * (Percentaged) number of tweets that contain quotes.
		 */
		private double quotes;

		/**
		 * (Percentaged) number of tweets that contain questions.
		 */
		private double questions;

		/**
		 * (Percentaged) number of tweets that contain exclamations.
		 */
		private double exclamations;

		/**
		 * JSON version of this instance.
		 */
		private JSONObject json;

		/**
		 * Generates an empty style feature.
		 */
		private Style() {
			this.json = null;
			this.emoticon = 0;
			this.quotes = 0;
			this.questions = 0;
			this.exclamations = 0;
		}

		/**
		 * Creates a new style feature.
		 * 
		 * @param style
		 *            A JSONObject that contains the features for this style
		 *            object.
		 */
		public Style(JSONObject style) {
			this();
			try {
				emoticon = style.getDouble("emoticon");
				quotes = style.getDouble("quotes");
				questions = style.getDouble("questions");
				exclamations = style.getDouble("exclamations");
			} catch (Exception e) {
				System.err.println("Problems parsing JSONObject to Language.Style");
				System.err.println(style);
			}
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see misc.JSONCreator#json()
		 */
		public JSONObject json() {

			if (json == null) {
				json = new JSONObject();
				json.setDouble("emoticon", emoticon);
				json.setDouble("quotes", quotes);
				json.setDouble("questions", questions);
				json.setDouble("exclamations", exclamations);

			}
			return json;
		}

		/**
		 * Getter.
		 * 
		 * @return (Percentaged) number of tweets that contain emoticon (smileys
		 *         or emojis).
		 */
		public float getEmoticon() {
			return (float) emoticon;
		}

		/**
		 * Getter.
		 * 
		 * @return (Percentaged) number of tweets that contain quotes.
		 */
		public float getQuotes() {
			return (float) quotes;
		}

		/**
		 * Getter.
		 * 
		 * @return (Percentaged) number of tweets that contain questions.
		 */
		public float getQuestions() {
			return (float) questions;
		}

		/**
		 * Getter.
		 * 
		 * @return (Percentaged) number of tweets that contain exclamations.
		 */
		public float getExclamations() {
			return (float) exclamations;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see misc.MeanCreator#mean(java.util.ArrayList)
		 */
		@Override
		public Style mean(ArrayList<Language> elements) {
			return mean(elements.toArray(new Language[elements.size()]));
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see misc.MeanCreator#mean(java.lang.Object[])
		 */
		@Override
		public Style mean(Language[] elements) {
			Style newElement = new Style();

			for (Language element : elements) {
				newElement.emoticon = element.style.emoticon;
				newElement.quotes = element.style.quotes;
				newElement.questions = element.style.questions;
				newElement.exclamations = element.style.exclamations;
			}

			if (elements.length == 0) {
				return newElement;
			}

			newElement.emoticon /= elements.length;
			newElement.quotes /= elements.length;
			newElement.questions /= elements.length;
			newElement.exclamations /= elements.length;

			return newElement;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see mycbr.similarity.AbstractDistance#get()
		 */
		@Override
		public Style get() {
			return this;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see mycbr.similarity.AbstractDistance#distance(java.lang.Object,
		 * java.lang.Object)
		 */
		@Override
		public float distance(Style o, Style v) {
			// get distance function for each attribute & calculate distance for the attribute
			float dEmoticon = DistanceFactory.getSimpleFloatDistance(LanguageDistances.STYLE_EMOTICON_DISTANCE)
					.distance(o.getEmoticon(), v.getEmoticon());
			float dQuotes = DistanceFactory.getSimpleFloatDistance(LanguageDistances.STYLE_QUOTES_DISTANCE)
					.distance(o.getQuotes(), v.getQuotes());
			float dQuestions = DistanceFactory.getSimpleFloatDistance(LanguageDistances.STYLE_QUESTIONS_DISTANCE)
					.distance(o.getQuestions(), v.getQuestions());
			float dExlamations = DistanceFactory.getSimpleFloatDistance(LanguageDistances.STYLE_EXCLAMATIONS_DISTANCE)
					.distance(o.getExclamations(), v.getExclamations());
			return 0.25f * (dEmoticon + dQuotes + dQuestions + dExlamations);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#toString()
		 */
		public String toString() {
			String txt = "";
			txt += "style: { ";
			txt += "emoticon percentage : " + getEmoticon() + ", ";
			txt += "quotes percentage : " + getQuotes() + ", ";
			txt += "questions percentage : " + getQuestions() + ", ";
			txt += "exclamations percentage : " + getExclamations() + " }";
			return txt;
		}

	}

}
