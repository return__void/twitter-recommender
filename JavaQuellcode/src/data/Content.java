package data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import processing.data.JSONArray;
import processing.data.JSONObject;

import de.dfki.mycbr.core.DefaultCaseBase;
import de.dfki.mycbr.core.casebase.Instance;
import de.dfki.mycbr.core.model.Concept;
import de.dfki.mycbr.core.retrieval.Retrieval;
import de.dfki.mycbr.core.similarity.Similarity;
import de.dfki.mycbr.util.Pair;

import misc.JSONCreator;
import misc.MeanCreator;
import misc.Tools;
import mycbr.CBRConstants;
import mycbr.CBREngine;
import mycbr.CBRConstants.Distances.ContentDistances;
import mycbr.similarity.AbstractDistance;
import mycbr.similarity.Distance;
import mycbr.similarity.DistanceFactory;

/**
 * A class that represents the content feature of a TwitDoc. The content
 * includes information about which keywords and hashtags the user used in the
 * his / her tweets and which media the tweets include (urls, videos, photos,
 * ...).
 * 
 * @author Diana Lange
 *
 */
public class Content extends AbstractDistance<Content> implements MeanCreator<Content, Content>, JSONCreator {

	/**
	 * The about content feature of the user (hashtags and keywords).
	 */
	private About about;

	/**
	 * The media content feature of the user (urls, photos, videos, ...).
	 */
	private Media media;

	/**
	 * The twitter id of the user.
	 */
	private String id;

	/**
	 * JSON version of this instance.
	 */
	private JSONObject json;

	/**
	 * Generates an empty content feature.
	 */
	protected Content() {
		json = null;
		id = "unknown";
		this.about = new About();
		this.media = new Media();
	}

	/**
	 * Creates a new content feature.
	 * 
	 * @param about
	 *            About feature of the user's content.
	 * @param media
	 *            Media feature of the user's content.
	 */
	private Content(About about, Media media) {
		this();
		this.about = about;
		this.media = media;
	}

	/**
	 * Creates a new content feature.
	 * 
	 * @param content
	 *            A JSONObject that contains the features about and media.
	 */
	public Content(JSONObject content) {
		this("unknown", content);
	}

	/**
	 * Creates a new content feature.
	 * 
	 * @param id
	 *            The twitter id of the user.
	 * @param content
	 *            A JSONObject that contains the features about and media.
	 */
	public Content(String id, JSONObject content) {
		this();
		this.id = id;

		try {
			about = new About(content.getJSONObject("about"));
			media = new Media(content.getJSONObject("media"));
		} catch (Exception e) {
			System.err.println("Problems parsing JSONObject to Content");
			System.err.println(content);
		}
	}

	/**
	 * Stores the Conent in the casebase. The name of the new instance is
	 * "content_####" where #### stands for the twitter user id.
	 */
	public void save() {
		try {
			Concept concept = CBREngine.get().getConcept(CBRConstants.Concepts.CONTENT);
			DefaultCaseBase cb = CBREngine.get().getCasebase(CBRConstants.CB.CONTENT);

			Instance newInstance = concept.addInstance(Content.name() + "_" + getID());
			TwitDoc.setInstance(this, getID(), newInstance, concept);

			cb.addCase(newInstance);

		} catch (Exception e) {
			System.err.println(e);
		}
	}

	/**
	 * Finds cases that are similar to the current instance of Content.
	 * 
	 * @return A list of results (similar cases) sorted by their similarity
	 *         (beginning with the most similar case). The list may include also
	 *         the case that is exactly the same as the input instance of the
	 *         Content.
	 */
	public List<Pair<Instance, Similarity>> retrieve() {
		Concept concept = CBREngine.get().getConcept(CBRConstants.Concepts.CONTENT);
		DefaultCaseBase cb = CBREngine.get().getCasebase(CBRConstants.CB.CONTENT);

		Retrieval ret = new Retrieval(concept, cb);
		ret.setRetrievalMethod(Retrieval.RetrievalMethod.RETRIEVE_SORTED);

		Instance query = ret.getQueryInstance();
		TwitDoc.setInstance(this, getID(), query, concept);
		ret.start();

		return ret.getResult();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see misc.JSONCreator#json()
	 */
	public JSONObject json() {

		if (json == null) {
			json = new JSONObject();

			json.setJSONObject("about", about.json());
			json.setJSONObject("media", media.json());

		}
		return json;
	}

	/**
	 * Getter.
	 * 
	 * @return The twitter user id or "unknown".
	 */
	public String getID() {
		return id;
	}

	/**
	 * Getter.
	 * 
	 * @return The about content feature of the user (hashtags and keywords).
	 */
	public About getAbout() {
		return about;
	}

	/**
	 * Getter.
	 * 
	 * @return The media content feature of the user (urls, photos, videos,
	 *         ...).
	 */
	public Media getMedia() {
		return media;
	}

	/**
	 * Getter.
	 * 
	 * @return "content"
	 */
	public static String name() {
		return "content";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see misc.MeanCreator#mean(java.util.ArrayList)
	 */
	@Override
	public Content mean(ArrayList<Content> elements) {
		return mean(elements.toArray(new Content[elements.size()]));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see misc.MeanCreator#mean(java.lang.Object[])
	 */
	@Override
	public Content mean(Content[] elements) {
		About a = about.mean(elements);
		Media m = media.mean(elements);
		return new Content(a, m);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mycbr.similarity.AbstractDistance#get()
	 */
	@Override
	public Content get() {
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mycbr.similarity.AbstractDistance#distance(java.lang.Object,
	 * java.lang.Object)
	 */
	@Override
	public float distance(Content o, Content v) {
		return 0.5f * (o.about.distance(v.about) + o.media.distance(v.media));
	}

	/**
	 * Creates a distance calculator for instances of Content.
	 * 
	 * @return The Content distance calculator.
	 */
	public static Distance<Content> getDistanceFct() {
		return (o, v) -> 0.5f * (o.about.distance(v.about) + o.media.distance(v.media));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {

		String txt = "content: { \n";
		txt += "\t" + about + "\n";
		txt += "\t" + media + "\n";
		txt += "}";
		return txt;
	}

	/**
	 * A class that represents the media feature of a Content.
	 * 
	 * @author Diana Lange
	 *
	 */
	public static class Media extends AbstractDistance<Media> implements MeanCreator<Media, Content>, JSONCreator {

		/**
		 * (Percentaged) number of tweets which include urls.
		 */
		private double urls;

		/**
		 * (Percentaged) number of tweets which include videos.
		 */
		private double videos;

		/**
		 * (Percentaged) number of tweets which include photos.
		 */
		private double photos;

		/**
		 * (Percentaged) number of tweets which include gifs.
		 */
		private double gifs;

		/**
		 * List of domains that the user refers to in his / her tweets (e.g.
		 * facebook, uni-hildesheim, ...). Keys and values are both the domain
		 * names.
		 */
		private HashMap<String, String> domains;

		/**
		 * JSON version of this instance.
		 */
		private JSONObject json;

		/**
		 * Generates an empty media feature.
		 */
		public Media() {
			json = null;
			domains = new HashMap<String, String>();
			urls = 0;
			videos = 0;
			photos = 0;
			gifs = 0;
		}

		/**
		 * Creates a new media feature.
		 * 
		 * @param media
		 *            A JSONObject that contains the features for this media
		 *            instance.
		 */
		public Media(JSONObject media) {
			this();

			try {
				json = null;
				urls = media.getDouble("urls");
				videos = media.getDouble("videos");
				photos = media.getDouble("photos");
				gifs = media.getDouble("gifs");

				JSONArray domainArray = media.getJSONArray("domains");

				for (int i = 0; i < domainArray.size(); i++) {
					domains.put(domainArray.getString(i), domainArray.getString(i));
				}
			} catch (Exception e) {
				System.err.println("Problems parsing JSONObject to Content.Media");
				System.err.println(media);
			}
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see misc.JSONCreator#json()
		 */
		public JSONObject json() {

			if (json == null) {
				json = new JSONObject();

				json.setDouble("urls", urls);
				json.setDouble("videos", videos);
				json.setDouble("photos", photos);
				json.setDouble("gifs", gifs);

				JSONArray domainArray = new JSONArray();

				for (Map.Entry<String, String> entry : domains.entrySet()) {
					domainArray.append(entry.getValue());
				}

				json.setJSONArray("domains", domainArray);
			}
			return json;
		}

		/**
		 * Getter.
		 * 
		 * @return (Percentaged) number of tweets which include urls.
		 */
		public float getUrls() {
			return (float) urls;
		}

		/**
		 * Getter.
		 * 
		 * @return (Percentaged) number of tweets which include videos.
		 */
		public float getVideos() {
			return (float) videos;
		}

		/**
		 * Getter.
		 * 
		 * @return (Percentaged) number of tweets which include photos.
		 */
		public float getPhotos() {
			return (float) photos;
		}

		/**
		 * Getter.
		 * 
		 * @return (Percentaged) number of tweets which include gifs.
		 */
		public float getGifs() {
			return (float) gifs;
		}

		/**
		 * Getter.
		 * 
		 * @return List of domains that the user refers to in his / her tweets
		 *         (e.g. facebook, uni-hildesheim, ...). Keys and values are
		 *         both the domain names.
		 */
		public HashMap<String, String> getDomains() {
			return domains;
		}

		/**
		 * Getter.
		 * 
		 * @return List of domains that the user refers to in his / her tweets
		 *         (e.g. facebook, uni-hildesheim, ...).
		 */
		public String[] getDomainArray() {
			return domains.keySet().toArray(new String[domains.keySet().size()]);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see misc.MeanCreator#mean(java.util.ArrayList)
		 */
		@Override
		public Media mean(ArrayList<Content> elements) {
			return mean(elements.toArray(new Content[elements.size()]));
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see misc.MeanCreator#mean(java.lang.Object[])
		 */
		@Override
		public Media mean(Content[] elements) {
			Media newElement = new Media();

			for (Content element : elements) {
				newElement.urls += element.media.urls;
				newElement.videos += element.media.videos;
				newElement.photos += element.media.photos;
				newElement.gifs += element.media.gifs;
				for (Map.Entry<String, String> entry : element.media.domains.entrySet()) {
					newElement.domains.put(entry.getKey(), entry.getValue());
				}
			}

			if (elements.length == 0) {
				return newElement;
			}

			newElement.urls /= elements.length;
			newElement.videos /= elements.length;
			newElement.photos /= elements.length;
			newElement.gifs /= elements.length;

			return newElement;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see mycbr.similarity.AbstractDistance#get()
		 */
		@Override
		public Media get() {
			return this;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see mycbr.similarity.AbstractDistance#distance(java.lang.Object,
		 * java.lang.Object)
		 */
		@Override
		public float distance(Media o, Media v) {
			// get distance function for each attribute & calculate distance for the attribute
			float dURLS = DistanceFactory.getSimpleFloatDistance(ContentDistances.MEDIA_URL_DISTANCE)
					.distance(o.getUrls(), v.getUrls());
			float dVideos = DistanceFactory.getSimpleFloatDistance(ContentDistances.MEDIA_VIDEOS_DISTANCE)
					.distance(o.getUrls(), v.getUrls());
			float dPhotos = DistanceFactory.getSimpleFloatDistance(ContentDistances.MEDIA_PHOTOS_DISTANCE)
					.distance(o.getUrls(), v.getUrls());
			float dGifs = DistanceFactory.getSimpleFloatDistance(ContentDistances.MEDIA_GIFS_DISTANCE)
					.distance(o.getUrls(), v.getUrls());
			float dDomains = 0;

			if (o.domains.isEmpty() && v.domains.isEmpty()) {
				dDomains = 0;
			} else if (o.domains.isEmpty() || v.domains.isEmpty()) {
				dDomains = 1;
			} else {
				int keyNum = 0;

				for (Map.Entry<String, String> entry : o.domains.entrySet()) {
					String key = entry.getKey();

					if (!v.domains.containsKey(key)) {
						dDomains++;
					}

					keyNum++;
				}

				dDomains /= keyNum;
			}

			return dDomains * 0.3f + 0.7f * ((dURLS + dVideos + dPhotos + dGifs) / 4.0f);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#toString()
		 */
		public String toString() {
			String txt = "";

			txt += "media: { ";
			txt += "domains : [" + Tools.join(getDomains(), ", ") + "], ";
			txt += "urls: " + this.getUrls() + ", ";
			txt += "videos: " + this.getVideos() + ", ";
			txt += "photos: " + this.getPhotos() + ", ";
			txt += "gifs: " + this.getGifs();
			txt += " }";

			return txt;
		}

	}

	/**
	 * A class that represents the about feature of a Content.
	 * 
	 * @author Diana Lange
	 *
	 */
	public static class About extends AbstractDistance<About> implements MeanCreator<About, Content>, JSONCreator {

		/**
		 * Hashtags which the user uses in his / her tweets. Similar hashtags
		 * (e.g. #Inktober and #Inktober2017) are grouped with WordGroups.
		 */
		private ArrayList<WordGroup> hashtags;

		/**
		 * Keywords which the user uses in his / her tweets regularly. Similar
		 * keywords (e.g. circle and circles) are grouped with WordGroups.
		 * Keywords are detected, regularly used nouns.
		 */
		private ArrayList<WordGroup> keywords;

		/**
		 * JSON version of this instance.
		 */
		private JSONObject json;

		/**
		 * Generates an empty about feature.
		 */
		private About() {
			json = null;
			hashtags = new ArrayList<WordGroup>();
			keywords = new ArrayList<WordGroup>();
		}

		/**
		 * Creates a new about feature.
		 * 
		 * @param about
		 *            A JSONObject that contains the features for this about
		 *            instance.
		 */
		public About(JSONObject about) {
			this();

			try {

				JSONObject ht = about.getJSONObject("hashtags");

				for (Object key : ht.keys()) {
					String k = (String) key;
					JSONObject entry = ht.getJSONObject(k);

					WordGroup newGroup = new WordGroup(k, entry);

					hashtags.add(newGroup);
				}

				JSONObject kw = about.getJSONObject("keywords");

				for (Object key : kw.keys()) {
					String k = (String) key;
					JSONObject entry = kw.getJSONObject(k);

					WordGroup newGroup = new WordGroup(k, entry);

					keywords.add(newGroup);
				}

				// may cause java.lang.IllegalArgumentException, I guess because
				// it may not be symmetric, Exception should not happen anymore.
				// Keep catch for safety.
				// ignore exception, sort will then be not correct though
				try {

					if (hashtags.size() > 1) {
						Collections.sort(hashtags, (w1, w2) -> w1.getCount() == w2.getCount() ? 0
								: w1.getCount() < w2.getCount() ? 1 : -1);
					}
					if (keywords.size() > 1) {
						Collections.sort(keywords, (w1, w2) -> w1.getCount() == w2.getCount() ? 0
								: w1.getCount() < w2.getCount() ? 1 : -1);
					}
				} catch (Exception e) {

				}
			} catch (Exception e) {
				System.err.println("Problems parsing JSONObject to Content.About");
				System.err.println(about);
			}
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see misc.JSONCreator#json()
		 */
		public JSONObject json() {
			if (json == null) {
				json = new JSONObject();

				JSONObject hashtagsObj = new JSONObject();
				JSONObject keywordsObj = new JSONObject();

				for (WordGroup gp : hashtags) {
					hashtagsObj.setJSONObject(gp.getRepresentant(), gp.json());
				}

				for (WordGroup gp : keywords) {
					keywordsObj.setJSONObject(gp.getRepresentant(), gp.json());
				}

				json.setJSONObject("hashtags", hashtagsObj);
				json.setJSONObject("keywords", keywordsObj);
			}
			return json;
		}

		/**
		 * Getter.
		 * 
		 * @return Hashtags which the user uses in his / her tweets. Similar
		 *         hashtags (e.g. #Inktober and #Inktober2017) are grouped with
		 *         WordGroups.
		 */
		public ArrayList<WordGroup> getHashtags() {
			return this.hashtags;
		}

		/**
		 * Getter.
		 * 
		 * @return Keywords which the user uses in his / her tweets regularly.
		 *         Similar keywords (e.g. circle and circles) are grouped with
		 *         WordGroups. Keywords are detected, regularly used nouns.
		 */
		public ArrayList<WordGroup> getKeywords() {
			return this.keywords;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see misc.MeanCreator#mean(java.util.ArrayList)
		 */
		@Override
		public About mean(ArrayList<Content> elements) {
			return mean(elements.toArray(new Content[elements.size()]));
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see misc.MeanCreator#mean(java.lang.Object[])
		 */
		@Override
		public About mean(Content[] elements) {
			About newElement = new About();

			for (Content e : elements) {
				if (e.about.hashtags.size() > 0) {
					newElement.hashtags.add(e.about.hashtags.get(0));
				}

				if (e.about.keywords.size() > 0) {
					newElement.keywords.add(e.about.keywords.get(0));
				}
			}

			return newElement;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see mycbr.similarity.AbstractDistance#get()
		 */
		@Override
		public About get() {
			return this;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see mycbr.similarity.AbstractDistance#distance(java.lang.Object,
		 * java.lang.Object)
		 */
		@Override
		public float distance(About o, About v) {
			// get distance function for each attribute & calculate distance for the attribute
			Distance<ArrayList<WordGroup>> hashtagFct = DistanceFactory.getWordGroupDistance(
					DistanceFactory.getSimpleStringDistance(ContentDistances.ABOUT_HASHTAGS_WORD_DISTANCE), 0.35f);// (LanguageDistances.STYLE_EMOTICON_DISTANCE).distance(o.getEmoticon(),
																													// v.getEmoticon());
			Distance<ArrayList<WordGroup>> keywordsFct = DistanceFactory.getWordGroupDistance(
					DistanceFactory.getSimpleStringDistance(ContentDistances.ABOUT_KEYWORDS_WORD_DISTANCE), 0.35f);// (LanguageDistances.STYLE_EMOTICON_DISTANCE).distance(o.getEmoticon(),
																													// v.getEmoticon());

			float dHashtags = hashtagFct.distance(o.hashtags, v.hashtags);
			float dKeywords = keywordsFct.distance(o.hashtags, v.hashtags);

			return 0.6f * dHashtags + 0.4f * dKeywords;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#toString()
		 */
		public String toString() {
			String txt = "";

			txt += "about: { ";
			txt += "hashtags : [" + Tools.join(hashtags, ", ",
					hashtag -> "#" + hashtag.getRepresentant() + "(" + hashtag.getCount() + "x)") + "], ";
			txt += "keywords : ["
					+ Tools.join(keywords, ", ", keyword -> keyword.getRepresentant() + "(" + keyword.getCount() + "x)")
					+ "], ";

			txt += " }";

			return txt;
		}

	}

}
