package data;

import java.util.HashMap;
import java.util.Map;

import processing.data.JSONArray;
import processing.data.JSONObject;

import de.dfki.mycbr.core.DefaultCaseBase;
import de.dfki.mycbr.core.casebase.Instance;
import de.dfki.mycbr.core.model.Concept;
import misc.JSONCreator;
import misc.Tools;
import mycbr.CBRConstants;
import mycbr.CBREngine;

/**
 * A class that represents the general feature of a TwitDoc. The general feature
 * is not used for any similarity calculations but includes the twitter id of
 * the user that is represented in the TwitDoc. Instances may also include the
 * ids of the user's twitter friends (people the user follows on twitter).
 * 
 * @author Diana Lange
 *
 */
public class General implements JSONCreator {

	/**
	 * The twitter id of the user.
	 */
	private String twitterID;

	/**
	 * The ids of the friends of the user.
	 */
	private HashMap<String, String> friendsIDs;

	/**
	 * The ids of the friends of the user.
	 */
	private JSONArray jsonFriends;

	/**
	 * JSON version of this instance.
	 */
	private JSONObject json;

	/**
	 * Number of friends.
	 */
	private int friendsSize;

	/**
	 * Generates an empty general feature.
	 */
	protected General() {
		friendsSize = 0;
		json = null;
		twitterID = "unknown";
		friendsIDs = null;
		jsonFriends = null;
	}

	/**
	 * Creates a new general feature.
	 * 
	 * @param jsonText
	 *            A text representing a JSONObject that contains the attributes
	 *            for this feature.
	 */
	public General(String jsonText) {
		this(JSONObject.parse(jsonText));
	}

	/**
	 * Creates a new general feature.
	 * 
	 * @param general
	 *            A JSONObject that contains the attributes for this feature.
	 */
	public General(JSONObject general) {
		this();
		try {
			twitterID = general.getString("id");
			jsonFriends = general.getJSONArray("friends");
		} catch (Exception e) {
			System.err.println("Problems parsing JSONObject to General");
			System.err.println(general);
		}
	}

	/**
	 * Stores the General feature in the casebase. The name of the new instance
	 * is "general_####" where #### stands for the twitter user id.
	 */
	public void save() {
		try {
			Concept concept = CBREngine.get().getConcept(CBRConstants.Concepts.GENERAL);
			DefaultCaseBase cb = CBREngine.get().getCasebase(CBRConstants.CB.GENERAL);

			Instance newInstance = concept.addInstance(General.name() + "_" + getID());
			TwitDoc.setInstance(this, getID(), newInstance, concept);

			cb.addCase(newInstance);

		} catch (Exception e) {
			System.err.println(e);
		}
	}

	/**
	 * Getter.
	 * 
	 * @return "general"
	 */
	public static String name() {
		return "general";
	}

	/**
	 * Creates a JSON object that represents this instance of a General feature.
	 * 
	 * @return The JSON.
	 */
	@Override
	public JSONObject json() {

		if (json == null) {
			json = new JSONObject();
			json.setString("id", twitterID);
			JSONArray arr = new JSONArray();

			for (Map.Entry<String, String> entry : getFriends().entrySet()) {
				arr.append(entry.getValue());
			}

			json.setString("id", twitterID);
			json.setJSONArray("friends", arr);
		}
		return json;
	}

	/**
	 * Getter.
	 * 
	 * @return The twitter user id.
	 */
	public String getID() {
		return twitterID;
	}

	/**
	 * Checks if input id is included in the friends list.
	 * 
	 * @param twitID
	 *            The input twitter id.
	 * @return True, if the user with the input id is a friend of this user.
	 */
	public boolean hasFriend(String twitID) {
		return getFriends().containsKey(twitID);
	}

	/**
	 * Getter.
	 * 
	 * @return Returns all friends of the user in form of their ids (key, values
	 *         are both the id).
	 */
	public HashMap<String, String> getFriends() {
		if (friendsIDs == null) {
			friendsIDs = new HashMap<String, String>();

			if (jsonFriends != null) {
				for (int i = 0; i < jsonFriends.size(); i++) {
					String s = jsonFriends.getString(i);
					friendsIDs.put(s, s);
				}

				friendsSize = jsonFriends.size();
			}

		}

		return friendsIDs;
	}

	/**
	 * Overrides the friends with a new set of friends (e.g. for updating the
	 * casebase).
	 * 
	 * @param updated
	 *            A General feature which will override the old friends list of
	 *            this instance.
	 */
	public void setFriends(General updated) {
		this.friendsIDs = updated.getFriends();
		this.jsonFriends = updated.jsonFriends;
		this.friendsSize = updated.friendsSize;

	}

	/**
	 * Getter.
	 * 
	 * @return The number of twitter friends of this user.
	 */
	public int getFriendsSize() {

		if (friendsIDs == null) {
			getFriends();
		}

		return friendsSize;
	}

	/**
	 * Getters.
	 * 
	 * @return Returns all friends as an array if ids.
	 */
	public String[] getFriendsArray() {
		return getFriends().keySet().toArray(new String[friendsSize]);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		String txt = "general: { \n";
		txt += "\tid: " + getID() + "\n";
		txt += "\tfriends: [" + Tools.join(getFriends(), ", ") + "]\n";
		txt += "}";
		return txt;
	}
}
