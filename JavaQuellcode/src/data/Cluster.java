package data;

import java.util.ArrayList;

/**
 * A class representing a cluster of elements / a group of elements that are
 * similar to each other.
 * 
 * @author Diana Lange
 *
 * @param <T>
 *            The type of the elements of the cluster;
 */
public class Cluster<T> {

	/**
	 * Members of the cluster / group.
	 */
	private ArrayList<T> members;

	/**
	 * Mean element of the cluster or null if unknown.
	 */
	private T mean;

	/**
	 * Loss / error of the cluster (e.g. mean distance of the cluster members to
	 * mean element) or -1 if unknown.
	 */
	private float error;

	/**
	 * Creates a new cluster with one initial member.
	 * 
	 * @param initMember
	 *            The first member of the cluster.
	 */
	public Cluster(T initMember) {
		mean = null;
		members = new ArrayList<T>();
		members.add(initMember);
		error = -1;
	}

	/**
	 * Getter.
	 * 
	 * @return All members of the cluster (without mean value).
	 */
	public ArrayList<T> getMembers() {
		return members;
	}

	/**
	 * Getter.
	 * 
	 * @return Number of elements in the cluster.
	 */
	public Integer size() {
		return members.size();
	}

	/**
	 * Gets and returns the element of the cluster with the given index. May
	 * cause IndexOutOfBoundsException if the index is out of range of the
	 * cluster's size. Check size() method to avoid the exception.
	 * 
	 * @param index
	 *            Index of the cluster member.
	 * @return The element of the cluster with the given index.
	 */
	public T get(Integer index) {
		return members.get(index.intValue());
	}

	/**
	 * Getter.
	 * 
	 * @return Mean element of the cluster or null if unknown.
	 */
	public T getMean() {
		return mean;
	}

	/**
	 * Sets the error value of the cluster.
	 * 
	 * @param error
	 *            The new error value of the cluster.
	 */
	public void setError(float error) {
		this.error = error;
	}

	/**
	 * Getter.
	 * 
	 * @return Loss / error of the cluster (e.g. mean distance of the cluster
	 *         members to mean element) or -1 if unknown.
	 */
	public float getError() {
		return error;
	}

	/**
	 * Sets the mean value of the cluster.
	 * 
	 * @param mean
	 *            The new mean of the cluster.
	 */
	public void setMean(T mean) {
		this.mean = mean;
	}

	/**
	 * Prints all members of the cluster in console.
	 */
	public void printCluster() {
		for (T element : members) {
			System.out.println(element);
		}
	}

	/**
	 * Merges the other cluster with this cluster. The resulting cluster (this)
	 * will contain all members of the two clusters.
	 * 
	 * @param other
	 *            The cluster which should be merged with this cluster.
	 */
	public void merge(Cluster<T> other) {

		members.addAll(other.members);
	}
}
