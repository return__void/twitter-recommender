/**
 * A collection of classes for parsing and analyzing cases.
 * 
 * @author Diana Lange
 *
 */
package data;