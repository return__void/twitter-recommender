package mycbr;

/**
 * A collection of constants for identifying different parts of the retrieval
 * process.
 * 
 * @author Diana Lange
 *
 */
public class CBRConstants {

	/**
	 * No instances allowed
	 */
	private CBRConstants() {
	}

	/**
	 * A collection for identifying Category functions. Used in CategoryFactory.
	 * 
	 * @author Diana Lange
	 *
	 */
	public static class Categories {

		private Categories() {
		}

		public static class BehaviorCategories {
			public final static String GENERAL_TWEETFREQUENCY = "BEHAVIOR_GENERAL_TWEETFREQUENCY_CATEGORY";
			public final static String GENERAL_FIENDSPERFOLLOWER = "BEHAVIOR_GENERAL_FIENDSPERFOLLOWER_CATEGORY";
			public final static String GENERAL_LISTED = "BEHAVIOR_GENERAL_LISTED_CATEGORY";
			public final static String INTERACTION_FAVORITES = "BEHAVIOR_INTERACTION_FAVORITES_CATEGORY";
			public final static String INTERACTION_PEOPLE = "BEHAVIOR_INTERACTION_PEOPLE_CATEGORY";
			public final static String REACTION_RETWEETS = "BEHAVIOR_REACTION_RETWEETS_CATEGORY";
			public final static String REACTION_FAVORITES = "BEHAVIOR_REACTION_Favorites_CATEGORY";
		}

		public static class LanguageCategories {
			public final static String WORDS_AVGTWEETLENGTH = "LANGUAGE_WORDS_AVGTWEETLENGTH_CATEGORY";
			public final static String OUTLINE_AVGHASHTAGS = "LANGUAGE_OUTLINE_AVGHASHTAGS_CATEGORY";
		}
	}

	/**
	 * A collection for identifying Distance functions. Used in DistanceFactory.
	 * 
	 * @author Diana Lange
	 *
	 */
	public static class Distances {

		private Distances() {
		}

		public final static String BEHAVIOR_DISTANCE = "BEHAVIOR_DISTANCE";
		public final static String LANGUAGE_DISTANCE = "LANGUAGE_DISTANCE";
		public final static String CONTENT_DISTANCE = "CONTENT_DISTANCE";

		public static class BehaviorDistances {

			private BehaviorDistances() {
			};

			public final static String BEHAVIOR_GENERAL_DISTANCE = "BEHAVIOR_GENERAL_DISTANCE";
			public final static String BEHAVIOR_INTERACTION_DISTANCE = "BEHAVIOR_INTERACTION_DISTANCE";
			public final static String BEHAVIOR_REACTION_DISTANCE = "BEHAVIOR_REACTION_DISTANCE";

			public final static String GENERAL_TWEETFREQUENCY_DISTANCE = "BEHAVIOR_GENERAL_TWEETFREQUENCY_DISTANCE";
			public final static String GENERAL_LISTED_DISTANCE = "BEHAVIOR_GENERAL_LISTED_DISTANCE";
			public final static String GENERAL_FRIENDSPERFOLLOWER_DISTANCE = "BEHAVIOR_GENERAL_FRIENDSPERFOLLOWER_DISTANCE";

			public final static String INTERACTION_FAVORITES_DISTANCE = "BEHAVIOR_INTERACTION_FAVORITES_DISTANCE";
			public final static String INTERACTION_RETWEETS_DISTANCE = "BEHAVIOR_INTERACTION_RETWEETS_DISTANCE";
			public final static String INTERACTION_REPLIES_DISTANCE = "BEHAVIOR_INTERACTION_REPLIES_DISTANCE";
			public final static String INTERACTION_ATMENTIONS_DISTANCE = "BEHAVIOR_INTERACTION_ATMENTIONS_DISTANCE";
			public final static String INTERACTION_AVGPEOPLE_DISTANCE = "BEHAVIOR_INTERACTION_AVGPEOPLE_DISTANCE";
			public final static String INTERACTION_RECURRINGPEOPLE_DISTANCE = "BEHAVIOR_INTERACTION_RECURRINGPEOPLE_DISTANCE";

			public final static String REACTION_AVGRETWEETS_DISTANCE = "BEHAVIOR_REACTION_AVGRETWEETS_DISTANCE";
			public final static String REACTION_PRETWEETS_DISTANCE = "BEHAVIOR_REACTION_PRETWEETS_DISTANCE";
			public final static String REACTION_AVGFAVORITES_DISTANCE = "BEHAVIOR_REACTION_AVGFAVORITES_DISTANCE";
			public final static String REACTION_PFAVORITES_DISTANCE = "BEHAVIOR_REACTION_PFAVORITES_DISTANCE";
		}

		public static class LanguageDistances {

			private LanguageDistances() {
			}

			public final static String LANGUAGE_STYLE_DISTANCE = "LANGUAGE_STYLE_DISTANCE";
			public final static String LANGUAGE_WORDS_DISTANCE = "LANGUAGE_WORDS_DISTANCE";
			public final static String LANGUAGE_OUTLINE_DISTANCE = "LANGUAGE_OUTLINE_DISTANCE";

			public final static String STYLE_EMOTICON_DISTANCE = "LANGUAGE_STYLE_EMOTICON_DISTANCE";
			public final static String STYLE_QUOTES_DISTANCE = "LANGUAGE_STYLE_QUOTES_DISTANCE";
			public final static String STYLE_QUESTIONS_DISTANCE = "LANGUAGE_STYLE_QUESTIONS_DISTANCE";
			public final static String STYLE_EXCLAMATIONS_DISTANCE = "LANGUAGE_STYLE_EXCLAMATIONS_DISTANCE";

			public final static String WORDS_EMPTY_TWEETS_DISTANCE = "LANGUAGE_WORDS_EMPTY_TWEETS_DISTANCE";
			public final static String WORDS_SHORT_TWEETS_DISTANCE = "LANGUAGE_WORDS_SHORT_TWEETS_DISTANCE";
			public final static String WORDS_AVGWORDS_DISTANCE = "LANGUAGE_WORDS_AVGWORDS_DISTANCE";
			public final static String WORDS_SEMANTICS_NOUN_DISTANCE = "LANGUAGE_WORDS_SEMANTICS_NOUN_DISTANCE";
			public final static String WORDS_SEMANTICS_ADVERB_DISTANCE = "LANGUAGE_WORDS_SEMANTICS_ADVERB_DISTANCE";
			public final static String WORDS_SEMANTICS_VERB_DISTANCE = "LANGUAGE_WORDS_SEMANTICS_VERB_DISTANCE";
			public final static String WORDS_SEMANTICS_ADJECTIVE_DISTANCE = "LANGUAGE_WORDS_SEMANTICS_ADJECTIVE_DISTANCE";
			public final static String WORDS_SEMANTICS_NUMBER_DISTANCE = "LANGUAGE_WORDS_SEMANTICS_NUMBER_DISTANCE";
			public final static String WORDS_SEMANTICS_WH_DISTANCE = "LANGUAGE_WORDS_SEMANTICS_WH_DISTANCE";

			public final static String OUTLINE_PHASHTAGS_DISTANCE = "LANGUAGE_OUTLINE_PHASHTAGS_DISTANCE";
			public final static String OUTLINE_AVGHASHTAGS_DISTANCE = "LANGUAGE_OUTLINE_AVGHASHTAGS_DISTANCE";
			public final static String OUTLINE_LANGUAGES_COUNTRY_DISTANCE = "OUTLINE_LANGUAGES_COUNTRY_DISTANCE";
			public final static String OUTLINE_LANGUAGES_P_DISTANCE = "OUTLINE_LANGUAGES_P_DISTANCE";
		}

		public static class ContentDistances {

			private ContentDistances() {
			}

			public final static String CONTENT_ABOUT_DISTANCE = "CONTENT_ABOUT_DISTANCE";
			public final static String CONTENT_MEDIA_DISTANCE = "CONTENT_MEDIA_DISTANCE";

			public final static String ABOUT_HASHTAGS_DISTANCE = "CONTENT_ABOUT_HASHTAGS_DISTANCE";
			public final static String ABOUT_KEYWORDS_DISTANCE = "CONTENT_ABOUT_KEYWORDS_DISTANCE";
			public final static String ABOUT_HASHTAGS_WORD_DISTANCE = "CONTENT_ABOUT_HASHTAGS_WORD_DISTANCE";
			public final static String ABOUT_KEYWORDS_WORD_DISTANCE = "CONTENT_ABOUT_KEYWORDS_WORD_DISTANCE";

			public final static String MEDIA_URL_DISTANCE = "CONTENT_MEDIA_URL_DISTANCE";
			public final static String MEDIA_VIDEOS_DISTANCE = "CONTENT_MEDIA_VIDEOS_DISTANCE";
			public final static String MEDIA_PHOTOS_DISTANCE = "CONTENT_MEDIA_PHOTOS_DISTANCE";
			public final static String MEDIA_GIFS_DISTANCE = "CONTENT_MEDIA_GIFS_DISTANCE";
			public final static String MEDIA_DOMAIN_WORD_DISTANCE = "CONTENT_MEDIA_DOMAIN_WORD_DISTANCE";
		}

	}

	/**
	 * A collection of similarity function instances names which are used for
	 * the descriptors of the project. Used in constructor of the cbr engine as
	 * well as in JSONFct to identify the currently executed similarity function
	 * (parser and distance functions are retrieved by these constants).
	 * 
	 * @author Diana Lange
	 *
	 */
	public static class SimFcts {

		private SimFcts() {
		}

		public final static String BEHAVIOR = "behaviorJsonFct";
		public final static String CONTENT = "contentJsonFct";
		public final static String GENERAL = "generalJsonFct";
		public final static String LANGUAGE = "languageJsonFct";
		public final static String USER = "userJsonFct";
	}

	/**
	 * A collection of concept names which are used for the concepts of the
	 * project. Use these constants for CBREngine.get().getConcept(String);
	 * 
	 * @author Diana Lange
	 *
	 */
	public static class Concepts {

		private Concepts() {
		}

		public final static String BEHAVIOR = "behavior";
		public final static String CONTENT = "content";
		public final static String GENERAL = "general";
		public final static String LANGUAGE = "language";
		public final static String USER = "user";
	}

	/**
	 * A collection of case base names which are used for the case bases of the
	 * project. Use these constants for CBREngine.get().getCasebase(String);
	 * 
	 * @author Diana Lange
	 *
	 */
	public static class CB {

		private CB() {
		}

		public final static String USER = "users";
		public final static String BEHAVIOR = "behaviors";
		public final static String CONTENT = "contents";
		public final static String LANGUAGE = "languages";
		public final static String GENERAL = "generals";

	}
}
