package mycbr.similarity;

import java.util.ArrayList;
import java.util.HashMap;

import data.WordGroup;
import mycbr.CBRConstants.Distances;
import mycbr.CBRConstants.Categories;

/**
 * A factory that builds and stores the distance functions for TwitDocs and all
 * it's features and sub-attributes. All distances are in range of [0, 1] where
 * 0 means exact match.
 * 
 * @author Diana Lange
 *
 */
public class DistanceFactory {

	/**
	 * A map of a distance function name to a simple float distance function.
	 * Simple float distance functions take float values as input.
	 */
	private static HashMap<String, Distance<Float>> simpleFloatDistances = new HashMap<String, Distance<Float>>();

	/**
	 * A map of a distance function name to a simple String distance function.
	 * Simple String distance functions take String values as input.
	 */
	private static HashMap<String, Distance<String>> simpleStringDistances = new HashMap<String, Distance<String>>();

	/**
	 * A simple distance function that takes two float values and calculates a
	 * distance value.
	 */
	private static Distance<Float> defaultFloatDistance = createSimpleFloatDistance();

	/**
	 * A simple distance function that takes two String values. Produces only 0
	 * or 1 as result, since only exact matches are detected as similar.
	 */
	private static Distance<String> defaultStringDistance = createExactMatchDistance();

	/**
	 * A simple distance function that takes two String values and calculates
	 * the distance via Levenshtein algorithm.
	 */
	private static Distance<String> levenshteinDistance = levenshteinDistance();

	/**
	 * A map of a WordGroup list distance functions. The key is the distance
	 * function that is used to detect similarities between words within the
	 * WordGroups. The second HashMap is a map from maximal distance values to
	 * the WordGroup list distance function. The maximal distance value defines,
	 * how high the distance between words (String distance) can be so that the
	 * words are still detected as some kind of similar. All bigger distances
	 * than that value will be evaluated with 1.
	 */
	private static HashMap<Distance<String>, HashMap<Integer, Distance<ArrayList<WordGroup>>>> wordgroupDistances = new HashMap<Distance<String>, HashMap<Integer, Distance<ArrayList<WordGroup>>>>();

	/**
	 * Finds a simple String distance function that is mapped to the input name.
	 * 
	 * @param name
	 *            The name of the function (see CBRConstants.Distances).
	 * @return The String distance function.
	 */
	public static Distance<String> getSimpleStringDistance(String name) {
		if (!simpleFloatDistances.containsKey(name)) {
			setSimpleStringDistance(name);
		}
		return simpleStringDistances.get(name);
	}

	/**
	 * Finds a simple float distance function that is mapped to the input name.
	 * 
	 * @param name
	 *            The name of the function (see CBRConstants.Distances).
	 * @return The float distance function.
	 */
	public static Distance<Float> getSimpleFloatDistance(String name) {
		if (!simpleFloatDistances.containsKey(name)) {
			setSimpleFloatDistance(name);
		}
		return simpleFloatDistances.get(name);
	}

	/**
	 * Finds a WordGroup list distance with known String distance function.
	 * 
	 * @param wordDistance
	 *            The String distance function which is used to detect
	 *            similarities between the words of the WordGroups.
	 * @param maxDist
	 *            The maximal distance value defines, how high the distance
	 *            between words (String distance) can be so that the words are
	 *            still detected as some kind of similar. All bigger distances
	 *            than that value will be evaluated with 1.
	 * @return The WordGroup list distance.
	 */
	public static Distance<ArrayList<WordGroup>> getWordGroupDistance(final Distance<String> wordDistance,
			final float maxDist) {
		if (!DistanceFactory.wordgroupDistances.containsKey(wordDistance)) {
			DistanceFactory.wordgroupDistances.put(wordDistance,
					new HashMap<Integer, Distance<ArrayList<WordGroup>>>());
		}

		HashMap<Integer, Distance<ArrayList<WordGroup>>> container = DistanceFactory.wordgroupDistances
				.get(wordDistance);

		int key = (int) (maxDist * 100); // 0.35 >> 35
		if (!container.containsKey(key)) {

			Distance<ArrayList<WordGroup>> d = (o, v) -> {

				Distance<String> ld = wordDistance;
				float dist = 0;
				//boolean matchFound = false;
				float bestDist = 1;
				float distSum = 0;

				if (o.size() == 0 && v.size() == 0) {
					return 0f;
				} else if (o.size() == 0 || v.size() == 0) {
					return 1f;
				}

				for (int i = 0; i < o.size(); i++) {

					WordGroup owg = o.get(i);
					String os = owg.getRepresentant();
					//matchFound = false;
					bestDist = 1;

					for (int j = 0; j < v.size() /*&& !matchFound*/; j++) {

						WordGroup vwg = v.get(j);
						for (int k = 0; k < vwg.getAlternatives().length /*&& !matchFound*/; k++) {
							dist = ld.distance(os, vwg.getAlternatives()[k]);

							if (dist < maxDist && dist < bestDist) {
								//distSum += dist;
								bestDist = dist;
								//matchFound = true;
							} else if (dist > 0.7) {
								break;
							}
						}
					}
					
					distSum += bestDist;
				}

				return distSum / (float) o.size();
			};
			container.put(key, d);
		}
		return container.get(key);
	}

	/**
	 * Creates the most simple String distance function, which only checks if
	 * the Strings are exactly the same (case sensitive).
	 * 
	 * @return The String distance function.
	 */
	public static Distance<String> createExactMatchDistance() {
		Distance<String> d = (o, v) -> {
			return o.equals(v) ? 1 : 0;
		};

		return d;
	}

	/**
	 * Creates a String distance function based on Levenshtein algorithm.
	 * 
	 * @return The String distance function.
	 */
	public static Distance<String> levenshteinDistance() {
		Distance<String> d = (o, v) -> {

			if (o.equals(v)) {
				return 0f;
			}

			return (float) NormalizedLevenshtein.get().distance(o, v);
		};

		return d;
	}

	/**
	 * Creates a String distance function and adds it to the local storage
	 * HashMap DistanceFactory.simpleStringDistances.
	 * 
	 * @param name
	 *            The name of the distance function (see
	 *            CBRConstants.Distances).
	 */
	private static void setSimpleStringDistance(String name) {
		Distance<String> newDist = null;

		switch (name) {
		case Distances.LanguageDistances.OUTLINE_LANGUAGES_COUNTRY_DISTANCE:
		case Distances.ContentDistances.MEDIA_DOMAIN_WORD_DISTANCE:
			newDist = DistanceFactory.defaultStringDistance;
			break;
		case Distances.ContentDistances.ABOUT_HASHTAGS_WORD_DISTANCE:
		case Distances.ContentDistances.ABOUT_KEYWORDS_WORD_DISTANCE:
			newDist = DistanceFactory.levenshteinDistance;
		}

		if (newDist != null) {
			DistanceFactory.simpleStringDistances.put(name, newDist);
		}
	}

	/**
	 * Creates a float distance function and adds it to the local storage
	 * HashMap DistanceFactory.simpleFloatDistances.
	 * 
	 * @param name
	 *            The name of the distance function (see
	 *            CBRConstants.Distances).
	 */
	private static void setSimpleFloatDistance(String name) {

		Distance<Float> newDist = null;

		switch (name) {
		case Distances.BehaviorDistances.GENERAL_FRIENDSPERFOLLOWER_DISTANCE:
			newDist = DistanceFactory.createSimpleCatDistance(Categories.BehaviorCategories.GENERAL_FIENDSPERFOLLOWER);
		case Distances.BehaviorDistances.GENERAL_LISTED_DISTANCE:
			newDist = DistanceFactory.createSimpleCatDistance(Categories.BehaviorCategories.GENERAL_LISTED);
		case Distances.BehaviorDistances.GENERAL_TWEETFREQUENCY_DISTANCE:
			newDist = DistanceFactory.createSimpleCatDistance(Categories.BehaviorCategories.GENERAL_TWEETFREQUENCY);
			break;
		case Distances.BehaviorDistances.INTERACTION_FAVORITES_DISTANCE:
			newDist = DistanceFactory.createSimpleCatDistance(Categories.BehaviorCategories.INTERACTION_FAVORITES);
			break;
		case Distances.BehaviorDistances.INTERACTION_AVGPEOPLE_DISTANCE:
			newDist = DistanceFactory.createSimpleCatDistance(Categories.BehaviorCategories.INTERACTION_PEOPLE);
			break;
		case Distances.BehaviorDistances.REACTION_AVGFAVORITES_DISTANCE:
			newDist = DistanceFactory.createSimpleCatDistance(Categories.BehaviorCategories.REACTION_FAVORITES);
			break;
		case Distances.BehaviorDistances.REACTION_AVGRETWEETS_DISTANCE:
			newDist = DistanceFactory.createSimpleCatDistance(Categories.BehaviorCategories.REACTION_RETWEETS);
			break;
		case Distances.LanguageDistances.WORDS_AVGWORDS_DISTANCE:
			newDist = DistanceFactory.createSimpleCatDistance(Categories.LanguageCategories.WORDS_AVGTWEETLENGTH);
			break;
		case Distances.LanguageDistances.OUTLINE_AVGHASHTAGS_DISTANCE:
			newDist = DistanceFactory.createSimpleCatDistance(Categories.LanguageCategories.OUTLINE_AVGHASHTAGS);
			break;
		case Distances.BehaviorDistances.INTERACTION_RETWEETS_DISTANCE:
		case Distances.BehaviorDistances.INTERACTION_REPLIES_DISTANCE:
		case Distances.BehaviorDistances.INTERACTION_ATMENTIONS_DISTANCE:
		case Distances.BehaviorDistances.INTERACTION_RECURRINGPEOPLE_DISTANCE:
		case Distances.BehaviorDistances.REACTION_PFAVORITES_DISTANCE:
		case Distances.BehaviorDistances.REACTION_PRETWEETS_DISTANCE:
		case Distances.LanguageDistances.STYLE_EMOTICON_DISTANCE:
		case Distances.LanguageDistances.STYLE_QUOTES_DISTANCE:
		case Distances.LanguageDistances.STYLE_QUESTIONS_DISTANCE:
		case Distances.LanguageDistances.STYLE_EXCLAMATIONS_DISTANCE:
		case Distances.LanguageDistances.WORDS_EMPTY_TWEETS_DISTANCE:
		case Distances.LanguageDistances.WORDS_SHORT_TWEETS_DISTANCE:
		case Distances.LanguageDistances.WORDS_SEMANTICS_ADJECTIVE_DISTANCE:
		case Distances.LanguageDistances.WORDS_SEMANTICS_ADVERB_DISTANCE:
		case Distances.LanguageDistances.WORDS_SEMANTICS_NOUN_DISTANCE:
		case Distances.LanguageDistances.WORDS_SEMANTICS_NUMBER_DISTANCE:
		case Distances.LanguageDistances.WORDS_SEMANTICS_VERB_DISTANCE:
		case Distances.LanguageDistances.WORDS_SEMANTICS_WH_DISTANCE:
		case Distances.LanguageDistances.OUTLINE_PHASHTAGS_DISTANCE:
		case Distances.LanguageDistances.OUTLINE_LANGUAGES_P_DISTANCE:
		case Distances.ContentDistances.MEDIA_URL_DISTANCE:
		case Distances.ContentDistances.MEDIA_VIDEOS_DISTANCE:
		case Distances.ContentDistances.MEDIA_PHOTOS_DISTANCE:
		case Distances.ContentDistances.MEDIA_GIFS_DISTANCE:
			newDist = DistanceFactory.defaultFloatDistance;
			break;
		}

		if (newDist != null) {
			DistanceFactory.simpleFloatDistances.put(name, newDist);
		}
	}

	/**
	 * Creates a simple float distance function.
	 * 
	 * @return The new simple float function.
	 */
	private static Distance<Float> createSimpleFloatDistance() {
		Distance<Float> d = (o, v) -> {
			float dist = o - v;
			return dist < 0 ? dist * -1 : dist;
		};

		return d;
	}

	/**
	 * Creates a category based distance function. Category based functions take
	 * float values (without boundaries) and maps them to discrete int values.
	 * The int value defines the Category of the float. The distance is then
	 * calculated from the Categories of the inputs.
	 * 
	 * @param catName
	 *            The name of the Category (see CBRConstants.Categories).
	 * @return A float distance function.
	 */
	private static Distance<Float> createSimpleCatDistance(String catName) {
		final Category<Float> cat = CategoryFactory.get(catName);
		final int cases = CategoryFactory.getCases(catName);

		Distance<Float> d = (o, v) -> {

			int catO = cat.get(o);
			int catV = cat.get(v);

			float dist = (float) (catO - catV) / cases;

			return dist < 0 ? dist * -1 : dist;
		};

		return d;
	}
}
