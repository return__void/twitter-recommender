package mycbr.similarity;

/**
 * Class for calculating distances / similarities between objects. Distance and
 * similarity are both in range of [0, 1]. similarity = 1 - distance or distance
 * = 1 - similarity.
 * 
 * @author Diana Lange
 *
 * @param <T>
 *            The type of the objects of which the distance / similarity should
 *            be calculated.
 */
public abstract class AbstractDistance<T> implements Distance<T> {

	/**
	 * Getter.
	 * 
	 * @return Returns an instance of a comparable object.
	 */
	abstract public T get();

	/**
	 * Calculates the similarity between get() and another object.
	 * 
	 * @param v
	 *            The other object.
	 * @return The similarity value [0, 1] where 1 stands for identical objects.
	 */
	public float similarity(T v) {
		return 1 - distance(v);
	}

	/**
	 * Calculates the similarity between the two input objects.
	 * 
	 * @param o
	 *            The first object.
	 * @param v
	 *            The other object.
	 * @return The similarity value [0, 1] where 1 stands for identical objects.
	 */
	public float similarity(T o, T v) {
		return 1 - distance(o, v);
	}

	/**
	 * Calculates the distance between get() and another object.
	 * 
	 * @param v
	 *            The other object.
	 * @return The distance value [0, 1] where 0 stands for identical objects.
	 */
	public float distance(T v) {
		return distance(get(), v);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mycbr.similarity.Distance#distance(java.lang.Object,
	 * java.lang.Object)
	 */
	abstract public float distance(T o, T v);
}
