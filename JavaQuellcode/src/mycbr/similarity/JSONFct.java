package mycbr.similarity;

import processing.data.JSONObject;

import de.dfki.mycbr.core.Project;
import de.dfki.mycbr.core.casebase.Attribute;
import de.dfki.mycbr.core.casebase.StringAttribute;
import de.dfki.mycbr.core.model.StringDesc;
import de.dfki.mycbr.core.similarity.Similarity;
import de.dfki.mycbr.core.similarity.StringFct;
import de.dfki.mycbr.core.similarity.config.StringConfig;

import data.Behavior;
import data.Content;
import data.Language;
import data.TwitDoc;
import misc.JSONCreator;
import misc.Parser;
import mycbr.CBRConstants.SimFcts;

/**
 * A class for similarity functions. The similarity measure is defined by the
 * passed distance function. The class is based on StringFct. This has to be
 * done for myCBR project compatibility. Because when the myCBR projects are
 * loaded the loader tries to create all similarity functions, but the loader
 * doesn't know the similarity classes created for this TwitterRecommender
 * project. Therefore, when extending from a known similarity function, the
 * loader sets a known class like StringFct as a measure for a concept. But this
 * can be overwritten by adding an instance of this class with the same
 * similarity function name as the default one to the relevant descriptor.
 * 
 * @author Diana Lange
 *
 * @param <T>
 *            The type of which the similarities should be calculated (TwitDoc
 *            or Behavior or Language or Content).
 */
public class JSONFct<T extends JSONCreator> extends StringFct {

	/**
	 * A parse that parse String to the type of the instance. E.g. a String
	 * (with JSON data) to a TwitDoc.
	 */
	private Parser<String, T> parser;

	/**
	 * A distance function which defines the similarity measure.
	 */
	private Distance<T> distanceFct;

	/**
	 * Creates a new JSON text based similarity function class.
	 * 
	 * @param prj
	 *            The (myCBR) project.
	 * @param desc
	 *            The descriptor for the attributes of the similarity
	 *            calculation.
	 * @param name
	 *            The name of the similarity function.
	 */
	public JSONFct(Project prj, StringDesc desc, String name) {
		super(prj, StringConfig.EQUALITY, desc, name);

		parser = JSONFct.getParser(name);
		distanceFct = JSONFct.getDistance(name);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.dfki.mycbr.core.similarity.StringFct#calculateSimilarity(de.dfki.mycbr
	 * .core.casebase.Attribute, de.dfki.mycbr.core.casebase.Attribute)
	 */
	@Override
	public Similarity calculateSimilarity(Attribute queryAttr, Attribute caseAttr) throws Exception {

		Similarity res = Similarity.INVALID_SIM;

		if (queryAttr instanceof StringAttribute && caseAttr instanceof StringAttribute) {
			StringAttribute qStr = (StringAttribute) queryAttr;
			StringAttribute cStr = (StringAttribute) caseAttr;

			String q = qStr.getValue();
			String c = cStr.getValue();

			float d = distanceFct.distance(parser.parse(q), parser.parse(c));

			res = Similarity.get(1d - d);
		} else {
			return super.calculateSimilarity(queryAttr, caseAttr);
		}

		return res;
	}

	/**
	 * Gets the distance function for the given generic type of the instance.
	 * 
	 * @param functionName
	 *            The name of the distance function (see CBRConstants.SimFcts).
	 * @param <T>
	 *            A distance function that can calculate the distances between
	 *            Behaviors or Contents or Languages or TwitDocs depending on
	 *            the given function name.
	 * @return The distance function.
	 */
	@SuppressWarnings("unchecked")
	private static <T extends JSONCreator> Distance<T> getDistance(final String functionName) {
		Distance<T> newDistance = null;

		switch (functionName) {
		case SimFcts.BEHAVIOR:
			newDistance = (Distance<T>) Behavior.getDistanceFct();
			break;
		case SimFcts.CONTENT:
			newDistance = (Distance<T>) Content.getDistanceFct();
			break;
		case SimFcts.LANGUAGE:
			newDistance = (Distance<T>) Language.getDistanceFct();
			break;
		case SimFcts.USER:
			newDistance = (Distance<T>) TwitDoc.getWeightedDistanceFct(1, 2, 1);
			break;
		}

		return newDistance;
	}

	/**
	 * Gets the parser function for the given generic type of the instance.
	 * 
	 * @param functionName
	 *            The name of the parser function (see CBRConstants.SimFcts).
	 * @param <T>
	 *            A parser that parses from String (JSON-text) to Behavior or
	 *            Content or Language or TwitDoc depending on the given function
	 *            name.
	 * @return The distance function.
	 */
	@SuppressWarnings("unchecked")
	private static <T extends JSONCreator> Parser<String, T> getParser(final String functionName) {

		Parser<String, T> newParser = null;

		switch (functionName) {
		case SimFcts.BEHAVIOR:
			newParser = str -> (T) new Behavior(JSONObject.parse(str));
			break;
		case SimFcts.CONTENT:
			newParser = str -> (T) new Content(JSONObject.parse(str));
			break;
		case SimFcts.LANGUAGE:
			newParser = str -> (T) new Language(JSONObject.parse(str));
			break;
		case SimFcts.USER:
			newParser = str -> (T) new TwitDoc(JSONObject.parse(str));
			break;
		}

		return newParser;
	}

}
