package mycbr.similarity;

/**
 * Interface for calculating distances between objects.
 * 
 * @author Diana Lange
 *
 * @param <T>
 *            The type of the objects of which the distance should be
 *            calculated.
 */
public interface Distance<T> {

	/**
	 * Calculates the distance between the two inputs. The function usually
	 * should return values in range of [0, 1]. Where 0 means that the two
	 * inputs are identical and 1 that they are not similar at all.
	 * 
	 * @param o
	 *            First input object.
	 * @param v
	 *            Second input object.
	 * @return The distance between the two objects.
	 */
	float distance(T o, T v);
}
