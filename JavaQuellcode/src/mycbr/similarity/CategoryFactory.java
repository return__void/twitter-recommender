package mycbr.similarity;

import java.util.HashMap;

import mycbr.CBRConstants.Categories;

/**
 * A factory that generates category functions (for distance functions for
 * attributes of TwitDocs).
 * 
 * @author Diana Lange
 *
 */
public class CategoryFactory {

	/**
	 * A map of category names to a known Category functions.
	 */
	private static HashMap<String, Category<Float>> cats = new HashMap<String, Category<Float>>();

	/**
	 * A map of category names to the number of cases a Category function can
	 * produce.
	 */
	private static HashMap<String, Integer> catsCases = new HashMap<String, Integer>();

	/**
	 * Finds and returns a category function. The input name should be member of
	 * CBRConstants.Categories.
	 * 
	 * @param name
	 *            The name of the category (see CBRConstants.Categories).
	 * @return The category function.
	 */
	public static Category<Float> get(String name) {

		if (!cats.containsKey(name)) {
			cats.put(name, CategoryFactory.create(name));
		}

		return cats.get(name);
	};

	/**
	 * Finds and returns the number of cases a category function can produce.
	 * The input name should be member of CBRConstants.Categories.
	 * 
	 * @param name
	 *            The name of the category (see CBRConstants.Categories).
	 * @return The number of cases for the category (number greater / equal to
	 *         1),
	 */
	public static int getCases(String name) {
		if (!catsCases.containsKey(name)) {
			catsCases.put(name, CategoryFactory.createCase(name));
		}

		return catsCases.get(name);
	}

	/**
	 * Returns the number of cases for the given category name.
	 * 
	 * @param name
	 *            The name of the category (see CBRConstants.Categories).
	 * @return The number of cases for the category.
	 */
	private static int createCase(String name) {
		int cases = 1;

		switch (name) {
		case Categories.BehaviorCategories.GENERAL_FIENDSPERFOLLOWER:
		case Categories.BehaviorCategories.REACTION_FAVORITES:
		case Categories.BehaviorCategories.REACTION_RETWEETS:
			cases = 8;
			break;
		case Categories.LanguageCategories.OUTLINE_AVGHASHTAGS:
			cases = 7;
		case Categories.BehaviorCategories.INTERACTION_FAVORITES:
			cases = 6;
			break;
		case Categories.BehaviorCategories.INTERACTION_PEOPLE:
		case Categories.BehaviorCategories.GENERAL_TWEETFREQUENCY:
		case Categories.BehaviorCategories.GENERAL_LISTED:
		case Categories.LanguageCategories.WORDS_AVGTWEETLENGTH:
			cases = 5;
			break;
		default:
			System.out.println("case number not found");
			cases = 1;
			break;

		}

		return cases;
	}

	/**
	 * Returns a category function for the given category name.
	 * 
	 * @param name
	 *            The name of the category (see CBRConstants.Categories).
	 * @return The category function.
	 */
	private static Category<Float> create(String name) {

		Category<Float> cat = null;

		switch (name) {
		case Categories.BehaviorCategories.GENERAL_FIENDSPERFOLLOWER:
			cat = BehaviorFactory.generalFriendsPerFollower();
			break;
		case Categories.BehaviorCategories.GENERAL_LISTED:
			cat = BehaviorFactory.generalListed();
			break;
		case Categories.BehaviorCategories.GENERAL_TWEETFREQUENCY:
			cat = BehaviorFactory.generalTweetfrequency();
			break;
		case Categories.BehaviorCategories.INTERACTION_FAVORITES:
			cat = BehaviorFactory.interactionFavorites();
			break;
		case Categories.BehaviorCategories.INTERACTION_PEOPLE:
			cat = BehaviorFactory.interactionPeople();
			break;
		case Categories.BehaviorCategories.REACTION_FAVORITES:
			cat = BehaviorFactory.reactionFavorites();
			break;
		case Categories.BehaviorCategories.REACTION_RETWEETS:
			cat = BehaviorFactory.reactionRetweets();
			break;
		case Categories.LanguageCategories.WORDS_AVGTWEETLENGTH:
			cat = LanguageFactory.avgWords();
			break;
		case Categories.LanguageCategories.OUTLINE_AVGHASHTAGS:
			cat = LanguageFactory.avgHashtags();
		default:
			cat = obj -> 0;
			break;

		}

		return cat;
	}

	/**
	 * A factory that creates category function which belongs to the attributes
	 * of the Language feature of a TwitDoc.
	 * 
	 * @author Diana Lange
	 *
	 */
	public static class LanguageFactory {

		/**
		 * Category function for Language.Words.avgTweetLength
		 * 
		 * @return The category function.
		 */
		private static Category<Float> avgWords() {
			Category<Float> cat = value -> {
				if (value < 3) {
					return 0;
				} else if (value < 7) {
					return 1;
				} else if (value < 11) {
					return 2;
				} else if (value < 15) {
					return 3;
				} else {
					return 4;
				}
			};

			return cat;
		}

		/**
		 * Category function for Language.Outline.avgHashtags
		 * 
		 * @return The category function.
		 */
		private static Category<Float> avgHashtags() {
			Category<Float> cat = value -> {
				if (value < 1) {
					return 0;
				} else if (value < 1.1) {
					return 1;
				} else if (value < 1.3) {
					return 2;
				} else if (value < 1.5) {
					return 3;
				} else if (value < 2.1) {
					return 4;
				} else if (value < 3) {
					return 5;
				} else {
					return 6;
				}
			};

			return cat;
		}
	}

	/**
	 * A factory that creates category function which belongs to the attributes
	 * of the Behavior feature of a TwitDoc.
	 * 
	 * @author Diana Lange
	 *
	 */
	public static class BehaviorFactory {

		// general concept

		/**
		 * Category function for Behavior.General.tweetfrequency.
		 * 
		 * @return The category function.
		 */
		private static Category<Float> generalTweetfrequency() {
			Category<Float> cat = value -> {

				if (value < 0.5) {
					return 0;
				} else if (value < 3) {
					return 1;
				} else if (value < 10) {
					return 2;
				} else if (value < 20) {
					return 3;
				} else {
					return 4;
				}
			};

			return cat;
		}

		/**
		 * Category function for Behavior.General.friendsPerFollower.
		 * 
		 * @return The category function.
		 */
		private static Category<Float> generalFriendsPerFollower() {
			Category<Float> cat = value -> {

				if (value < 0.05) {
					return 0;
				} else if (value < 0.1) {
					return 1;
				} else if (value < 0.45) {
					return 2;
				} else if (value < 0.9) {
					return 3;
				} else if (value < 1.1) {
					return 4;
				} else if (value < 1.6) {
					return 5;
				} else if (value < 2.1) {
					return 6;
				} else {
					return 7;
				}
			};

			return cat;
		}

		/**
		 * Category function for Behavior.General.listed.
		 * 
		 * @return The category function.
		 */
		private static Category<Float> generalListed() {
			Category<Float> cat = value -> {
				if (value < 11) {
					return 0;
				} else if (value < 100) {
					return 1;
				} else if (value < 300) {
					return 2;
				} else if (value < 1000) {
					return 3;
				} else if (value < 10000) {
					return 4;
				} else if (value < 80000) {
					return 5;
				} else {
					return 6;
				}
			};

			return cat;
		}

		// interaction concept

		/**
		 * Category function for Behavior.Interaction.favorites.
		 * 
		 * @return The category function.
		 */
		private static Category<Float> interactionFavorites() {
			Category<Float> cat = value -> {
				if (value < 0.1) {
					return 0;
				} else if (value < 0.5) {
					return 1;
				} else if (value < 1) {
					return 2;
				} else if (value < 10) {
					return 3;
				} else if (value < 30) {
					return 4;
				} else {
					return 5;
				}
			};

			return cat;
		}

		/**
		 * Category function for Behavior.Interaction.avgPeople.
		 * 
		 * @return The category function.
		 */
		private static Category<Float> interactionPeople() {
			Category<Float> cat = value -> {

				if (value < 1.1) {
					return 0;
				} else if (value < 1.3) {
					return 1;
				} else if (value < 1.7) {
					return 2;
				} else if (value < 2.1) {
					return 3;
				} else {
					return 4;
				}
			};

			return cat;
		}

		// reaction

		/**
		 * Category function for Behavior.Reaction.avgRetweets.
		 * 
		 * @return The category function.
		 */
		private static Category<Float> reactionRetweets() {
			Category<Float> cat = value -> {
				if (value < 1.3) {
					return 0;
				} else if (value < 2) {
					return 1;
				} else if (value < 10) {
					return 2;
				} else if (value < 25) {
					return 3;
				} else if (value < 100) {
					return 4;
				} else if (value < 300) {
					return 5;
				} else if (value < 800) {
					return 6;
				} else {
					return 7;
				}
			};

			return cat;
		}

		/**
		 * Category function for Behavior.Reaction.avgFavorites.
		 * 
		 * @return The category function.
		 */
		private static Category<Float> reactionFavorites() {
			Category<Float> cat = value -> {

				if (value < 1.5) {
					return 0;
				} else if (value < 5) {
					return 1;
				} else if (value < 20) {
					return 2;
				} else if (value < 100) {
					return 3;
				} else if (value < 350) {
					return 4;
				} else if (value < 900) {
					return 5;
				} else if (value < 1500) {
					return 6;
				} else {
					return 7;
				}
			};

			return cat;

		}

	}

}
