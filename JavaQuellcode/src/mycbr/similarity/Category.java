package mycbr.similarity;

/**
 * Maps any input to an int.
 * 
 * @author Diana Lange
 *
 * @param <T>
 *            The type of the input.
 */
public interface Category<T> {

	/**
	 * Finds the mapping of the input to the int.
	 * 
	 * @param o
	 *            The input object.
	 * @return The category of the input which is represented by the int number.
	 */
	int get(T o);
}
