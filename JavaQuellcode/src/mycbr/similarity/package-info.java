/**
 * A collection of classes that are relevant for the calculation of similarities
 * between the cases of the Twitter Recommender.
 * 
 * @author Diana Lange
 *
 */
package mycbr.similarity;