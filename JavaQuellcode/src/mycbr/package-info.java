/**
 * A collection of classes for the case-based reasoning processes of the Twitter Recommender.
 * 
 * @author Diana Lange
 *
 */
package mycbr;