package mycbr;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import processing.data.JSONObject;

import de.dfki.mycbr.core.DefaultCaseBase;
import de.dfki.mycbr.core.Project;
import de.dfki.mycbr.core.casebase.Instance;
import de.dfki.mycbr.core.casebase.StringAttribute;
import de.dfki.mycbr.core.model.Concept;
import de.dfki.mycbr.core.model.StringDesc;
import de.dfki.mycbr.core.similarity.Similarity;
import de.dfki.mycbr.util.Pair;

import data.Behavior;
import data.Clustering;
import data.Content;
import data.General;
import data.Language;
import data.TwitDoc;
import misc.ConfigFile;
import misc.JSONCreator;
import misc.MeanCreator;
import misc.Tools;
import mycbr.CBRConstants.Concepts;
import mycbr.similarity.JSONFct;
import mycbr.CBRConstants.CB;
import mycbr.CBRConstants.SimFcts;

/**
 * The engine that delegates all myCBR relevant tasks like importing the case
 * base, updating the case base and retrieve cases. The engine will be started
 * the first time CBREngine.get() is called. Therefore the first retrieval may
 * take a while since all cases are loaded at that time.<br>
 * <br>
 * The whole project is build around a JSON data structure. New incoming cases
 * are formated as JSON since the data has been retrieved from the Twitter REST
 * API (which returns JSONs) and processed by a node server. The incoming JSONs
 * are parsed to the project intern structure named "TwitDoc". This structure
 * filters the data from the JSON (ignores some unimportant attributes). The
 * filtered data / TwitDoc then can be saved in the myCBR case base. All cases
 * are stored as Strings which represent a JSON-text version of the TwitDoc. For
 * retrieval the cases can be parsed back to TwitDocs, which makes the
 * similarity calculation easier. Each TwitDoc has four sub-features (Content,
 * Behavior, Language and General), and each of this feature can has more
 * sub-features. Each sub-feature or sub-sub-feature represents the idea of a
 * myCBR concept. But since the structure of myCBR doesn't seem to allow such
 * complex tree like structures (at least, at the moment), the work-around with
 * the TwitDocs and TwitDoc bases similarity calculations has been created.
 * 
 * @author Diana Lange
 *
 */
public class CBREngine {

	/**
	 * Storage for one instance of CBREngine. This is the engine for the
	 * TwitterRecommender, which handles all cbr requests.
	 */
	private static CBREngine engine = null;

	/**
	 * The cbr project.
	 */
	private Project project = null;

	/**
	 * A map of concept-names to concepts. All names are member of
	 * CBRConstants.Concepts.
	 */
	private HashMap<String, Concept> concepts;

	/**
	 * A map of casebase-names to casebases. All names are member of
	 * CBRConstants.CB. Cases are saved redundant. For each user a
	 * TwitDoc.json() is saved in the case base "users". But also, the four main
	 * features Behavior.json(), General.json(), Language.json() and
	 * Content.json() are stored in their own case bases. This makes the
	 * retrieval of similar cases which only concern one of the features more
	 * efficient since not the whole JSON needs to get parsed back to TwitDoc
	 * for each similarity calculation but only the feature that is needed.
	 */
	private HashMap<String, DefaultCaseBase> casebases;

	/**
	 * Number of user analysis stored in the case base when loaded the project.
	 */
	int initialUserSize = 0;

	/**
	 * Builds a new TwitterRecommender engine.
	 * 
	 * @param projectPath
	 *            The path to the project file. Make sure, this path exists!
	 */
	private CBREngine(File projectPath) {

		// load project and cases
		try {
			project = new Project(projectPath.getAbsolutePath()).getProject();
			while (project.isImporting()) {
				Thread.sleep(1000);
			}
		} catch (Exception e) {
		}

		// store all concepts in map
		Concept behaviorConcept = project.getConceptByID(Concepts.BEHAVIOR);
		Concept contentConcept = project.getConceptByID(Concepts.CONTENT);
		Concept generalConcept = project.getConceptByID(Concepts.GENERAL);
		Concept languageConcept = project.getConceptByID(Concepts.LANGUAGE);
		Concept userConcept = project.getConceptByID(Concepts.USER);

		concepts = new HashMap<String, Concept>();
		concepts.put(Concepts.BEHAVIOR, behaviorConcept);
		concepts.put(Concepts.CONTENT, contentConcept);
		concepts.put(Concepts.GENERAL, generalConcept);
		concepts.put(Concepts.LANGUAGE, languageConcept);
		concepts.put(Concepts.USER, userConcept);

		// store all case bases in map
		casebases = new HashMap<String, DefaultCaseBase>();
		casebases.put(CB.BEHAVIOR, (DefaultCaseBase) project.getCaseBases().get(CB.BEHAVIOR));
		casebases.put(CB.CONTENT, (DefaultCaseBase) project.getCaseBases().get(CB.CONTENT));
		casebases.put(CB.GENERAL, (DefaultCaseBase) project.getCaseBases().get(CB.GENERAL));
		casebases.put(CB.LANGUAGE, (DefaultCaseBase) project.getCaseBases().get(CB.LANGUAGE));
		casebases.put(CB.USER, (DefaultCaseBase) project.getCaseBases().get(CB.USER));

		// build similarity functions and connect them with their descriptors
		StringDesc behaviorJsonDesc = (StringDesc) behaviorConcept.getAttributeDesc("json");
		StringDesc contentJsonDesc = (StringDesc) contentConcept.getAttributeDesc("json");
		StringDesc languageJsonDesc = (StringDesc) languageConcept.getAttributeDesc("json");
		StringDesc userJsonDesc = (StringDesc) userConcept.getAttributeDesc("json");

		JSONFct<Behavior> behaviorFct = new JSONFct<Behavior>(project, behaviorJsonDesc, SimFcts.BEHAVIOR);
		JSONFct<Content> contentrFct = new JSONFct<Content>(project, contentJsonDesc, SimFcts.CONTENT);
		JSONFct<Language> languageFct = new JSONFct<Language>(project, languageJsonDesc, SimFcts.LANGUAGE);
		JSONFct<TwitDoc> userFct = new JSONFct<TwitDoc>(project, userJsonDesc, SimFcts.USER);

		behaviorJsonDesc.addFunction(behaviorFct, true);
		contentJsonDesc.addFunction(contentrFct, true);
		languageJsonDesc.addFunction(languageFct, true);
		userJsonDesc.addFunction(userFct, true);

		initialUserSize = userConcept.getAllInstances().size();
	}

	/**
	 * Getter.
	 * 
	 * @return Number of user cases in the case base (on project load).
	 */
	public int size() {
		return initialUserSize;
	}

	/**
	 * Getter.
	 * 
	 * @return The project of this engine.
	 */
	public Project getProject() {
		return project;
	}

	/**
	 * Getter.
	 * 
	 * @param name
	 *            The name of the lookup case base (see CBRConstants.CB).
	 * @return The case bases which is mapped to the input name.
	 */
	public DefaultCaseBase getCasebase(final String name) {
		return casebases.get(name);
	}

	/**
	 * Getter.
	 * 
	 * @param name
	 *            The name of the lookup concept (see CBRConstants.Concepts).
	 * @return The concept which is mapped to the input name.
	 */
	public Concept getConcept(final String name) {

		return concepts.get(name);
	}

	/**
	 * Updates the case base with the given TwitDocs.
	 * 
	 * @param docs
	 *            One or more TwitDocs.
	 */
	public void updateCB(TwitDoc... docs) {

		DefaultCaseBase generalCB = getCasebase(CBRConstants.CB.GENERAL);
		Concept generalConcept = getConcept(CBRConstants.Concepts.GENERAL);

		for (TwitDoc doc : docs) {

			/*
			 * The new case may miss the array of friends ids of a user (since
			 * the request for friends is very restricted by the Twitter API).
			 * Therefore this tries to retrieve a previous version of the case
			 * from the case base. This case is parsed to the General feature
			 * (which includes the friends ids). If the found case includes
			 * friends ids, this array will replace the original friends array
			 * from the new case.
			 */
			if (doc.getGeneral().getFriendsSize() == 0) {

				// look for previous case fot the same user
				Instance i = generalCB.containsCase(General.name() + "_" + doc.getGeneral().getID());

				// instance is found
				if (i != null) {
					// parse to General feature and override the friends ids in
					// new case with the ids from old case
					StringAttribute json = (StringAttribute) i.getAttForDesc(generalConcept.getAttributeDesc("json"));
					General gen = new General(json.getValue());

					// should always be greater than zero, but safety first!
					if (gen.getFriendsSize() > 0) {
						doc.getGeneral().setFriends(gen);
					}
				}
			}
			// create instances and store them
			doc.save();
			doc.getBehavior().save();
			doc.getLanguage().save();
			doc.getContent().save();

			// only save the General feature when it includes a friends ids list
			if (doc.getGeneral().getFriendsSize() > 0) {
				doc.getGeneral().save();
			}
		}

		// store instances to case base
		project.save();

	}

	/**
	 * Finds a user / TwitDoc of a user in case base.
	 * 
	 * @param userID
	 *            The twitter id of the user.
	 * @return The TwitDoc or null if the userID was not found in the case base.
	 */
	public TwitDoc getUser(String userID) {
		TwitDoc user = null;

		String caseName = TwitDoc.name() + "_" + userID;
		Concept concept = getConcept(Concepts.USER);
		StringDesc jsonDesc = (StringDesc) concept.getAttributeDesc("json");
		DefaultCaseBase cb = getCasebase(CB.USER);
		Instance userInstance = cb.containsCase(caseName);

		if (userInstance != null) {
			user = new TwitDoc(userInstance.getAttForDesc(jsonDesc).getValueAsString());
		}
		return user;
	}

	/**
	 * Finds the twitter ids of the friends of a user.
	 * 
	 * @param userID
	 *            The twitter id of the user.
	 * @return The ids of the friends of the user or null if users could not be
	 *         found. The ids array may be empty (user has no friends or friends
	 *         ids are not stored yet).
	 */
	public String[] getFriendsIDs(String userID) {
		String[] friendIDs = null;

		Concept concept = getConcept(Concepts.GENERAL);
		StringDesc jsonDesc = (StringDesc) concept.getAttributeDesc("json");
		DefaultCaseBase cb = getCasebase(CB.GENERAL);
		Instance userInstance = cb.containsCase(General.name() + "_" + userID);

		if (userInstance != null) {
			General gen = new General(userInstance.getAttForDesc(jsonDesc).getValueAsString());
			friendIDs = gen.getFriendsArray();
		}

		return friendIDs;
	}

	/**
	 * Finds the friends of friends of a user.
	 * 
	 * @param friendIDs
	 *            The twitter ids of the friends.
	 * @param friendsMap
	 *            Also the twitter ids of the friends, but as a map for faster
	 *            lookup.
	 * @return A map of twitter user id (of a friend of a friend) to the number
	 *         which represents how often the friend of the friend is friended
	 *         by any friend.
	 */
	public HashMap<String, Integer> getFriendsOfFriends(String[] friendIDs, HashMap<String, String> friendsMap) {
		HashMap<String, Integer> friendsOfFriendsIDs = new HashMap<String, Integer>();

		Concept generalConcept = getConcept(Concepts.GENERAL);
		StringDesc generalJsonDesc = (StringDesc) generalConcept.getAttributeDesc("json");
		DefaultCaseBase generalCb = getCasebase(CB.GENERAL);

		// iterate over all friend ids
		for (String id : friendIDs) {

			// check if friend can be found in case base
			Instance friendInstance = generalCb.containsCase(General.name() + "_" + id);

			if (friendInstance != null) {
				General general = new General(friendInstance.getAttForDesc(generalJsonDesc).getValueAsString());

				// get the friends of the friend
				HashMap<String, String> friendsOfFriends = general.getFriends();

				// iterate over all friends of the friend
				for (Map.Entry<String, String> entry : friendsOfFriends.entrySet()) {

					// check, that the friend of friend is not a friend
					if (!friendsMap.containsKey(entry.getKey())) {
						// set counter depending how often the user is a friend
						// of friend
						if (!friendsOfFriendsIDs.containsKey(entry.getKey())) {
							friendsOfFriendsIDs.put(entry.getKey(), 1);
						} else {
							int val = friendsOfFriendsIDs.get(entry.getKey()) + 1;
							friendsOfFriendsIDs.put(entry.getKey(), val);
						}
					}
				}
			}
		}

		return friendsOfFriendsIDs;
	}

	/**
	 * Finds the friends of friends, that the friends have mostly in common.
	 * 
	 * @param userID
	 *            The twitter user id of the input user.
	 * @param friendsOfFriends
	 *            The friends of friends of the input user.
	 * @return The most common friends of friends.
	 */
	private ArrayList<JSONObject> getMostSharedFriendsOfFriends(String userID,
			HashMap<String, Integer> friendsOfFriends) {
		ArrayList<JSONObject> common = new ArrayList<JSONObject>();

		// keep track of the top numbers of shared friends number
		ArrayList<Integer> counts = new ArrayList<Integer>();

		// map of count (number of shared friends) to an object that contains
		// the twitter id of the friend of friend
		HashMap<Integer, ArrayList<JSONObject>> map = new HashMap<Integer, ArrayList<JSONObject>>();

		for (Map.Entry<String, Integer> entry : friendsOfFriends.entrySet()) {

			if (!entry.getKey().equals(userID)) {

				if (!map.containsKey(entry.getValue())) {
					map.put(entry.getValue(), new ArrayList<JSONObject>());
				}

				if (counts.size() > 0 && entry.getValue() > counts.get(0)) {
					counts.add(entry.getValue());
					Collections.sort(counts);

					if (counts.size() > 8) {
						counts.remove(0);
					}
				} else if (counts.size() == 0) {
					counts.add(entry.getValue());
				}

				JSONObject obj = new JSONObject();
				obj.setString("id", entry.getKey());
				obj.setInt("count", entry.getValue());

				map.get(entry.getValue()).add(obj);

			}
		}

		if (counts.size() > 0) {
			// remove everything from map where the number of shared friends is
			// too little
			int min = counts.get(0) - 1;

			while (min > 0) {
				map.remove(min);
				min--;
			}

			// add all remaining users (with high number of shared friends) to
			// return list
			for (Map.Entry<Integer, ArrayList<JSONObject>> entry : map.entrySet()) {
				common.addAll(entry.getValue());
			}
		}

		if (common.size() > 0) {
			int count = common.get(0).getInt("count");
			if (count == 1) {
				common.clear();
			}
		}

		return common;
	};

	/**
	 * Evaluates found similar cases and returns the best match. Found cases
	 * will likely include the case which is represented by the input user and
	 * users that are already friends of the input user. These results must be
	 * filtered out to provide reasonable results.
	 * 
	 * @param results
	 *            A list of retrieval results.
	 * @param concept
	 *            The concept which was used for the retrieval.
	 * @param friendsOfFriends
	 *            (Inclusion condition) Null or a map of friends of friends. If
	 *            friendsOfFriends is not null, the returned case / user will be
	 *            member of that friendsOfFriends set.
	 * @param friendsMap
	 *            (Exclusion condition) A map of friend twitter ids. The result
	 *            will not be member of the friendsMap.
	 * @param exclusions
	 *            (Exclusion condition) A set of excluded results (a list of
	 *            twitter user ids). The result will not be member of the
	 *            exclusions.
	 * @return The best match (highest similarity) which matches the set
	 *         condition(s).
	 */
	private Recommendation evaluate(List<Pair<Instance, Similarity>> results, Concept concept,
			HashMap<String, Integer> friendsOfFriends, HashMap<String, String> friendsMap, String... exclusions) {
		Recommendation recommendation = Recommendation.def();

		StringDesc idDesc = (StringDesc) concept.getAttributeDesc("id");

		// iterate over all found cases
		for (Pair<Instance, Similarity> pair : results) {
			String newID = pair.getFirst().getAttForDesc(idDesc).getValueAsString();

			// check exclusions
			boolean a = !Tools.contains(newID, exclusions);
			boolean b = !friendsMap.containsKey(newID);

			// check inclusion
			boolean c = friendsOfFriends == null ? true : friendsOfFriends.containsKey(newID);

			if (a && b && c) {
				// relevant case found, stop lookup
				recommendation = Recommendation.get(newID, (float) pair.getSecond().getValue());
				break;
			}
		}

		return recommendation;
	}

	/**
	 * Finds for each input feature the most similar case in case base.
	 * 
	 * @param user
	 *            A user / TwitDoc feature.
	 * @param behavior
	 *            A behavior feature.
	 * @param content
	 *            A content feature.
	 * @param language
	 *            A language feature.
	 * @return An object with two attributes: "shared" and "clustering". The
	 *         clustering attributes contain recommendations that are found to
	 *         be the most similar to the input features. The "shared"
	 *         recommendations are based on the number of friends, that have
	 *         this user as a friend. Only friends of friends are taken in
	 *         consideration that share more than one common friend. Both
	 *         attributes contain the sub-attributes: "language", "content",
	 *         "behavior" and "user". Each of them has the attributes
	 *         "similarity" (found similarity for that case), "id" (twitter id
	 *         for the found case) and "friends" (number of friends, that have
	 *         this user as a friend). The id might have the value "-1" when for
	 *         that feature no case could be found.
	 */
	public JSONObject findRecommendations(TwitDoc user, Behavior behavior, Content content, Language language) {
		Recommendation[] recommendations = new Recommendation[8];

		// ids of all friends
		String[] friendIDs = getFriendsIDs(user.getGeneral().getID());
		HashMap<String, String> friendsMap = new HashMap<String, String>();

		for (String id : friendIDs) {
			friendsMap.put(id, id);
		}

		// friends of friends of user (but no friends of user)
		HashMap<String, Integer> friendsOfFriends = getFriendsOfFriends(friendIDs, friendsMap);

		// get results for each feature
		List<Pair<Instance, Similarity>> userResults = user.retrieve();
		List<Pair<Instance, Similarity>> behaviorResults = behavior.retrieve();
		List<Pair<Instance, Similarity>> contentResults = content.retrieve();
		List<Pair<Instance, Similarity>> languageResults = language.retrieve();

		// find best result for each feature, exclude previous found best
		// matches
		recommendations[0] = evaluate(userResults, getConcept(Concepts.USER), null, friendsMap,
				user.getGeneral().getID());
		recommendations[2] = evaluate(contentResults, getConcept(Concepts.CONTENT), null, friendsMap,
				user.getGeneral().getID(), recommendations[0].id);
		recommendations[1] = evaluate(behaviorResults, getConcept(Concepts.BEHAVIOR), friendsOfFriends, friendsMap,
				user.getGeneral().getID(), recommendations[0].id, recommendations[2].id);
		recommendations[3] = evaluate(languageResults, getConcept(Concepts.LANGUAGE), friendsOfFriends, friendsMap,
				user.getGeneral().getID(), recommendations[0].id, recommendations[1].id, recommendations[2].id);

		// get friends of friends, that are most common in friends
		ArrayList<JSONObject> commonFriendsOfFriends = getMostSharedFriendsOfFriends(user.getGeneral().getID(),
				friendsOfFriends);

		// find similarities to the inputs (behavior, ...) in received shared
		// friends of friends
		for (int i = 4, j = 0; j < 4; j++, i++) {

			// storage for finding match with highest similarity
			String maxSimID = "-1";
			float maxSim = 0;
			int count = 1;

			// iterate over all found common people
			for (JSONObject obj : commonFriendsOfFriends) {

				// check if user is in case base
				String id = obj.getString("id");
				TwitDoc commonDoc = getUser(id);

				if (commonDoc != null) {

					// calc similarities
					float sim = 0;
					if (j == 0) {
						sim = user.similarity(commonDoc);
					} else if (j == 1) {
						sim = behavior.similarity(commonDoc.getBehavior());
					} else if (j == 2) {
						sim = language.similarity(commonDoc.getLanguage());
					} else {
						sim = content.similarity(commonDoc.getContent());
					}
					if (sim > maxSim
							&& !Tools.contains(id, recommendations, rec -> rec == null ? "undefined" : rec.id)) {
						maxSim = sim;
						maxSimID = id;
						count = obj.getInt("count");
					}
				} /*
					 * else { System.out.println(id + ", " +
					 * obj.getInt("count")); }
					 */
			}

			// store found case in array
			recommendations[i] = new Recommendation(maxSimID, maxSim);
			recommendations[i].sharedFriends = count;
		}

		// parse results to JSON
		JSONObject obj = new JSONObject();
		JSONObject clustering = new JSONObject();
		JSONObject shared = new JSONObject();
		shared.setJSONObject("user", recommendations[4].json());
		shared.setJSONObject("behavior", recommendations[5].json());
		shared.setJSONObject("language", recommendations[6].json());
		shared.setJSONObject("content", recommendations[7].json());

		clustering.setJSONObject("user", recommendations[0].json());
		clustering.setJSONObject("behavior", recommendations[1].json());
		clustering.setJSONObject("language", recommendations[3].json());
		clustering.setJSONObject("content", recommendations[2].json());
		obj.setJSONObject("clustering", clustering);
		obj.setJSONObject("shared", shared);

		return obj;
	}

	/**
	 * Finds "clusteringSize" friends of the input user defined by "userID"
	 * (randomly sampled). For each feature - Behavior, Content, Language - a
	 * clustering process is initialized with the found friends as the elements
	 * of the clusterings. During the clustering process similar users are
	 * grouped into clusters. For each clustering one cluster with similar users
	 * is retrieved. These clustering results are returned as a String parsed
	 * JSONObject with the attributes "behavior", "content" and "language".
	 * 
	 * @param userID
	 *            The twitter id of the input user.
	 * @param clusteringSize
	 *            The number (of friends) which are retrieved for the clustering
	 *            processes.
	 * @return The found clusters as String parsed JSONObjects.
	 */
	public JSONObject getClusterings(String userID, int clusteringSize) {
		JSONObject clusteringResults = null;

		// get list with cases of friends as TwitDocs.
		ArrayList<TwitDoc> friendsList = getFriends(userID, 100);
		if (friendsList.size() >= 1) {
			TwitDoc[] friends = friendsList.toArray(new TwitDoc[friendsList.size()]);

			// get a function that can calculate the mean of TwitDocs
			MeanCreator<TwitDoc, TwitDoc> meaCreator = TwitDoc.getMeanFct();

			// perform clustering
			Clustering<TwitDoc> behaviorClustering = new Clustering<TwitDoc>(friends,
					TwitDoc.getWeightedDistanceFct(1, 0, 0), meaCreator, 2);
			Clustering<TwitDoc> contentClustering = new Clustering<TwitDoc>(friends,
					TwitDoc.getWeightedDistanceFct(0, 1, 0), meaCreator, 2);
			Clustering<TwitDoc> languageClustering = new Clustering<TwitDoc>(friends,
					TwitDoc.getWeightedDistanceFct(0, 0, 1), meaCreator, 2);

			// get one cluster for each clustering
			TwitDoc meanBehavior = behaviorClustering.getRandomCluster().getMean();
			TwitDoc meanContent = contentClustering.getRandomCluster().getMean();
			TwitDoc meanLanguage = languageClustering.getRandomCluster().getMean();

			// parse the things
			clusteringResults = new JSONObject();
			clusteringResults.setString("behavior", meanBehavior.text());
			clusteringResults.setString("content", meanContent.text());
			clusteringResults.setString("language", meanLanguage.text());
		}

		return clusteringResults;
	}

	/**
	 * Gets the TwitDocs / cases of a user's friends.
	 * 
	 * @param userID
	 *            The twitter id of the input user.
	 * @return TwitDocs generated from case base which contain information about
	 *         the friends of the input user.
	 */
	public ArrayList<TwitDoc> getFriends(String userID) {
		return getFriends(userID, -1, false);
	}

	/**
	 * Gets the TwitDocs / cases of a user's friends.
	 * 
	 * @param userID
	 *            The twitter id of the input user.
	 * @param limit
	 *            Max. number of retrieved friends.
	 * @return TwitDocs generated from case base which contain information about
	 *         the friends of the input user.
	 */
	public ArrayList<TwitDoc> getFriends(String userID, int limit) {
		return getFriends(userID, limit, false);
	}

	/**
	 * Gets the TwitDocs / cases of a user's friends.
	 * 
	 * @param userID
	 *            The twitter id of the input user.
	 * @param removeFriendsWithoutFriends
	 *            If true, the returned TwitDocs will only contain users that
	 *            have friends.
	 * @return TwitDocs generated from case base which contain information about
	 *         the friends of the input user.
	 */
	public ArrayList<TwitDoc> getFriends(String userID, boolean removeFriendsWithoutFriends) {
		return getFriends(userID, -1, removeFriendsWithoutFriends);
	}

	/**
	 * Gets the TwitDocs / cases of a user's friends.
	 * 
	 * @param userID
	 *            The twitter id of the input user.
	 * @param limit
	 *            Max. number of retrieved friends.
	 * @param removeFriendsWithoutFriends
	 *            If true, the returned TwitDocs will only contain users that
	 *            have friends.
	 * @return TwitDocs generated from case base which contain information about
	 *         the friends of the input user.
	 */
	public ArrayList<TwitDoc> getFriends(String userID, int limit, boolean removeFriendsWithoutFriends) {
		ArrayList<TwitDoc> docs = new ArrayList<TwitDoc>();
		Concept concept = getConcept(Concepts.USER);
		StringDesc jsonDesc = (StringDesc) concept.getAttributeDesc("json");
		DefaultCaseBase cb = getCasebase(CB.USER);

		// find input user in case base
		Instance userInstance = cb.containsCase(TwitDoc.name() + "_" + userID);

		if (userInstance != null) {
			TwitDoc user = new TwitDoc(userInstance.getAttForDesc(jsonDesc).getValueAsString());

			// get friends ids
			String[] friendIDs = user.getGeneral().getFriendsArray();

			// shuffle friendIDs list to get a random sample
			for (int i = 0; i < friendIDs.length; i++) {
				int otherID = (int) (Math.random() * friendIDs.length);
				String other = friendIDs[otherID];

				String tmp = friendIDs[i];
				friendIDs[i] = other;
				friendIDs[otherID] = tmp;
			}

			// iterate over ids of friends
			for (String friendID : friendIDs) {

				// find instance of friend in case base
				Instance friendInstance = cb.containsCase(TwitDoc.name() + "_" + friendID);

				if (friendInstance != null) {

					// parse instance to TwitDoc and add to resulting list
					TwitDoc friend = new TwitDoc(friendInstance.getAttForDesc(jsonDesc).getValueAsString());

					if (!removeFriendsWithoutFriends || friend.getGeneral().getFriendsSize() > 0) {
						docs.add(friend);
					}
				}

				if (limit > 0 && docs.size() == limit) {
					break;
				}
			}
		}

		return docs;
	}

	/**
	 * Gets the cbr engine for the TwitterRecommender. Inits the engine on first
	 * call of get().
	 * 
	 * @return The cbr engine.
	 */
	public static CBREngine get() {
		if (CBREngine.engine == null) {
			File staticLocation = new File(System.getProperty("user.home"), ".twitterRecommender");
			String projectFileName = (String) ConfigFile.getConfig(ConfigFile.PROJECT);
			engine = new CBREngine(new File(staticLocation, projectFileName + ".prj"));
			// engine.printInstances();
		}

		return CBREngine.engine;
	}

	/**
	 * A class to store recommendation results with the attributes: "id" (a
	 * twitter id which represents a user that is recommended) and "similarity"
	 * (a measure how good the recommendation is).
	 * 
	 * @author Diana Lange
	 *
	 */
	private static class Recommendation implements JSONCreator {

		/**
		 * The twitter user id or -1 if no true recommendation was found.
		 */
		private String id;

		/**
		 * The quality criterion for the recommendation.
		 */
		private float similarity;

		/**
		 * Number of friends, the user is friends with.
		 */
		private int sharedFriends = 1;

		/**
		 * Creates a new Recommendation instance.
		 * 
		 * @param id
		 *            The twitter user id or -1 if no true recommendation was
		 *            found.
		 * @param similarity
		 *            The quality criterion for the recommendation.
		 */
		public Recommendation(String id, float similarity) {
			this.id = id;
			this.similarity = similarity;
		}

		/**
		 * Creates a default Recommendation with id=-1 und similarity=0.
		 * 
		 * @return The default recommendation.
		 */
		public static Recommendation def() {
			return new Recommendation("-1", 0f);
		}

		/**
		 * Creates a new Recommendation instance.
		 * 
		 * @param id
		 *            The twitter user id or -1 if no true recommendation was
		 *            found.
		 * @param similarity
		 *            The quality criterion for the recommendation.
		 * @return The Recommendation instance.
		 */
		public static Recommendation get(String id, float similarity) {
			return new Recommendation(id, similarity);
		}

		/**
		 * Creates a JSON object of this instance.
		 * 
		 * @return The JSON object that represents this instance.
		 */
		@Override
		public JSONObject json() {
			JSONObject obj = new JSONObject();
			obj.setString("id", id);
			obj.setFloat("similarity", similarity);
			obj.setInt("friends", sharedFriends);

			return obj;
		}

	}

}
