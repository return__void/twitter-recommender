﻿TwitterRecommender
==================

Dieses Projekt stellt ein System dar, welches versucht Vorschläge zu generieren, welchen Nutzern man auf Twitter folgen sollte. Twitter selbst bietet nur einen rudimentären Recommender, welcher (scheinbar) nur auf gemeinsamen Freunden basiert. Dieses System hingegen findet Vorschläge basierend auf Ähnlichkeiten.
Der "TwitterRecommender" analysiert die Twitterseite eines Nutzers und extrahiert drei Features "language" (Sprache, z.B. die Verwendung von Emoticon), "content" (Inhalt, z.B. Verwendete Schlüsselwörter und Hashtags) und "behavior" (Verhalten, z.B. Anzahl der täglich veröffentlichten Tweets). Diese Features werden genutzt um Ähnlichkeiten zu anderen Nutzern festzustellen unter Verwendung des fallbasierten Schließens (myCBR). 

Dieses Projekt wurde von Diana Lange für das Modul "Neue Technologien und Semantic Web für Wissensmanagement" der Universität Hildesheim erstellt.

Das Projekt wurde mittels zwei Server realisert:  
(1) Node Server: Dieser Server bietet eine Reihe von Rest Endpoints und koordiniert die Anfragen an die Twitter API. Weiterhin wird hier die Auswertung der Twitter Daten vorgenommen. Dazu werden die Roh-Daten der Twitter API analysiert und mittels verschiedener Techniken, z. B. POS-Tagging der Tweets, ausgewertet und in eine strukturierte Form gebracht.  
(2) Tomcat Server: Dieser Server stellt das Interface bereit und nimmt die fertig ausgewerteten Daten des Node Servers entgegen.  Diese Daten werden hier in der Fallbasis gespeichert und die Fälle werden hier ebenfalls über selbst definierte Ähnlichkeitsmaße retrieved.

[1] Systemvoraussetzungen
---------------------

Node 6.3 oder neuer (Download unter https://nodejs.org/)
Java 1.8 oder neuer (Download unter http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
Tomcat 7
Moderner Browser

Getestet auf Windows 10 64 bit mit i5-4200U CPU, Java 1.8 32 bit, Tomcat 7, Node 6.3.1 mit Opera v49 und Firefox v47.

[2] Verwendete Bibliotheken
-----------------------

Front-End: **jQuery** (Anfragenkoordination zwischen den Servern und Darstellung der Server-Antworten im Interface, https://jquery.com)  
Node: **express** (Rest API zur Kommunikation mit der Twitter API, http://expressjs.com), **twit** (Authentifizierung und Anfragenkommunikation mit der Twitter API, https://github.com/ttezel/twit), **pos** (POS-Tagging, https://github.com/dariusk/pos-js), **emoji-regex** (Erkennung von Emoji, https://github.com/mathiasbynens/emoji-regex)  
Java: **myCBR** (Casebase und Projektverwaltung, http://www.mycbr-project.net), **Processing** (JSON Parser, https://processing.org), **Levenshtein Ähnlichkeit** (String Ähnlichkeit, https://github.com/tdebatty/java-string-similarity),

[3] Installation
------------

(1) Projekt Ordner entpacken. Darin befinden sich die Unterordner "NodeServer", "TomcatServer" und "Casebase" sowie weitere Ordner. Der Tomcat Ordner beinhaltet bereits Tomcat, so dass keine weitere Installation notwenig ist.  
(2) Node Dependencies Installation: Im Pfad "YourPathToTheProject/NodeServer" folgenden Befehl ausführen:
```
npm install
```
(3) Casebase: Im Ordner "Casebase" befindet sich der Unterordner ".twitterRecommender". Dieser Ordner beinhaltet die Fallbasis / das myCBR Projekt des Twitter Recommenders. Der Ordner ".twitterRecommender" ist in den Homeordner des aktuellen angemeldeten Nutzers zu kopieren. Beispielpfad:
```
C:\Users\Diana\.twitterRecommender\
```

[4] Inbetriebnahme des Recommenders
-------------------------------

(1) Node Server starten: Im Pfad "YourPathToTheProject/NodeServer" folgenden Befehl ausführen:

```
npm start
```

Der Server startet standardmäßig unter "http://localhost:8888".

(2) Tomcat Server starten: Im Pfad "YourPathToTheProject/TomcatServer/bin" die Anwendung "startup.bat" ausführen.

(3) Browser starten: Wenn alles erfolgreich ausgeführt werden konnte, ist der Recommender unter 
``` 
http://localhost:8080/twitterRecommender/
```
in einem beliebigen Browser erreichbar.

[5] Einstellungen
-------------

Unter Umständen ist der Port des Node Servers bereits in Verwendung. In diesem Fall kann statt dessen manuell ein Port für den Node Server festgelegt werden. Hierfür müssen die folgenden zwei Schritte ausgeführt werden:  

(1) Starten des Node Servers mit definiertem Port:
```
npm start -- PORT
```  
PORT ist eine beliebige Portnummer. Bitte auf die Leerzeichen vor und nach den Slashes achten.

(2) Setzen des Node Server Ports in Config Datei:  

Im Ordner der Casebase ".twitterRecommender" befindet sich die Datei "config.txt". In dieser Datei muss nun ebenfalls der Port gesetzt werden indem die entsprechende Zeile abgeändert wird:
```
NODE-PORT=PORT
```

Wobei PORT wieder der Port ist, der bereits beim Starten des Node Servers verwendet wurde. Ein zusammenhängendes Beispiel sieht demnach so aus:
```
npm start -- 8899 (Befehl)
NODE-PORT=8899 (in Config.txt)

```
Nach Änderung des Ports des Node Servers muss der Tomcat Server neu gestartet werden.

In der Config Datei kann ebenfalls die verwendete Casebase abgeändert werden. Per Default sieht die Einstellung so aus:
```
PROJECT=twitterRecommenderJSON
```

"twitterRecommenderJSON" ist der Name der Projektdatei  (ohne Dateiendung) welche im gleichen Ordner liegt wie die Config Datei. Durch Änderung der PROJECT Zuweisung kann eine andere Casebase / Projektdatei verwendet werden. Im mitgelieferten Ordner ".twitterRecommender" ist zum Beispiel ein weitere Datei namens "twitterRecommenderEmpty" beigefügt mit leerer Casebase.

[6] Definitionen
------------

"Twitter (englisch für Gezwitscher) ist ein Mikrobloggingdienst des Unternehmens Twitter Inc. Auf Twitter können angemeldete Nutzer telegrammartige Kurznachrichten verbreiten. Die Nachrichten werden „Tweets“ (von englisch tweet „zwitschern“) genannt." [Quelle: https://de.wikipedia.org/wiki/Twitter]  
Die Nutzer werden über einen beliebig wählbaren, aber innerhalb von Twitter einzigartigen Namen identifiziert. Innerhalb von Tweets können Nutzer über diesen Namen und einem At-Zeichen erwähnt und verlinkt werden (genannt at-Mention).  Die Tweets eines Nutzers können über dessen User Timeline von anderen eingesehen werden. Neben den Tweets des Nutzers sind ebenefalls Re-Tweets in dieser User Timeline enthalten. Ein Re-Tweet ist ein Tweet, der ursprünglich von einem andere Nutzern verfasst wurden, aber von dem dazugehörigen Nutzer der Timeline geteilt wurde.   
Tweets sind bis zu 280 Zeichen langen Text-Nachrichten. Zusätzlich können einem Tweet weitere Inhalte beigefügt werden wie Fotos, Videos und Gifs. Innerhalb des Textteils der Nachricht können neben den At-Mentions auch URLs enthalten sein sowie Hashtags. Hashtags werden mit einem '#' gekennzeichnet und dienen primär der Verschlagwortung von Tweets.   
Tweets können von Nutzern favorisiert werden. Die favorisierten Tweets erscheinen jedoch nicht in der User Timeline desjenigen, der den Tweet als Favorit markiert hat.   
Ein Twitter Nutzer kann anderen Nutzern folgen. Die Nutzer, die einem einem anderen Nutzer folgen, werden Follower genannt. Die Nutzer, die von einem Nutzer gefolgt werden, werden Friends / Freunde genannt. Die Tweets von allen Freunden werden in der Home Timeline des Nutzers zusammengestellt.


[7] Das Model
---------

Jeder Fall der Fallbasis repräsentiert einen Twitter Nutzer. In der Fallbasis wird das Analyseresultat gespeichert, welches von dem Node Server generiert wurde. Ein Analyseresultat wird innerhalb dieser Arbeit "TwitDoc" genannt. Jedes TwitDoc wurde auf der Basis dreier Informationen zusammengestellt; jede Information stellt gleichzeitig eine einzelne Anfrage an die Twitter API dar. Diese Informationen sind: 

(1) Das User Objekt eines Twitter Nutzers:  
(z. B. Twitter ID, Anzahl der Tweets, Anzahl der Freunde, Anmeldedatum, ...)
https://developer.twitter.com/en/docs/accounts-and-users/follow-search-get-users/api-reference/get-users-lookup  
(2) Die User Timeline eines Twitter Nutzers:  
(Tweetarray sortiert nach Erscheinungsdatum beginnend mit dem neuesten Tweet. Jede Tweetinformationen enthält neben den Tweettext Meta-Informationen wie die erkannte Sprache, die eingebunden At-Mentions usw.)
https://developer.twitter.com/en/docs/tweets/timelines/api-reference/get-statuses-user_timeline  
(3) Die Freunde eines Twitter Nutzers:  
(Ein Array von Tweeter IDs von Freunden des Nutzers)
https://developer.twitter.com/en/docs/accounts-and-users/follow-search-get-users/api-reference/get-friends-ids
  
Die Informationen der Twitter API wurden gefiltert und in eine strukturierte Form gebracht. Das resultierende TwitDoc unterteilt sich in vier Features: "General", "Language", "Content" und "Behavior".

#General

Das "General" Feature repräsentiert allgemeine Informationen über einen Twitter Nutzer.

```
id : String : Twitter user ID
friends : String array : Twitter user IDs der Freunde des users
```

#Behavior

Das "Behavior" Feature repräsentiert das Verhalten des Nutzers auf Twitter. Dazu zählt, wie präsent er / sie mit anderen in Interaktion tritt und wie die Tweets von anderen wahrgenommen werden. Das Feature ist in drei Sub-Features unterteilt: "General", "Interaction" (Interaktionen, die vom Twitter Nutzer getätigt werden) und "Reaction" (Reaktionen anderer Nutzer auf die Tweets des Nutzers).

*Behavior.General*

```
tweetfrequency : double : Durchschnittliche Anzahl der veröffentlichten Tweets pro Tag
listed : int : gibt an, wie oft der User in Listen auftaucht
friendsPerFollower : double : gibt das Verhältnis an zwischen Friends- und Followerzahlen (friendsPerFollower  = Anzahl der Friends / Anzahl der Follower)
```

*Behavior.Interaction*

```
favorites : double : durchschnittliche Anzahl der als Favorite vom User gekennzeichneten Tweets pro Tag
retweets : double : prozentualer Anteil der Tweets des Users, die Retweets sind
replies : double : prozentualer Anteil der Tweets des Users, die Antworten auf andere Tweets sind
atMentions : double : prozentualer Anteil der Tweets, die at-Mentions beinhalten
people.average : double : durchschnittliche Anzahl von at-Mentions pro Tweet
people.recurring : double : prozentualer Anteil an mit at-Mentions verlinkten Personen, die mehrmals in den Tweets des Users auftauchen
```

*Behavior.Reaction*

```
retweets.percentage: double : prozentualer Anteil der Tweets, die vom User verfasst wurden, die von anderen Usern retweeted wurden
retweets.average: double : durchschnittliche Anzahl von Retweets, die ein Tweet des Users erhält
favorites.percentage: double : prozentualer Anteil der Tweets, die vom User verfasst wurden, die von anderen Usern als Favorit gekennzeichnet wurden
favorites.average: double : durchschnittliche Anzahl von Favorisierungen, die ein Tweet des Users erhält
```

#Language

Das "Language" Feature repräsentiert die Spracheigenschaften der Tweets des Nutzers. Innerhalb dieses Features wird untersucht, ob die Sprache eher formal ist (Beispielkennzeichen: viele Substantive, Zahlen), eher populistische Inhalte hat (Beispielkennzeichen: viele Ausrufe) oder eher umgangssprachlich (Beispielkennzeichen: viele Emoticon, viele Adjektive). Das Sprachfeature ist demnach eine Meta-Information über den Inhalt der Tweets, da sich aus vielen sprachlichen Faktoren gewisse Schlussfolgerungen über die Inhalte der Tweets machen lassen. 

*Language.Style*

```
emoticon: double : prozentualer Anteil der Tweets, die Emoticon (Smiley oder Emoji) enthalten
quotes: double : prozentualer Anteil der Tweets, die ein Zitat beinhalten
questions: double : prozentualer Anteil der Tweets, die Fragen enthalten
exclamations: double : prozentualer Anteil der Tweets, die Ausrufe enthalten
```

*Language.Words*

```
tweetlength.empty: double : prozentualer Anteil der Tweets, die keine textlichen Inhalte haben außer u. U. URLS, at-Mentions und Hashtags
tweetlength.short: double : prozentualer Anteil der Tweets, die sehr kurz sind (aus 1-3 Wörtern bestehen)
tweetlength.average: double : durchschnittliche Anzahl der Wörter pro Tweets
semantics.noun : double : durchschnittlicher Anteil von Substantiven in dem textuellen Anteil der Tweets des Users
semantics.adjective: double : durchschnittlicher Anteil von Substantiven in dem textuellen Anteil der Tweets des Users
semantics.verb: double : durchschnittlicher Anteil von Verben in dem textuellen Anteil der Tweets des Users
semantics.adverb: double : durchschnittlicher Anteil von Adverbien in dem textuellen Anteil der Tweets des Users
semantics.number: double : durchschnittlicher Anteil von Zahlen in dem textuellen Anteil der des Users
semantics.wh: double : durchschnittlicher Anteil von Fragewörtern in dem textuellen Anteil der des Users
```

*Language.Outline*

```
hashtags.percentage: double : prozentualer Anteil der Tweets, die Hashtags beinhalten
hashtags.avergage: double : durchschnittliche Anzahl der Hashtags pro Tweet
languages: { String : double} : Auflistung der Sprachen, in denen der User getweetet hat der prozentualer Anteil der Sprache an allen Tweets
```

#Content

Das "Content" Feature versucht die wichtigsten Inhalte der User-Timeline zusammenzufassen. Dazu zählen die wichtigsten Schlüsselwörter (Hashtags und Nicht-Hashtags) sowie welche Medien eingesetzt werden.

*Content.About*

```
hashtags: {String array : int} : Auflistung der wichtigsten / häufigsten Hashtags, die der User in seinen Tweets benutzt hat und die Anzahl, die angibt, wie oft dieser Hashtag verwendet wurde in den Tweets. Ähnliche Hashtags wie #Inktober und #Inktober2017 sind in dem Array zusammengefasst 
keywords: {String array : int} : Auflistung der wichtigsten / häufigsten Schlüsselwörter, die der User in seinen Tweets benutzt hat und die Anzahl, die angibt, wie oft dieses Schlüsselwort verwendet wurde in den Tweets. Ähnliche Schlüsselwörter wie "Processing" und "OpenProcessing" sind in dem Array zusammengefasst. Schlüsselwörter sind ausschließlich Eigennamen und Substantive
```

*Content.Media*

```
urls: double : Procentualer Anteil der Tweets, die URLs beinhalten
videos: double : Procentualer Anteil der Tweets, die Videos beinhalten
photos: double : Procentualer Anteil der Tweets, die Fotos beinhalten
gifs: double : Procentualer Anteil der Tweets, die Gifs beinhalten
domains: String array: Auflistung an in Tweets verlinkten Domains 
```

[8] Nutzung
-------

Das Interface des Twitter Recommenders ist als One-Page-Webseite aufgebaut, welcher sich dynamisch aktualisiert mit den entsprechenden Nutzereingaben. Die Seite ist partiell responsive. Es wird zum Beispiel die Schriftgröße  anhand der Auflösung des Ausgabegeräts berechnet. Falls die berechnete Schriftgröße zu klein oder zu groß sein sollte, lässt sich dies durch die Buttons in der oberen rechten Ecke des Banners korrigieren.

![enter image description here](http://returnvoid.net/lib/bitbucket/images/fontsize.jpg)

(1) Starten der CBR Engine  

Die CBR Engine startet automatisch beim erstmaligen Laden der Seite. Das Retrieval von Recommendations sollte erst getätigt werden, wenn die Engine gestarted ist. Dies kann unter Umständen einige Zeit dauern wenn die Fallbasis viele Fälle beinhaltet. Der Status der Engine kann im Footer der Webseite überprüft werden:

![enter image description here](http://returnvoid.net/lib/bitbucket/images/enginestate.jpg)

Die Anleitung zur Nutzung ist ebenfalls auf der Twitter Recommender Seite unter "00. About" vermerkt:

![enter image description here](http://returnvoid.net/lib/bitbucket/images/start.jpg)

Das Interface ist so selbsterklärend wie möglich aufgebaut. Der/Die NutzerIn wird durch die einzelnen Menüpunkte geleitet und bekommt die Informationen schrittweise aufgedeckt.  

(2) Twitter Nutzer Eingabe

Nachdem die Engine gestartet ist, kann in dem Textfeld des Abschnitts "01. Who are you?" der Name eines  Twitter Users - zum Beispiel "@DianaOnTheRoad" or "DianaOnTheRoad" - eingetragen werden. Für diesen/diese NutzerIn wird dann die Recommendation durchgeführt. Nachdem der "Submit" Button betätigt wurde - und der User gefunden werden konnte - wird eine Auswertung des Users gezeigt und der nächste Abschnitt eingeblendet.

![enter image description here](http://returnvoid.net/lib/bitbucket/images/who.jpg)

(3) Featureerkennung

In Abschnitt "02. Who do you follow?" wird durch das Drücken des Buttons "Start Feature Extraction" der nächste Schritt eingeleitet. Nachdem der Fortschrittsbalken die 100 Prozent erreicht hat wird der nächste und letzte Abschnitt eingeblendet. Dieser Schritt aktualisiert und vervollständigt die Fallbasis mit den Twitter Freunden (aka Twitter NutzerInnen, die den Inputuser als Follower haben).

![enter image description here](http://returnvoid.net/lib/bitbucket/images/loading.jpg)

Zusätzlich erfolgt eine Analyse der Freunde des Inputusers: Für jedes der drei Hauptfeatures "language", "content" und "behavior" (Sprache, Inhalt und Verhalten), wird aus den Freunden des Eingabenutzers ein Cluster generiert, welches eine möglichst homogene Partition der Freunde hinsichtlich des entsprechendes Features darstellt. Das Clustering wird mit einem agglomerativen hierarischen Clusteranalyse-Algorithmus realisiert mit Complete Linkage zur Distanzbestimmung von Clustern und Lance-Williams Algorithmus zur Laufzeitoptimierung. Das Ergebnis des Clusteringprozesses wird ebenefalls in diesem Abschnitt gezeigt:

![enter image description here](http://returnvoid.net/lib/bitbucket/images/clusters.jpg)

Durch erneutes Drücken des "Start Feature Extraction" Buttons kann der Clusteringprozess neu gestartet werden. Die Clusteringqualität kann variieren, da a) bei jedem Clusteringprozess neue Fälle hinzugefügt werden, b) bei jedem Clusteringprozess ein zufälliges Sample der Freunde verwendet wird und c) von jedem Clustering ein zufälliges Cluster ausgewählt wird. Dies wird durchgeführt in der Annahme, dass der Eingabenutzer verschiedenen Interessen verfolgt - z.B. Leuten folgt, die / er sie persönlich kennt und anderen Leute folgt, weil die Thematik der Tweets interessant ist - und somit jede dieser Interessensgruppen Potenzial für Recommendations bietet.  
  
(4) Recommendations  

Durch drücken des "Find new friends" Buttons wird die Suche nach Recommendations gestartet. In diesem Schritt wird erneut die Fallsbasis mit neuen Twitternutzern aktualisiert (mit den Freunden von Freunden des Eingabenutzers).

![enter image description here](http://returnvoid.net/lib/bitbucket/images/newpeople.jpg)

Nach erfolgreicher Suche werden bis zu acht Vorschläge gemacht.  Die Vorschläge basieren auf festgestellten Ähnlichkeiten zu a) den Input Nutzer, zu b) einem der Cluster aus Abschnitt 2 sowie c) gemeinsamen Freunden der Freunde des Inputnutzers. Für jede Recommendation wird eine Zusammenfassung des vorgeschlagenen Nutzers gezeigt inklusive einer kurzen Erklärung, warum dieser Nutzer vorgeschlagen wurde und einem Link zu der Twitterseite des Nutzers.

![enter image description here](http://returnvoid.net/lib/bitbucket/images/recommendations.jpg)

[9] Einschränkungen
---------------

Die Recommendations basieren auf den Inhalten der öffentlichen Twitterseite - Profil und Timeline - des Eingabenutzers und dessen Twitter Freunde. Daher kann die Recommendation nur funktionieren, wenn der Eingabenutzer a) öffentlich ist, b) bereits eigene Tweets verfasst hat (Retweets reichen nicht) und c) mind. einen Twitter Freund hat, welcher ebenfalls die Bedingungen von a) und b) erfüllt. Je mehr Freunde der Inputuser hat und je mehr Tweets verfasst wurden, desto besser werden die Vorschläge sein.  
Die Vorschläge beschränken sich auf den bekannten Fällen der Fallbasis. In jedem Recommendationprozess werden der Fallbasis mehrere neue Fälle hinzugefügt und / oder existierende Fälle aktualisiert. Mit wachsender Fallbasis werden die Vorschläge ebenfalls an Genauigkeit gewinnen.

[10] Node Server
-----------

Die REST Endpoints des Node Servers können auch eigenständig genutzt werden. Hier die wichtigsten Endpoints:

Anzeigen des öffentlichen Profils eines Nutzers (hier: Beispielnutzer "DianaOnTheRoad"):
```
http://localhost:PORT/user/DianaOnTheRoad
```

Anzeigen der Analyseauswertung eines Nutzers:
```
http://localhost:PORT/twitdoc/DianaOnTheRoad
```
Optional kann angegeben werden, dass die Analyse ein Array mit den Freunden des Nutzers beinhalten soll:
```
http://localhost:PORT/twitdoc/DianaOnTheRoad/friends
```
Von einem Nutzer favorisierte Tweets finden:
```
http://localhost:PORT/favorites/DianaOnTheRoad/
```
Die Timeline (Sammlung von Tweets) eines Nutzers finden:
```
http://localhost:PORT/timeline/DianaOnTheRoad/
```
Die Freunde oder Follower eines Nutzers finden:
```
http://localhost:PORT/friends/DianaOnTheRoad/
http://localhost:PORT/follower/DianaOnTheRoad/
```

[11] Inhalt
------

Dieses Repository enthält folgende Ordner:
```
Bilder >> Screenshots des Twitter Recommenders und Graphen
Casebase >> das myCBR Projekt / die Fallbasis des Twitter Recommenders
JavaDoc >> Die Dokumentation des Java Servers
JavaQuellcode >> Der Quellcode des Java Servers inkl. exportierter .war Datei
NodeServer >> Der Quellcode des Node Servers
TomcatServer >> Tomcat inkl. bereitgestellter .war Datei des Twitter Recommender Projekts

```



