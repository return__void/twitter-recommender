const Server = require('./src/server.js').app;
const Config = require('./src/config');

// server init
Server.listen(Config.PORT, () => console.log("Server started at http://localhost:" + Config.PORT));