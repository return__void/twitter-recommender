/**
 * Created by Diana on 12.10.2017.
 */

let createPartitions = function(objArray, elementsPerPartition) {
    let partitions = [];
    let partition = [];
    let paritionIndex = 0;
    for (let i in objArray) {
        if (paritionIndex === elementsPerPartition) {
            partitions.push(partition.slice(0));
            partition = [];
            paritionIndex = 0;
        }
        partition.push(objArray[i]);
        paritionIndex++;

        if (i == objArray.length - 1) {
            partitions.push(partition.slice(0));
        }
    }

    return partitions;

};

/**
 * Creates an array with int numbers in range of [0, n) with random order.
 * @param n
 *      Upper range limit
 * @returns
 *      {Array} Array with numbers.
 */
let randomIndex = function (n) {
    let numbers = [];
    for (let i = 0; i < n; i++) {
        numbers.push(i);
    }

    for (let i = 0; i < n; i++) {
        let index = Math.round(Math.random() * n);
        let tmp = numbers[i];
        numbers[i] = numbers[index];
        numbers[index] = tmp;
    }

    return numbers;
};

/**
 * Creates a subset of the input array with random order.
 * @param objArray
 *      An array.
 * @param n
 *      Number of elements for the subsets (should be smaller than the size of the input array).
 * @returns
 *      {Array} The subset.
 */
let randomSample = function(objArray, n) {
    n = n > objArray.length ? objArray.length : n;
    let newArray = [];
    let indicies = randomIndex(objArray.length);

    for (let i = 0; i < n; i++) {
        newArray.push(objArray[indicies[i]]);
    }

    return newArray;
};

/**
 * Checks if the input element is a number.
 * @param element
 *      The element to check (e.g. 12, "12", "5.435")
 * @returns
 *      {boolean} True if input is a number.
 */
let isNumber = function (element) {
    if (Array.isArray(element)) {
        return false;
    } else {
        // source : https://stackoverflow.com/questions/1303646/check-whether-variable-is-number-or-string-in-javascript
        return /^-?[\d.]+(?:e-?\d+)?$/.test(element);
    }
};

/**
 * Filters out elements of an array that don't meet a condition. Returns a new array with only elements that meet the
 * condition.
 * @param input
 *      An array or an object.
 * @param condition
 *      A condition function that takes an element of the given array and evaluates it to true or false.
 * @returns
 *      {Array} The filtered new array.
 */
let filter = function (input, condition) {
    let newArray = undefined;
    let newObj = undefined;

    if (input.constructor && input.constructor === Array) {
        newArray = [];
    } else {
        newObj = {};
    }


    for (let i in input) {
        if (condition(input[i])) {
            if (newArray) {
                newArray.push(input[i]);
            } else {
                newObj[i] = input[i];
            }
        }
    }

    return newArray || newObj;
};

/**
 * Finds and returns a sub-object of an object.
 * @param obj
 *      An object.
 * @param keyStack
 *      An array containing the (reversed) path to the desired sub-object. E.g. keyStack = ["count", "req"] for an input
 *      object like { req : { count : {...}}
 * @returns
 *      The found sub-object or undefined if keys are not found within given object.
 */
let objSub = function (obj, keyStack) {
    let key = keyStack.pop();

    if (key in obj) {
        // keystack empty => sub-object found
        if (keyStack.length == 0) {
            return obj[key];
        } else {
            // not found jet, get sub-object and go one depth deeper
            return objSub(obj[key], keyStack);
        }
    } else {
        // key not found in given object
        return undefined;
    }
};

/**
 * Extracts certain values from objects that match the given key.
 * @param input
 *      An array or object containing objects.
 * @param key
 *      The key of an object from whom the value should be extracted. Not every object of the array have to has this key.
 *      The key can be a sub-key. Sub-Keys should be separated with points. E.g. for key="req.count" the values that match
 *      objArray[index].req.count will be extracted.
 * @param condition
 *      Optional. A function that takes an object of the array and evaluates to true or false. Can be used to extract
 *      only values from the array that match certain conditions.
 * @returns
 *      {Array} An array with the found values that match the given key.
 */
let extract = function (input, key, condition) {
    if (condition == undefined) {
        condition = user => true;
    }

    let keyStack = key.split(".").reverse();

    let values = [];
    for (let i in input) {
        if (condition(input[i])) {

            // get sub-object with given key
            let value = key == "" ? input[i] : objSub(input[i], keyStack.slice(0));

            if (value != undefined) {
                values.push(value);
            }
        }
    }

    return values;
};

/**
 * Counts the number of elements that match a certain condition.
 * @param input
 *      An array or object.
 * @param condition
 *      A condition with (elementOfInput) => true | false
 * @returns
 *      {number} Number of elements that fulfill the condition.
 */
let count = function(input, condition) {
    let count = 0;
    for (let i in input) {
        if (condition(input[i])) {
            count++;
        }
    }
    return count;
};


module.exports = {
    count,
    filter,
    extract,
    isNumber,
    randomSample,
    createPartitions
};