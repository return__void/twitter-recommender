/**
 * Created by Diana on 21.10.2017.
 */

const pos = require('pos');
const Tokenizer = require('./tokenizer');

/**
 * A list of stop words (non keywords).
 * @type {{de: string[]}}
 */
let stopWords = {
    "de": {
        "der": "der",
        "die": "die",
        "diese" : "diese",
        "dieser" : "dieser",
        "das": "das",
        "ein": "ein",
        "eine": "eine",
        "wir": "wir",
        "er": "er",
        "es" : "es",
        "sie": "sie",
        "ich": "ich",
        "ihr" : "ihr",
        "mit" : "mit",
        "in" : "in",
        "mein" : "mein",
        "meine" : "meine",
        "dein" : "dein",
        "deine" : "deine",
        "uns" : "uns",
        "unsere" : "unsere",
        "für" : "für"
    }
};

/**
 * Checks if a token is a known stop word / non keyword.
 * @return
 *      True if stop word was detected.
 */
let isStopWord = function (token) {
    for (let langKey in stopWords) {

        if (stopWords[langKey].hasOwnProperty(token.toLowerCase())) {
            return true;
        }
    }

    return false;
};

/*
 https://www.npmjs.com/package/pos:

 CC Coord Conjuncn           and,but,or
 CD Cardinal number          one,two
 DT Determiner               the,some
 EX Existential there        there
 FW Foreign Word             mon dieu
 IN Preposition              of,in,by
 JJ Adjective                big
 JJR Adj., comparative       bigger
 JJS Adj., superlative       biggest
 LS List item marker         1,One
 MD Modal                    can,should
 NN Noun, sing. or mass      dog
 NNP Proper noun, sing.      Edinburgh
 NNPS Proper noun, plural    Smiths
 NNS Noun, plural            dogs
 POS Possessive ending       �s
 PDT Predeterminer           all, both
 PP$ Possessive pronoun      my,one�s
 PRP Personal pronoun         I,you,she
 RB Adverb                   quickly
 RBR Adverb, comparative     faster
 RBS Adverb, superlative     fastest
 RP Particle                 up,off
 SYM Symbol                  +,%,&
 TO �to�                     to
 UH Interjection             oh, oops
 VB verb, base form          eat
 VBD verb, past tense        ate
 VBG verb, gerund            eating
 VBN verb, past part         eaten
 VBP Verb, present           eat
 VBZ Verb, present           eats
 WDT Wh-determiner           which,that
 WP Wh pronoun               who,what
 WP$ Possessive-Wh           whose
 WRB Wh-adverb               how,where
 , Comma                     ,
 . Sent-final punct          . ! ?
 : Mid-sent punct.           : ; �
 $ Dollar sign               $
 # Pound sign                #
 " quote                     "
 ( Left paren                (
 ) Right paren               )
 */

let posTagAndTokenize = function (tweet, condition) {

    if (tweet.language != "en") {
        let tokensArray = Tokenizer.getTokens(tweet.textCleaned);
        let tokens = [];
        for (let i in tokensArray) {
            if (condition('NN', tokensArray[i])) {
                tokens.push({word: tokensArray[i]});
            }
        }

        return tokens;
    }

    var words = new pos.Lexer().lex(tweet.textCleaned);
    var tagger = new pos.Tagger();
    var taggedWords = tagger.tag(words);
    let tokens = [];
    for (let i in taggedWords) {
        var taggedWord = taggedWords[i];
        var tag = taggedWord[1];
        var word = taggedWord[0];

        if (condition(tag, word)) {
            tokens.push({
                word: word,
                tag: tag
            });
        }
    }

    return tokens;
};

/**
 * Finds the pos category (e.g. noun, verb) to a given token
 * @param token
 *      a token with the "attribute" tag. The tag should be as https://www.npmjs.com/package/pos. If token has no tag attribute, undefined is returned.
 * @returns
 *      The pos category.
 */
let getPosCategory = function (token) {

    // noun (start with N)
    // adjective (start with J)
    // verb (start with V)
    // adverb (start with R)
    // wh (start with W) e.g who, whose, how, where, ...
    // number (start with CD)

    if (!token.tag) {
        return undefined;
    } else {
        if (token.tag[0] == "N") {
            return "noun";
        } else if (token.tag[0] == "J") {
            return "adjective";
        } else if (token.tag[0] == "V") {
            return "verb";
        } else if (token.tag[0] == "R") {
            return "adverb";
        } else if (token.tag[0] == "W") {
            return "wh";
        } else if (token.tag.indexOf("CD") == 0) {
            return "number";
        } else {
            return undefined;
        }

    }

};

/**
 * Analyses the tweets of a user and extracts language features.
 * @param tweets
 *      The tweets of a user.
 * @returns
 *      {
 *          style: {emoticon: number, quotes: number, questions: number, exclamations: number},
 *          words: {tweetlength: {empty: number, short: number, average: number, deviation: number},
 *          semantics: {count: number, noun: number, adjective: number, verb: number, adverb: number, number: number, wh: number}}, outline: {hashtags: {percentage: number, average: number, deviation: number}, languages: {}}
 *      }
 */
let language = function (tweets) {

    let emoticonPercentage = 0;
    let emptyTweetsPercentage = 0;
    let shortTweetsPercentage = 0;
    let exclamationsPercentage = 0;
    let questionsPercentage = 0;
    let quotesPercentage = 0;

    let avgWords = 0;
    let avgHashtags = 0;

    let hashtagTweetCount = 0;

    let hashtag_counts = [];
    let word_counts = [];

    let varianceHashtags = 0;
    let varianceWords = 0;

    let language = {};

    let pos = {
        count: 0,
        noun: 0,
        adjective: 0,
        verb: 0,
        adverb: 0,
        number: 0,
        wh: 0
    };

    tweets.forEach(tweet => {

        if (tweet.exclamation) {
            exclamationsPercentage++;
        }

        if (tweet.question) {
            questionsPercentage++;
        }

        if (tweet.emoticons.length > 0) {
            emoticonPercentage++;
        }

        if (tweet.quotes.length > 0) {
            quotesPercentage++;
        }

        if (tweet.hashtags.length > 0) {
            hashtagTweetCount++;
            avgHashtags += tweet.hashtags.length;
            hashtag_counts.push(tweet.hashtags.length);
        }

        if (tweet.tokens.length == 0) {
            emptyTweetsPercentage++;
        }
        else if (tweet.tokens.length <= 3) {
            shortTweetsPercentage++;
        }

        avgWords += tweet.tokens.length; //tweet.emoticons.length +
        word_counts.push(tweet.tokens.length); // tweet.emoticons.length +

        if (language[tweet.language]) {
            language[tweet.language]++;
        } else {
            language[tweet.language] = 1;
        }

        tweet.tokens.forEach(token => {

            let cat = getPosCategory(token);
            //console.log(cat);

            if (cat) {
                pos[cat]++;
                pos.count++;
            }
        });
    });

    if (pos.count > 0) {
        for (let key in pos) {
            if (key == 'count') {
                continue;
            }

            pos[key] /= pos.count;
        }
        delete pos.count;
    } else {
        pos = {};
    }


    for (let key in language) {
        language[key] /= tweets.length;
    }

    emoticonPercentage /= tweets.length;
    quotesPercentage /= tweets.length;
    emptyTweetsPercentage /= tweets.length;
    shortTweetsPercentage /= tweets.length;
    avgWords /= tweets.length;
    questionsPercentage /= tweets.length;
    exclamationsPercentage /= tweets.length;
    avgHashtags /= hashtagTweetCount;

    hashtag_counts.forEach(hashtagNum => {
        let dist = hashtagNum - avgHashtags;
        varianceHashtags += dist * dist;
    });

    word_counts.forEach(wordNum => {
        let dist = wordNum - avgWords;
        varianceWords += dist * dist;
    });

    varianceHashtags /= hashtagTweetCount;
    varianceWords /= tweets.length;

    // trying to compensate error in emoji detection (no all detected emoji are actually emoji)
    // number is calculated using test data
    emoticonPercentage *= 0.92;

    return {
        style: {
            emoticon: emoticonPercentage ? emoticonPercentage : 0,
            quotes: quotesPercentage ? quotesPercentage : 0,
            questions: questionsPercentage ? questionsPercentage : 0,
            exclamations: exclamationsPercentage ? exclamationsPercentage : 0
        },
        words: {
            tweetlength: {
                empty: emptyTweetsPercentage ? emptyTweetsPercentage : 0,
                short: shortTweetsPercentage ? shortTweetsPercentage : 0,
                average: avgWords ? avgWords : 0,
                deviation: varianceWords ? Math.sqrt(varianceWords) : 0
            },
            semantics: pos
        },
        outline: {
            hashtags: {
                percentage: hashtagTweetCount ? hashtagTweetCount / tweets.length : 0,
                average: avgHashtags ? avgHashtags : 0,
                deviation: varianceHashtags ? Math.sqrt(varianceHashtags) : 0
            },

            languages: language,
        }

    };
};

module.exports = {
    language,
    posTagAndTokenize,
    getPosCategory,
    isStopWord
};