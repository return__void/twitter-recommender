/**
 * Created by Diana on 16.10.2017.
 */

const TwitDoc = require("./TwitDoc").TwitDoc;
const TwitterHandler = require("../twitterhandler");

/**
 * Collects and analyses information of a user (tweet behavior, language and content).
 * @param input
 *      object with as follows: {
 *          key : either 'user_id' or 'screen_name',
 *          value : twitter user name or user id (as defined with 'key'),
 *          count : number of tweets of the user's timeline which influence the analysis,
 *          friends : true or false, if true, a list of ids of the user's friends will be included
 *      }
 * @param callback
 *      (err, data) Will be called when analysis is done. data looks like this: {
 *          user : twitter user object,
 *          doc : analysis of that user
 *      }
 */
let getUserAnalyis = function (input, callback) {

    // variable which will contain return information
    let doc = undefined;

    // variables to store request results
    let user = undefined;
    let timeline = undefined;
    let friends = undefined;
    let repliesCount = 0;
    let errors = [];

    /**
     * Checks if all request are replied. If so, data analysis is done and
     * result is passed to callback.
     * @param err
     *      Undefined or error object
     * @returns {*}
     */
    let finalize = function (err) {
        repliesCount++;

        // if any error happens (at one of the requests), reply with error message
        if (err) {
            errors.push(err);
            // console.log(errors);
            //return callback(err, undefined);
        }


        // check if everything is finished / replied
        if (repliesCount == 3) {

            if (errors.length > 0){
                callback(errors, undefined);
            }
            else if (timeline.length == 0) {
                return callback(undefined, {user: user});
            } else {
                // do analysis and reply
                doc = new TwitDoc(timeline, user, friends);
                return callback(undefined, {user: user, analysis: doc.analysis});
            }
        }
    };


    // request options

    let timelineOptions = {
        count: input.count,
        trim_user: true,
        tweet_mode: "extended"
    };

    let usereOptions = {};

    let friendsOptions = {
        count: 5000,
        format: 'ids',
    };

    timelineOptions[input.key] = input.value;
    usereOptions[input.key] = input.value;
    friendsOptions[input.key] = input.value;

    // callbacks for each request. Check if all requests
    // are replied after each callback call
    let friendsCallback = function (err, data) {

        if (!err) friends = data.ids;
        finalize(err);
    };

    let timelineCallback = function (err, data) {
        if (!err) timeline = data;
        finalize(err);
    };

    let userCallback = function (err, data) {
        //console.log("user callback");

        if (!err) {
            user = data[0];
            finalize(undefined);

            if (data && data[0].statuses_count > 0) {
                TwitterHandler.getTimeline(timelineOptions, timelineCallback);
                if (input.friends && data[0].friends_count > 0) {
                    TwitterHandler.getFriends(friendsOptions, friendsCallback);
                } else {
                    friends = [];
                    finalize(undefined);
                }
            } else {
                finalize(undefined); // friends finalisation
                finalize({message : "Can't analyse user. Timeline is empty."}); // timeline finalisation
            }
        } else {
            finalize(undefined);
            finalize(undefined);
            finalize(err);
        }

    };


    // do request
    // (friends and timeline only requested if user has friends / tweets)
    TwitterHandler.getUsers(usereOptions, userCallback);

};


module.exports = {
    getUserAnalyis
};