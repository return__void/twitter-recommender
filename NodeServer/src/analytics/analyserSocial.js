/**
 * Created by Diana on 19.10.2017.
 */

const Tools = require("../tools");

/**
 * Analyses the reactions of the tweets of a timeline. The reactions are favorites and retweets that the tweets of the user receive.
 * @param timeline
 *      The tweets of a user.
 * @param user
 *      The input user.
 * @returns
 *      {
 *          retweets: {percentage: number, average: number, deviation: number},
 *          favorites: {percentage: number, average: number, deviation: number}
 *      }
 */
let reaction = function (timeline, user) {

    let retweetedCount = 0;
    let favoritedCount = 0;

    let avgRetweets = 0;
    let avgFavorites = 0;

    let varianceRetweets = 0;
    let varianceFavorites = 0;

    let originalCount = 0;

    let retweet_counts = [];
    let favorite_counts = [];

    timeline.forEach(tweet => {
        if (!tweet.retweeted_status) {

            originalCount++;

            if (tweet.retweet_count > 0) {
                retweetedCount++;
                avgRetweets += tweet.retweet_count;
                retweet_counts.push(tweet.retweet_count);
            }

            if (tweet.favorite_count > 0) {
                favoritedCount++;
                avgFavorites += tweet.favorite_count;
                favorite_counts.push(tweet.favorite_count);
            }
        }
    });

    avgRetweets /= retweetedCount;
    avgFavorites /= favoritedCount;

    retweet_counts.forEach(retweetNum => {
        let dist =  retweetNum - avgRetweets;
        varianceRetweets += dist * dist;
    });

    favorite_counts.forEach(favoriteNum => {
        let dist =  favoriteNum - avgFavorites;
        varianceFavorites += dist * dist;
    });

    varianceRetweets /= retweetedCount;
    varianceFavorites /= favoritedCount;

    return {
        retweets: {
            percentage: originalCount == 0 || !retweetedCount ? 0 : parseFloat(retweetedCount) / originalCount,
            average: avgRetweets ? avgRetweets : 0,
            deviation: varianceRetweets ? Math.sqrt(varianceRetweets) : 0
        },
        favorites: {
            percentage: originalCount == 0 || !favoritedCount ? 0 : parseFloat(favoritedCount) / originalCount,
            average: avgFavorites ? avgFavorites : 0,
            deviation: varianceFavorites  ? Math.sqrt(varianceFavorites) : 0
        }

    };
};

/**
 * Analyses the interactions of a user. The interaction is how often the user retweets, how often the user favorite something, ...
 * @param timeline
 *      The tweets of a user.
 * @param user
 *      The input user.
 * @returns
 *      {
 *          favorites: number,
 *          retweets: {total: number, percentage: number},
 *          replies: {total: number, percentage: number},
 *          atMentions: {total: number, percentage: number},
 *          people: {average: number, recurring: number}
 *      }
 */
let interaction = function (timeline, user) {

    let now = Date.now();
    let creation = new Date(user.created_at);
    let oldest = new Date(timeline[timeline.length - 1].created_at);
    let retweetCount = 0;//timeline.length == 0 ? 0 : Tools.count(timeline, tweet => 'retweeted_status' in tweet);
    let replyCount = 0;
    let atMentionTweetCount = 0;
    let atMentionCount = 0;
    let atMentions = {};
    let originalCount = 0;
    let numberOfPeopleMentioned = 0;

    timeline.forEach(tweet => {
        if (tweet.retweeted_status) {
            retweetCount++;
        } else {
            originalCount++;
        }

        if (tweet.in_reply_to_status_id) {
            replyCount++;
        }

        if (!tweet.retweeted_status && tweet.entities.user_mentions.length > 0) {
            atMentionTweetCount++;
            atMentionCount += tweet.entities.user_mentions.length;
            tweet.entities.user_mentions.forEach(mention => {
                numberOfPeopleMentioned++;
                let userName = mention.screen_name;

                if (!atMentions[userName]) {
                    atMentions[userName] = 1
                } else {
                    atMentions[userName]++;
                }
            });

        }

    });

    let timediffOverall = (now - creation) / (1000 * 60 * 60 * 24);
    let timmediffLatest = (now - oldest) / (1000 * 60 * 60 * 24);

    return {
        favorites: user.favourites_count / timediffOverall,
        retweets: {
            total: retweetCount / timmediffLatest,
            percentage : timeline.length == 0 ? 0 : parseFloat(retweetCount) / timeline.length
        },
        replies: {
            total : replyCount / timmediffLatest,
            percentage : timeline.length == 0 ? 0 : parseFloat(replyCount) / timeline.length
        },
        atMentions: {
            total : atMentionCount / timmediffLatest,
            percentage : originalCount == 0 ? 0 : parseFloat(atMentionTweetCount) / originalCount,
        },
        people : {
            average : atMentionTweetCount == 0 ? 0 : numberOfPeopleMentioned / parseFloat(atMentionTweetCount),
            recurring : numberOfPeopleMentioned == 0 ? 0 : Tools.count(atMentions, mention => mention > 1) / numberOfPeopleMentioned
        }
    };
};

module.exports = {
    interaction,
    reaction
};