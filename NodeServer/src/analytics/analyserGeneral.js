/**
 * Created by Diana on 20.10.2017.
 */

const Regex = require('./tokenizerPatterns');

/**
 * Calculates tweetfrequency (number of tweets per day).
 * @param timeline
 *      A timeline (an array of tweets, sorted by time, beginning with newest)
 * @param user
 *      A user object
 * @returns
 *      {object} The tweetfrequency in tweets / day (latest and overall).
 */
let tweetfrequency = function (timeline, user) {

    // calculate the time that has passed
    let creation = new Date(user.created_at);
    let now = Date.now();
    let oldest = new Date(timeline[timeline.length - 1].created_at);

    // time difference between oldest tweet in timeline an now
    let timediffLatest = (now - oldest) / (1000 * 60 * 60 * 24);

    // time difference between the creation of the twitter account and now
    let timediffOverall = (now - creation) / (1000 * 60 * 60 * 24);

    return {
        latest: timeline.length / timediffLatest,
        overall: user.statuses_count / timediffOverall
    };
};

/**
 * Extracts information from the tweets of the given timeline and arranges them in a suitable way.
 * @param timeline
 * @returns {Array}
 */
let timelineContent = function (timeline) {

    // element that will be returned
    let tweets = [];

    // factory to create default tweet object
    let tweetmock = function () {
        return {
            // text with extended urls (not shortened)
            textExtended: "",

            // text without media / url / atMention & hashtags
            textCleaned: "",
            urls: [],
            photos: [],

            // videos als Link (bekannte Webseiten wie youtube) oder eingebunden
            // z.B. https://twitter.com/creativeapps/status/920638277169934336/video/1" (achtung: entities.media.type=photo! aber extended_entities.media.type=video)
            videos: [],
            gifs: [],
            atMentions: [],
            quotes: [],
            hashtags: [],
            question: false,
            exclamation: false,
            language: "en"
        };
    };

    // save urls which refer to a video provider as video not as url
    let videoProviders = "youtube\\.|youtu\\.be|dailymotion\\.|vimeo\\.|twitch\\.";
    let videoRegex = new RegExp("(?:" + videoProviders + ")", "gmi");

    // regex which records all elements that have to be replaced in
    // original tweet text (e.g. hashtags, at-mentions) to get
    // a cleaned version (for tokenization)
    let tweetCleaners = "";

    timeline.forEach(tweet => {

        // ignore retweets
        if (!tweet.retweeted_status) {

            tweetCleaners = "";
            let t = tweetmock();
            let textExtended = tweet.text ? tweet.text : tweet.full_text;

            // media
            if (tweet.extended_entities && tweet.extended_entities.media) {

                tweet.extended_entities.media.forEach(entry => {

                    // video
                    if (entry.type == 'video') {
                        t.videos.push(entry.expanded_url);
                    }
                    // photo
                    if (entry.type == 'photo') {
                        t.photos.push(entry.expanded_url);
                    }

                    // gif
                    if (entry.type == 'animated_gif') {
                        t.gifs.push(entry.expanded_url);
                    }

                });
            }

            // hastags
            tweet.entities.hashtags.forEach(hashtag => {
                tweetCleaners += "#" + hashtag.text + "|";
                t.hashtags.push(hashtag.text);
            });

            // at-mentions
            tweet.entities.user_mentions.forEach(mention => {
                tweetCleaners += "@" + mention.screen_name + "|";
                t.atMentions.push(mention.screen_name);
            });

            if (tweet.entities.media) {
                tweet.entities.media.forEach(medium => {
                    tweetCleaners += medium.url + "|";
                    textExtended = textExtended.replace(medium.url, medium.expanded_url);
                });
            }

            // link or link to video
            tweet.entities.urls.forEach(url => {

                tweetCleaners += url.url + "|";
                textExtended = textExtended.replace(url.url, url.expanded_url);

                let full = url.expanded_url;
                if (full.match(videoRegex)) {
                    t.videos.push(full);
                } else {
                    t.urls.push(full);
                }
            });


            // text
            let tweetText = tweet.text ? tweet.text : tweet.full_text;

            // clean text
            if (tweetCleaners.length > 0) {
                let replaceRegex = new RegExp("(?:" + tweetCleaners.substr(0, tweetCleaners.length - 1) + ")", "gmi");
                tweetText = tweetText.replace(replaceRegex, "").trim();
            }

            t.exclamation = tweetText.indexOf("!") > -1;
            t.question = tweetText.indexOf("?") > -1;
            t.textCleaned = tweetText;
            t.textExtended = textExtended;
            t.language = tweet.lang;

            // quotes
            let quotes = [];
            let quote;
            while (quote = Regex.Patterns.quotes.exec(tweetText)) {
                quotes.push(quote[0]);
            }
            t.quotes = quotes;

            // add tweet to array
            tweets.push(t);

        }
    });

    return tweets;
};

module.exports = {
    tweetfrequency,
    timelineContent
};