/**
 * Created by Diana on 14.10.2017.
 */

const Regex = require('./tokenizerPatterns');
const Tools = require('../tools');


/**
 * Extracts emoticon from an input text.
 * @param input
 *      A String.
 * @returns
 *      {Array} An array with detected emoticon.
 */
let getEmoticons = function (input) {

    let smiley = Regex.Patterns.smiley;
    let whitespace = "\\s+";
    let leftCheck = new RegExp("(" + whitespace + "|" + Regex.Definitions.brackets + ")", "g");
    let rightCheck = new RegExp("(" + whitespace + "|" + Regex.Definitions.brackets + "|" + Regex.Definitions.punctuation + ")", "g");
    let match;
    let matches = [];
    let emojis = input.match(Regex.Patterns.emoji);
    if (emojis) {
        emojis.forEach(emoji => matches.push(emoji));
    }

    while (match = smiley.exec(input)) {
        let whitespaceCount = 0;

        whitespaceCount += match.index == 0 ? 1 : input[match.index - 1].match(leftCheck) != undefined ? 1 : 0;
        whitespaceCount += match.index + match[0].length == input.length ? 1 : input[match.index + match[0].length].match(rightCheck) != undefined ? 1 : 0;

        if (match[0] == "...") {
            whitespaceCount = 0;
        }

        if (whitespaceCount > 0) {
            matches.push(match[0].trim());
        }
    }

    return matches;

};

/**
 * Checks if an input word is an emoticon.
 * @param token
 *      A String (single word),
 * @returns
 *      True if emoticon is detected.
 */
let isEmoticon = function(token) {

    let smiley = token.match(Regex.Patterns.smiley);
    let emoji = token.match(Regex.Patterns.emoji);

    return smiley || emoji;
};

/**
 * Tokenizes an input.
 * @param input
 *      A String (e.g. a sentence).
 * @return
 *      The detected tokens of the input.
 */
let getTokens = function (input) {
    const splitRegex = Regex.Patterns.split;
    return Tools.filter(input.split(splitRegex), txt => txt.trim().length > 0 && !isEmoticon(txt));
};


module.exports = {
    getEmoticons,
    getTokens,
    isEmoticon
};