/**
 * Created by Diana on 14.10.2017.
 */
const emojiRegex = require('emoji-regex');


// Definitions (partial regex definitions)

// general definitions

let punctuation = "[.,…;:?!\"\'\u201c\u201d\u2018\u2019/\b^]";
let brackets = "(?:[(){}\\]\\[<>]|&gt;|&lt;)";
let splitTokens = "(?:" + brackets + "+|" + punctuation + "+|\\s+|_+|(?:\\s+-\\s+))+";
let quoteChars = "(?:\"|“|”|&quot;|&laquo;|&raquo;|&ldquo;|&rdquo;|&bdquo;)";
let quotes = quoteChars + "([^\"]|\"\"|)*" + quoteChars; //"(?:\"|“”|&quot;([^\"]|\"\"|)*\")";

// url regex source: https://stackoverflow.com/questions/34818020/javascript-regex-url-extract-domain-only
let url = "^(?:https?:\\/\\/)?(?:[^@\n]+@)?(?:www\\.)?([^:\/\n\?\=]+)";

// smiley definitions

/*
 https://en.wikipedia.org/wiki/List_of_emoticons:

 let japan1 = "<_<|>_>|<_>|>_<|>.<|(^_^)|^_^|^^|(^.^)|^.^|(^_~)|^_~|(-.-)|-.-|d(^_^)b|(*_*)|*_*|(+_°)|+_°|(TT-TT)|(;_;)|T_T|;_;|(>_<)|(^)(>.<)(^)|(-_-)|-_-";
 let japan2 = "(ô_ó)|ô_ó|(q_q)|q_q|(ò_ó)|ò_ó|(ó_ò)|ó_ò|(O_O)|O_O|(o_O)|(^_^)\"|(^_^;)|(ó_O)|(o_O)|(3>_<3)|(♥_♥)|(._.);|(X.X)|X.X|(+.+)|+.+|(ö_ö)|ö_ö|(~_~)|~_~|~~|(@_@)|@_@|(,,,)---=^.^=---(,,,)";
 let surprised2 = "O_O|o-o|O_o|o_O|o_o|O-O";
 let happy =  ":-))|:-)|:)|:-]|:]|:-3|:3|:->|:>|8-)|8)|:-}|:}|:o)|:c)|:^)|=]|=)";
 let loughing = ":-D|:D|8‑D|8D|x-D|xD|X-D|XD|=D|=3|B^D";
 let sad = ":-(|:(|:-c|:c|:-<|:<|:-[|:[|>:[|:{|:@|>:(";
 let crying = ":'‑(|:'(";
 let tearsOfHappiness =  ":'‑)|:')";
 let horror = "D-':|D:<|D:|D8|D;|D=|DX";
 let surprise = ":‑O|:O|:-o|:o|:-0|8‑0|>:O";
 let kiss =  ":-*|:*|:×";
 let wink = ":3|;-)|;)|*-)|*)|;-]|;]|;^)|:-,|;D";
 let tongue =  ":-P|:P|X-P|XP|x-p|xp|:-p|:p|:-Þ|:Þ|:-þ|:þ|:-b|:b|d:|=p|>:P";
 let sceptical = ":-/|:/|:-.|>:\\|>:/|:\\|=/|=\\|:L|=L|:S";
 let straight = "(:-\|)|(:\|)";
 let emberassed = ":$";
 let tied =  ":-X|:X|:-#|:#|:-&|:&";
 let angel = "O:‑)|O:)|0:‑3|0:3|0:‑)|0:)|0;^)";
 let evil = ">:‑)|>:)|}:-)|}:)|3:‑)|3:)|>;)|";
 */

let smileyDefs = new function(){
    this.western = {};
    this.western.parts = {};
    this.western.parts.eyebrowsOrHats = "(?:[>}3DO0]|&gt;)?";
    this.western.parts.eyes = "[;:8=xXB]";
    this.western.parts.tears = "(?:')?";
    this.western.parts.nose = "[-^']?";
    this.western.parts.mouth = "(?:[()\\]\\[{}oO0cD<>@38*×,PpÞþb|$#&S/\b]|&gt;|&lt;|\\)\\))";
    this.western.parts.specials ="(?:D:|D8|D;|D=|DX|d:)";
    this.western.composition = "(?:" + this.western.parts.eyebrowsOrHats + this.western.parts.eyes + this.western.parts.tears + this.western.parts.nose + this.western.parts.mouth + "|" + this.western.parts.specials + ")";
    this.eastern = {};
    this.eastern.parts = {};
    this.eastern.parts.eyes = "(?:[<>^~-°T;ôóqò♥.Xx+ö@]|&gt;|&lt;|TT|3>|<3|3&gt;|&lt;3|^;)";
    this.eastern.parts.mouth = "[-_.]?";
    this.eastern.parts.ears = "(?:[db\";]|\\(\\^\\))?";
    this.eastern.parts.faceLeft = "(?:" + this.eastern.parts.ears + "\\()?";
    this.eastern.parts.faceRight = "(?:" + "\\)?" + this.eastern.parts.ears + ")?";
    this.eastern.composition = "(?:" + this.eastern.parts.faceLeft + this.eastern.parts.eyes + this.eastern.parts.mouth + this.eastern.parts.eyes + this.eastern.parts.faceRight + ")";
    this.general = {};
    this.general.hearts = "(?:</3|<[\b]3|<3|&lt;/3|&lt;[\b]3|&lt;3)";
    this.general.special = "(?:\\( ͡° ͜ʖ ͡°\\)|¯[\b]_\\(ツ\\)_/¯|ಠ_ಠ|\\(╯°□°\\）╯︵ ┻━┻|ノ┬─┬ノ ︵ \\( [\b]o°o\\)[\b]|ಠᴗಠ|\\(,,,\\)---=^.^=---\\(,,,\\))";
};

let smiley = "(:?" + smileyDefs.western.composition + "|" + smileyDefs.general.hearts + "|" + smileyDefs.eastern.composition + "|" + smileyDefs.general.special + ")";

// Regex ------------------------

let smileyRegex = new RegExp(smiley, "gm");
let splitRegex = new RegExp(splitTokens, "gm");
let emojisRegex = emojiRegex();
let quoteRegex = new RegExp(quotes, "gm");
let urlRegex = new RegExp(url, "im");

// Objects for export ------------------------

let Definitions = {
    punctuation: punctuation,
    brackets: brackets,
    split: splitTokens,
    smiley: smiley,
    quotes : quotes,
    url : url
};

let Patterns = {
    smiley: smileyRegex,
    emoji : emojisRegex,
    split: splitRegex,
    quotes : quoteRegex,
    url : urlRegex
};

module.exports = {
    Definitions,
    Patterns
};