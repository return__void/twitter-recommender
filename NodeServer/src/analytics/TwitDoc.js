/**
 * Created by Diana on 20.10.2017.
 */
const Tokenizer = require("./tokenizer");
const GeneralAnalysis = require("./analyserGeneral");
const SocialAnalysis = require("./analyserSocial");
const LanguageAnalysis = require("./analyserLanguage");
const ContentAnalysis = require("./analyserContent");


/**
 * Takes information of a user and analysis it
 * @param timeline
 *      The timeline of the user (array with twitter tweet objects).
 * @param user
 *      The user object.
 * @param friendIDs
 *      An array with ids of friends of the user.
 * @constructor
 */
function TwitDoc(timeline, user, friendIDs) {

    // information extraction ----------------------

    /**
     * tweets = array of tweet
     * every tweet is defined as follows:
     *
     * textExtended: "twitter text with urls & co",
     * textCleaned: "twitter text with no extras",
     * urls: [],
     * photos: [],
     * gifs: [],
     * atMentions: [],
     * quotes: [],
     * hashtags: [],
     * question: boolean,
     * exclamation: boolean,
     * language: "en",
     * emoticons : [],
     * tokens : [{word pos?}]
     */
    this.tweets = GeneralAnalysis.timelineContent(timeline);

    /**
     * add tokens and emoticons to tweets
     */
    this.tokeniseTweets = function (t) {

        // pos-tagger has problems detecting some words right
        // filter out words that may be wrong or are not
        // needed (like punctuation )
        let ignore = "(?:[,;.:$&()’/%€$\\[\\]{}\"“”^!?|]|&#x007C;)";
        let ignoreRegex = new RegExp(ignore, "gi");

        let ignoreMultiple = "(?:--)";
        let ignoreMultipleRegex = new RegExp(ignoreMultiple, "gi");

        let posFilter = function (tag, word) {
            if (word.length <= 1 && word != "I") {
                return false;
            } else if (word.match(ignoreRegex)) {
                return false;
            } else if (Tokenizer.isEmoticon(word)) {
                return false;
            } else if (word.length == 2 && word.match(ignoreMultiple)) {
                return false;
            } else {
                return true;
            }
        };

        t.forEach(tweet => {

            // smiley and emoji
            tweet.emoticons = Tokenizer.getEmoticons(tweet.textCleaned);

            // get words and pos tags
            tweet.tokens = LanguageAnalysis.posTagAndTokenize(tweet, posFilter);
        });
    }(this.tweets);

    // information analysis ----------------------

    /**
     * element to store analysis results
     * @type {{id: number, friends: [ids], behavior: {}, language: {}, content: {}}}
     */
    this.analysis = {

        general : {},
        behavior: {},
        language: {},
        content: {}
    };

    /**
     * get general analysis and stor it
     */
    this.general = function(container, user, friendsIDs) {

        container.id = user.id_str;
        container.friends = [];
        friendsIDs.forEach(id => {
            container.friends.push("" + id);
        });

    }(this.analysis.general, user, friendIDs);

    /**
     * get behavior analysis and store it
     */
    this.behavior = function (container, t, u) {
        container.general = {};
        container.general.tweetfrequency = GeneralAnalysis.tweetfrequency(t, u);
        container.general.listed = u.listed_count;
        container.general.friendsPerFollower =  u.friends_count / (u.followers_count == 0 ? 0.00001 : u.followers_count);
        container.interaction = SocialAnalysis.interaction(t, u);
        container.reaction = SocialAnalysis.reaction(t, u);
    }(this.analysis.behavior, timeline, user);

    /**
     * get language analysis and store it
     */
    this.language = function (container, tweets) {
        container.language = LanguageAnalysis.language(tweets);
    }(this.analysis, this.tweets);

    /**
     * get content analysis and store it
     */
    this.content = function (container, tweets) {
        container.about = {};
        container.about.hashtags = ContentAnalysis.groupHashtags(tweets);
        container.media = {};
        let media = ContentAnalysis.getMediaContent(tweets);
        for (let key in media) {
            container.media[key] = media[key];
        }
        container.about.keywords = ContentAnalysis.getKeyWords(tweets);
    }(this.analysis.content, this.tweets);

    // test stuff ---------------------------------

    /**
     * print language analysis to console
     */
    this.printLanguage = function () {

        console.log("emoticon: " + 100.0 * this.analysis.language.emoticon + "% of all tweets contain emoticon");
        console.log("quotes: " + 100.0 * this.analysis.language.quotes + "% of all tweets contain quotations");
        console.log("empty: " + 100.0 * this.analysis.language.tweetlength.empty + "% of all tweets are empty besides of hashtags, at-mentions and media");
        console.log("short: " + 100.0 * this.analysis.language.tweetlength.short + "% of all tweets are short (1-3 words)");
        console.log("mean words per tweet: " + this.analysis.language.tweetlength.average + " +- " + this.analysis.language.tweetlength.deviation);
        console.log("hashtags: " + 100.0 * this.analysis.language.hashtags.percentage + "% of all tweets contain hashtags");
        console.log("mean number of hashtags: " + this.analysis.language.hashtags.average + " +- " + this.analysis.language.hashtags.deviation);
        console.log("?: " + 100.0 * this.analysis.language.questions + "% of all tweets contain questions");
        console.log("!: " + 100.0 * this.analysis.language.exclamations + "% of all tweets contain exclamations");
        console.log("detected languages: " + JSON.stringify(this.analysis.language.languages));
        console.log("word usage: " + JSON.stringify(this.analysis.language.semantics));
    };

    /**
     * print behavior analysis to console
     */
    this.printBehavior = function () {
        let tweetFreq = this.analysis.behavior.tweetfrequency;
        let givingInteraction = this.analysis.behavior.interaction;
        let receivingInteraction = this.analysis.behavior.reaction;

        // general
        console.log("Tweetfreq (latest): " + tweetFreq.latest + " tweets per day");
        console.log("Tweetfreq (overall): " + tweetFreq.overall + " tweets per day");

        // interaction
        console.log("Giving Faves (overall): " + givingInteraction.favorites + " favorites per day");

        console.log("Doing Retweets (latest): " + givingInteraction.retweets.total + " retweets per day");
        console.log("Doing Retweets (latest): " + givingInteraction.retweets.percentage * 100 + "% of all tweets are retweets");

        console.log("At-Mentions (latest): " + givingInteraction.atMentions.total + " at-mentions per day");
        console.log("At-Mentions (latest): " + givingInteraction.atMentions.percentage * 100 + "% of all tweets contain at-mentions");
        console.log("People (latest): " + givingInteraction.people.recurring * 100 + "% of all at-mentions containing tweets include people that are mentioned more than once");
        console.log("People (latest): " + givingInteraction.people.average + " people mentioned in a at-mention containing tweet");

        console.log("Replying(latest): " + givingInteraction.replies.total + " tweets per day are replies");
        console.log("Replying(latest): " + givingInteraction.replies.percentage * 100 + "% of all tweets are replies");

        // reaction
        console.log("Getting Retweets (latest): " + receivingInteraction.retweets.percentage * 100 + "% of all tweets are getting retweeted (mean number of retweets per retweeted tweed: " + receivingInteraction.retweets.average + " +- " + receivingInteraction.retweets.deviation + ")");
        console.log("Getting Favorites (latest): " + receivingInteraction.favorites.percentage * 100 + "% of all tweets are getting favorited (mean number of favorits per favorited tweed: " + receivingInteraction.favorites.average + " +- " + receivingInteraction.favorites.deviation + ")");

    };

    /**
     * print content analysis to console
     */
    let printContent = function() {
        console.log(this.analysis.content);
    };

    /**
     * print tweets (pre-analysis)
     */
    this.printTweets = function () {
        this.tweets.forEach(t => {
            //console.log();

            if (t.urls && t.urls.length > 0) {
                for (let key in t) {
                    console.log(key + " : " + (typeof t[key] === "object" ? JSON.stringify(t[key]) : t[key]));
                }
            }

        });

    };

}

module.exports = {
    TwitDoc
};