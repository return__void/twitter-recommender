/**
 * Created by Diana on 20.10.2017.
 */

const Tools = require("../tools");
const Regex = require("./tokenizerPatterns");
const Language = require("./analyserLanguage");

/**
 * Calculates the similarity between two words.
 * @param word1
 *      The first word (String).
 * @param word2
 *      The second word (String).
 * @param n
 *      Number of characters in a subset (used for similarity calculation).
 * @returns
 *      {number} The amount of similarity in range of [0, 1].
 */
let ngram = function (word1, word2, n) {

    let w1 = word1.toLowerCase();
    let w2 = word2.toLowerCase();

    if (w1 == w2) {
        // exact match
        return 1.1;
    } else if ((w1.indexOf(w2) == 0 && w2.length > 4) || (w2.indexOf(w1) == 0 && w1.length > 4)) {
        // one word is part of the other word
        return 1;
    }

    // approx. match
    let parts1 = {};
    let parts2 = {};

    // create subsets of each word
    for (let i = 0; i <= w1.length - n; i++) {
        let part = w1.substring(i, i + n);
        parts1[part] = part;
    }

    for (let i = 0; i <= w2.length - n; i++) {
        let part = w2.substring(i, i + n);
        parts2[part] = part;
    }

    // look for similar subsets
    let sameParts = 0;

    for (let key in parts1) {
        if (key in parts2) {
            sameParts++;
        }
    }

    let sim = sameParts / Math.max(Object.keys(parts1).length, Object.keys(parts2).length);

    return sim;
};

/**
 * Calculates the similarity of a word to a word group.
 * @param word
 *      The word (string).
 * @param group
 *      A word group with attributes "words" (String array) and "count" (int).
 * @returns
 *      {number} The amount of similarity in range of [0, 1] | 1.1. 1.1 means there was a exact match between the word and one member of the word group.
 */
let groupSimilarity = function (word, group) {

    let sum = 0;

    let best = 0;

    for (let index in group.words) {
        let sim = ngram(group.words[index], word, 2);
        if (sim > best) {
            best = sim;
        }
        sum += sim;
    }

    if (best >= 1) {
        return best;
    } else {
        return sum / group.words.length;
    }
};

/**
 * Finds the most important hashtags within the input tweets. The most
 * important hashtags are hashtags that are used the most. Similar hashtags
 * are grouped and counted as one hashtag. For each hashtag the number of
 * appearances is counted.
 * @param tweets
 *      Tweets of a timeline of a user.
 * @returns
 *      An object with each sub-object containing a hashtag group (with attribute "words" - String array and "count" - int).
 */
let groupHashtags = function (tweets) {

    /*
     let exampleGroup = {
     words: ["hallo", "hello", "allo"],
     count: 4
     };
     */

    let groups = {};
    let matches = [];

    // iterate over all tweets
    tweets.forEach(tweet => {

        // iterate over all hashtags of the tweet
        let hashtags = tweet.hashtags;
        hashtags.forEach(hashtag => {

            // find matching existing hashtags (word groups)
            // may find more than one matching
            matches = [];
            let doAdd = true;
            for (let key in groups) {
                let sim = groupSimilarity(hashtag, groups[key]);
                if (sim >= 1) {
                    if (sim == 1) {
                        groups[key].words.push(hashtag);
                    }
                    groups[key].count++;
                    doAdd = false;
                    break;
                } else if (sim >= 0.6) {
                    matches.push({key: key, sim: sim});
                }
            }

            if (doAdd && matches.length > 0) {

                // find best match if multiple matches are found
                let key = 0;
                let sim = 0;

                matches.forEach(match => {
                    if (match.sim > sim) {
                        sim = match.sim;
                        key = match.key;
                    }
                });


                groups[key].words.push(hashtag);
                groups[key].count++;


            } else if (doAdd) {

                // no match found
                // add new hashtag to storage

                groups[hashtag] = {
                    words: [hashtag],
                    count: 1
                }
            }


        });
    });

    // find average number of hashtag occurences
    // filter all hashtags that are below average
    let avgCount = 0;

    for (let key in groups) {
        avgCount += groups[key].count;
    }

    let groupsLength = Object.keys(groups).length;

    if (groupsLength != 0) {
        avgCount /= groupsLength;
    }

    let keyHashtags = {};

    for (let key in groups) {
        if (groups[key].count >= avgCount) {
            keyHashtags[key] = {
                word: groups[key].words,
                count: groups[key].count
            };
        }
    }

    return keyHashtags;
};

/**
 * Extracts the domain of an url, e.g. "http://www.uni-hildesheim.de" -> "uni-hildesheim"
 * @param url
 *      The url.
 * @returns
 *      {string} The domain in the url.
 */
let extractDomainName = function (url) {
    let domain = url.match(Regex.Patterns.url)[0];
    let domainSplit = domain.split(/[.]|\/\//);
    return domainSplit[domainSplit.length - 2].toLowerCase();
};

/**
 * Extracts the occurrence of media in a timeline.
 * @param tweets
 *       Tweets of a timeline of a user.
 * @returns
 *      {urls: number, videos: number, photos: number, gifs: number, domains: Array}
 */
let getMediaContent = function (tweets) {
    let domains = {};
    let videos = 0;
    let gifs = 0;
    let photos = 0;
    let urls = 0;

    tweets.forEach(tweet => {
        if (tweet.videos.length > 0) {
            videos++;
        }

        if (tweet.gifs.length > 0) {
            gifs++;
        }

        if (tweet.photos.length > 0) {
            photos++;
        }

        if (tweet.urls.length > 0) {
            urls++;
        }

        tweet.urls.forEach(url => {
            let domain = extractDomainName(url);

            // cheap map, maybe for later a real map
            // with all known abbr. to full domain name
            if (domain == 'fb') {
                domain = 'facebook';
            } else if (domain == 'wh') {
                domain = 'whitehouse';
            } else if (domain == 'flic') {
                domain = "flickr";
            } else if (domain == 'instagr') {
                domain = 'instagram';
            } else if (domain == 'nyti') {
                domain = 'nytimes';
            }

            domains[domain] = domains[domain] || 0;
            domains[domain]++;
        });


    });

    return {
        urls: tweets.length == 0 ? 0 : urls / tweets.length,
        videos: tweets.length == 0 ? 0 : videos / tweets.length,
        photos: tweets.length == 0 ? 0 : photos / tweets.length,
        gifs: tweets.length == 0 ? 0 : gifs / tweets.length,
        domains: Object.keys(Tools.filter(domains, domain => domain > 1))
    };
};

/**
 * Finds the most important keywords within the input tweets. The most
 * important keyword are nouns that are used the most. Similar keyword
 * are grouped and counted as one keyword. For each keyword the number of
 * appearances is counted.
 * @param tweets
 *      Tweets of a timeline of a user.
 * @returns
 *      An object with each sub-object containing a keyword group (with attribute "words" - String array and "count" - int).
 */
let getKeyWords = function (tweets) {

    let keywords = {};

    let matches = [];

    // iterate over all tweets
    tweets.forEach(tweet => {

        // iterate over all detected tokens
        for (let i in tweet.tokens) {

            // check if word is a noun
            let category = undefined;

            if (!tweet.tokens[i].tag && tweet.tokens[i].word[0] !== tweet.tokens[i].word[0].toLowerCase() && !Language.isStopWord(tweet.tokens[i].word)) {
                // pos-tag is missing, but word starts with
                // uppercase, therefor name or noun assumed
                category = 'noun';
            } else if (!tweet.tokens[i].tag) {
                // can't create keywords when pos-tag is missing
                // pos-tags are only working for english
                break;
            } else {
                category = Language.getPosCategory(tweet.tokens[i])
            }



            if (category == 'noun') {

                // find matching existing keywords (word groups)
                // may find more than one matching
                matches = [];
                let doAdd = true;
                let word = tweet.tokens[i].word.toLowerCase();
                for (let key in keywords) {
                    let sim = ngram(tweet.tokens[i].word, key);

                    if (sim >= 1) {

                        for (let j in keywords[key].words) {
                            if (keywords[key].words[j] == word) {
                                sim = 1.1;
                                break;
                            }
                        }
                        if (sim == 1) {
                            keywords[key].words.push(word);
                        }
                        keywords[key].count++;
                        doAdd = false;
                        break;
                    } else if (sim >= 0.6) {
                        matches.push({key: key, sim: sim});
                    }
                }

                if (doAdd && matches.length > 0) {

                    // find best match when multiple are matching
                    let key = 0;
                    let sim = 0;

                    matches.forEach(match => {
                        if (match.sim > sim) {
                            sim = match.sim;
                            key = match.key;
                        }
                    });


                    keywords[key].words.push(word);
                    keywords[key].count++;


                } else if (doAdd) {
                    // no match found, add new keyword
                    keywords[word] = {
                        words: [word],
                        count: 1
                    }
                }
            }

        }
    });

    // find most important keywords, remove others
    let counts = [];
    let avgCount = 0;

    for (let key in keywords) {
        counts.push(keywords[key].count);
        avgCount += keywords[key].count;
    }
    counts.sort((a, b) => a - b);
    avgCount /= counts.length;

    let words = {};

    let cutoff = Math.round(counts.length * 0.95);
    cutoff = cutoff > counts.length - 2 ? counts.length - 2 : cutoff;
    cutoff = cutoff < 0 ? 0 : cutoff;
    let quantil = counts[cutoff] + 1;

    for (let key in keywords) {
        if (keywords[key].count >= quantil && keywords[key].count > avgCount) {
            words[key] = {
                word: keywords[key].words,
                count: keywords[key].count
            };
        }
    }

    return words;
};


module.exports = {
    ngram,
    groupHashtags,
    getMediaContent,
    getKeyWords
};