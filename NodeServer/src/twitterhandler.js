const Twitter = require('./config').Twitter;
const Tools = require('./tools');

let apiCounter = 0;

function TwitterAuth() {
    let twit = Twitter[apiCounter];
    apiCounter = ++apiCounter >= Twitter.length ? 0 : apiCounter;
    return twit;
}

function get(path, options, callback) {
    TwitterAuth().get(path, options, (err, data, response) => callback(err, data));
}

// --------- tools

/**
 * Callbackfunction which is used to find user information to a list of ids (in getFriends / getFollower)
 * @param err
 *      undefined or error object when error
 * @param data
 *      undefined when error or array with user ids
 * @param callback
 *
 */
let idsToListCallback = function(err, data, callback) {

    if (err) {
        return callback(err, data);
    }

    // data.ids = array with up to 5000 elements

    if (data.ids.length > 100) {
        // multiple requests are needed

        let partitions = Tools.createPartitions(data.ids, 100);
        for (let i = 0; i < partitions.length; i++) {
            partitions[i] = partitions[i].join(",");
        }

        let usersOptions = {
            ids : partitions
        };

        let store = function(replies, options, element, data) {

            replies.users = replies.users || [];

            // data = array
            // save replies
            for (let i in data) {
                replies.users.push(data[i]);
            }

            return replies;
        };

        requestPerElement(getUsers, usersOptions, store, callback);
    } else {
        // just one request is needed

        let usersOptions = {
            user_id : data.ids.join(",")
        };

        let userInfoCallBack = function(err, data) {

            let newData = data;

            if (!err) {
                newData = {};
                newData.users = data;
            }

            callback(err, newData);
        };

        getUsers(usersOptions, userInfoCallBack);
    }

    /*
     let sampled = Tools.randomSample(data.users, 10);
     data.users = sampled;*/
};

// request pro id from id list : options.ids | options.list
/**
 * Makes multiple requests (one request per element). Call callback when all requests are answered.
 * @param action
 *      An action function - e.g. getUser(), getFriends() - with inputs (options, callback[err, data]). This function
 *      is called with each element.
 * @param options
 *      Request options for each element. This object should either contain a key named "list" or a key named "ids".
 *      When "ids" are given an array is expected (e.g. each element of the array is a user id).
 *      When 'list" is given an array with user objects is expected
 * @param store
 *      A store function which takes inputs (replies, options, element, data).
 *      replies : object to store the data
 *      options : same options as input options for requestPerElement
 *      element : element of the given array (from options.list or options.ids) / request parameter
 *      data : the returned data for the element / request
 * @param callback
 */
var requestPerElement = function (action, options, store, callback) {

    // options for each request (id gets replaced in each iteration)
    let userOptions = options;
    userOptions.user_id = 0;

    // object to store the replies
    let replies = {};

    // function for looping (just definition, implementation above)
    let loopPaused = false;
    let loop = () => {
    };

    // for each id (twitter user) one request has to be send

    // storing ids in a stack (requestStack object behaves similar to an array)
    // each element (id of twitter user) passes 3 stages:
    // stage 1 : request not send yet (element is in "toDo")
    // stage 2 : request is send but reply is not received yet (element is in "inProgress" but not in "toDo")
    // stage 3 : reply is received and stored in "replies" (element is neither in "toDo" nor in "inProgress")
    let requestStack = {

        // number of elements that have been stored in replies object
        count: undefined,

        // total number of elements / ids
        elements: undefined,

        // arrays that represent the processing status of an id
        toDo: [],
        inProgress: [],

        // if true, reply can be send aka callback is called (everything is finished)
        isEmpty: function () {
            return this.count != undefined && this.toDo.length == 0 && this.inProgress.length == 0;
        },

        // this should be called after last id is added to this object
        initCount: function () {
            this.count = 0;
            this.elements = this.toDo.length;
        },

        // adds an element to the object
        push: function (newElement) {
            this.toDo.push(newElement);
        },

        // moves an element from toDO to inProgress. Returns undefined when toDo is empty
        pop: function () {

            if (this.toDo.length > 0) {
                let element = this.toDo.pop();
                this.inProgress.push(element);

                return element;
            } else {
                return undefined;
            }
        },

        // removes an element from inProgess
        finalize: function (element) {
            let index = -1;

            // find element in array
            for (let i = 0; i < this.inProgress.length; i++) {
                if (element == this.inProgress[i]) {
                    index = i;
                    break;
                }
            }

            // remove element with given index
            this.inProgress.splice(index, 1);

            this.count++;
        }

    };

    // applying all ids to the stack
    let listLength = 'ids' in options ? options.ids.length : options.list.length;
    for (let i = 0; i < listLength; i++) {
        let element = 'ids' in options ? options.ids[i] : options.list[i].id;
        requestStack.push(element);
    }
    requestStack.initCount();

    // callback for each action request
    let actionCallback = function (element, err, data) {
        if (err) {

            if (err.code == 34) {
                // err.Text : Sorry, that page does not exist
                // not all ids may exist (or are public)
                // remove as error and provide empty data for following steps
                data = [];
                err = undefined;
            }  else if (err.code == 88) {
             console.log("Rate limit exeeded");
             // err.Text : Rate limit exeeded
             // requests not done yet, but need to wait to continue

             // pause loop
             loopPaused = true;

             // check if loop is finished aka no element is in to do list
             let wasEmpty = requestStack.toDo.length == 0;

             // add rejected element to to do list
             requestStack.push(element);

             // restart loop after a certain time
             if (wasEmpty) {
             setTimeout(() => {
             loopPaused = false;
             loop();
             }, 1000);

             }
             }
            else {
                // unknown error, can't catch it...
                 return callback(err, undefined);
            }
        }

        // record that the reply has arrived
        requestStack.finalize(element);
        // save data
        store(replies, options, element, data);

        // respond when all request have been replied
        if (requestStack.isEmpty()) {
            callback(undefined, replies);
        }
    };

    // does an request with known id (element)
    let makeRequest = function (element) {
        userOptions.user_id = element;
        //getTimeline
        action(userOptions, (err, data) => {
            actionCallback(element, err, data);
        });
    };

    // a loop function that makes little pauses between the requests
    // loop stops when loopPaused is true
    loop = function () {

        if (loopPaused) {
            return;
        }

        // get an element / id from stack
        let element = requestStack.pop();
        //console.log("[" + element + "] " + requestStack.toDo.length + " of " + requestStack.elements);

        if (element != undefined) {
            // make timeline request and do loop
            setTimeout(() => {
                makeRequest(element);
                return loop();
            }, 5);
        } // else  stack is empty, stop loop
    };

    // start loop
    loop();
};

/**
 * Finds followers or friends of a user (max 5000).
 * @param path
 *      "friends" or "followers"
 * @param options
 *      options object for the request. Should contain key format = 'list' or format = 'ids'
 * @param callback
 *      Called after request has been replied. Input is (err, data).
 */
var getFollowersOrFriends = function(path, options, callback) {
    // request either as ids or list width users possible, but max. number of replied data differs
    // ids : 20 replies per request, list : 5000 per request
    // solution: first ask for ids, the collect user objects with getUsers()
    let pipeCallback = options.format == 'list' ? (err, data) => idsToListCallback(err, data, callback): (err, data) => callback(err, data);
    return get(path + "/ids", options, pipeCallback);
};

// --------- public functions

// max 100 user pro request
var getUsers = function (options, callback) {
    let path = 'users/lookup';
    return get(path, options, callback);
};

var getFavorites = function (options, callback) {
    let path = 'favorites/list';
    return get(path, options, callback);
};

// max 5000
var getFollowers = function (options, callback) {
    getFollowersOrFriends("followers", options, callback);
};

// max 5000
var getFriends = function (options, callback) {
    getFollowersOrFriends("friends", options, callback);
};

var getFriendsOrFollowersFromUsers = function (options, callback) {

    let store = function (replies, options, element, data) {

        // save replies
        replies[element] = {};
        replies[element][options.who] = data;
        if (options.list) {
            // if list with user information is provided, then save it in "replies"
            // look for user with id in input array
            for (let index in options.list) {
                if (element == options.list[index].id) {
                    replies[element].user = options.list[index];
                    break;
                }
            }
        }
        return replies;

    };

    let action = options.who == 'friends' ? getFriends : getFollowers;
    requestPerElement(action, options, store, callback);
};

var getTimeline = function (options, callback) {
    return get('statuses/user_timeline', options, callback);
};

/**
 * Finds the timelines from a set of users.
 * @param options
 *      An object with following keys:
 *      options.req.list with an array of user objects or options.req.ids an array of twitter user ids (numbers)
 *      options.req.count The number of tweets per user
 * @param callback
 *      A function with two inputs (error, data). This function will be called when all timelines are found.
 */
var getTimelines = function (options, callback) {

    let store = function (replies, options, element, data) {

        // filter out non public / empty timelines
        if (data.length > 0) {

            // save replies
            replies[element] = {};
            replies[element].timeline = data;
            if (options.list) {
                // if list with user information is provided, then save it in "replies"
                // look for user with id in input array
                for (let index in options.list) {
                    if (element == options.list[index].id) {
                        replies[element].user = options.list[index];
                        break;
                    }
                }
            }
            return replies;
        }
    };

    requestPerElement(getTimeline, options, store, callback);

};

let getTweet = function(options, callback) {
    return get('statuses/show', options, callback);
};

module.exports = {
    getTweet,
    getUsers,
    getFollowers,
    getFriends,
    getFriendsOrFollowersFromUsers,
    getTimeline,
    getTimelines,
    getFavorites,
    requestPerElement
};
