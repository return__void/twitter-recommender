const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const TwitterHandler = require('./twitterhandler');
const Tools = require('./tools');
const Analyser = require('./analytics/analyser');
const cors = require('cors');


// body in JSON parsen
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cors());

// gets one user object from a user name / user id
app.route('/user/:user').get((req, res, next) => {

    let callback = function (err, data) {
        res.send(err ? err : data[0]);
    };

    let options = {
        tweet_mode: "extended"
    };

    let key = Tools.isNumber(req.params.user) ? 'user_id' : 'screen_name';

    if (key == 'screen_name' && req.params.user[0] == '@') {
        req.params.user = req.params.user.slice(1, req.params.user.length);
    }

    options[key] = req.params.user;

    TwitterHandler.getUsers(options, callback);
});

// gets multiple user objects from a list of ids. Ids are send per body
app.route('/users').post((req, res, next) => {
    let ids = req.body.ids;

    let callback = function (err, raw) {
        let data = [];

        for (let i = 0; i < ids.length; i++) {
            data[i] = {};
            for (let j in raw) {
                if (ids[i] == raw[j].id_str) {
                    data[i] = raw[j];
                    break;
                }
            }
        }

        res.send(err ? err : data);
    };

    let options = {
        tweet_mode: "extended"
    };

    options.user_id = ids;

    TwitterHandler.getUsers(options, callback);

});

// gets the tweet with the id
app.route('/tweet/:id').get((req, res) => {

    let callback = function (err, data) {
        res.send(err ? err : data);
    };

    let options = {
        id : req.params.id,
        trim_user: true,
        include_entities : true,
        tweet_mode: "extended"
    };


    TwitterHandler.getTweet(options, callback);
});

// gets the TwitDoc (analysis) of a user
app.route("/twitdoc/:user/:friends(friends)?/:count(\\d+)?").get((req, res) => {


    let options = {
        count: req.params.count == undefined ? 200 : req.params.count,
        key :  Tools.isNumber(req.params.user) ? 'user_id' : 'screen_name',
        friends : req.params.friends ? true : false
    };

    if (options.key == 'screen_name' && req.params.user[0] == '@') {
        req.params.user = req.params.user.slice(1, req.params.user.length);
    }

    options.value = req.params.user;

    Analyser.getUserAnalyis(options, (err, data) => {

        res.send(err ? err : data)
    });
});

// gets the tweets that are favored by the input user
app.route('/favorites/:user/:count(\\d+)?').get((req, res) => {
    let callback = function (err, data) {
        res.send(err ? err : {
            original: data,
            tweetids: Tools.extract(data, "id"),
            extracted: Tools.extract(data, "user.id")
        });
    };

    let options = {
        count: req.params.count == undefined ? 20 : req.params.count,
        tweet_mode: "extended"
    };

    let key = Tools.isNumber(req.params.user) ? 'user_id' : 'screen_name';
    options[key] = req.params.user;

    TwitterHandler.getFavorites(options, callback);


});

// gets the timeline of a user
app.route('/timeline/:user/:count(\\d+)?').get((req, res) => {

    let callback = function (err, data) {
        res.send(err ? err : data);
    };

    let options = {
        count: req.params.count == undefined ? 20 : req.params.count,
        trim_user: true,
        tweet_mode: "extended"
    };

    let key = Tools.isNumber(req.params.user) ? 'user_id' : 'screen_name';
    options[key] = req.params.user;

    TwitterHandler.getTimeline(options, callback);
});

// gets the timelines of the followers / friends of a user
app.route('/timeline/:user/:from(follower|friends)/:userslimit(\\d+)?/:timelinelimit(\\d+)?').get((req, res) => {
    let callback = function (err, data) {
        res.send(err ? err : data);
    };

    let next = function (err, data) {
        if (err) {
            return callback(err, data);
        }

        let timelineOptions = {
            trim_user: true,
            list: Tools.filter(data.users, user => user.protected == true || user.statuses_count > 0),
            count: req.params.timelinelimit != undefined ? req.params.timelinelimit : 20,
            format: 'list',
            tweet_mode: "extended"
        };

        TwitterHandler.getTimelines(timelineOptions, callback);
    };

    let options = {
        count: req.params.count == undefined ? 20 : req.params.count,
        format: 'list'
    };

    let key = Tools.isNumber(req.params.user) ? 'user_id' : 'screen_name';
    options[key] = req.params.user;

    if (req.params.from == 'follower') {
        TwitterHandler.getFollowers(options, next);
    } else {
        TwitterHandler.getFriends(options, next);
    }
});

// gets the follower or friends of a user
app.route('/:who(friends|follower)/:user/:output(list|ids)?/:count(\\d+)?').get((req, res) => {

    let callback = function (err, data) {
        res.send(err ? err : data);
    };

    let options = {
        count: req.params.count == undefined ? 20 : req.params.count,
        format: req.params.output != undefined ? req.params.output : 'list',
        tweet_mode: "extended"
    };

    let key = Tools.isNumber(req.params.user) ? 'user_id' : 'screen_name';
    options[key] = req.params.user;


    if (req.params.who == 'friends') {
        TwitterHandler.getFriends(options, callback);
    } else {
        TwitterHandler.getFollowers(options, callback);
    }
});

// gets the follower / friends of the follower / friends of a user
app.route('/:who(friends|follower)/:user/:from(friends|follower)/:output(list|ids)?/:whocount(\\d+)?/:fromcount(\\d+)?').get((req, res) => {
    let callback = function (err, data) {
        res.send(err ? err : data);
    };

    let nextPeople = function (err, data) {
        if (err) {
            return callback(err, data);
        }

        let nextOptions = {

            // filter out the people that don't have friends / followers
            list: Tools.filter(data.users, user => user.protected == true || req.params.from == 'friends' ? user.friends_count > 0 : user.follower_count > 0),
            who: req.params.from,
            count: req.params.fromcount != undefined ? req.params.fromcount : 20,
            format: req.params.output != undefined ? req.params.output : 'list',
            tweet_mode: "extended"
        };

        TwitterHandler.getFriendsOrFollowersFromUsers(nextOptions, callback);

    };


    let options = {
        count: req.params.whocount == undefined ? 5 : req.params.whocount,
        format: 'list',
    };

    let key = Tools.isNumber(req.params.user) ? 'user_id' : 'screen_name';
    options[key] = req.params.user;

    if (req.params.who == 'friends') {
        TwitterHandler.getFriends(options, nextPeople);
    } else {
        TwitterHandler.getFollowers(options, nextPeople);
    }
});


module.exports = {
    app
};