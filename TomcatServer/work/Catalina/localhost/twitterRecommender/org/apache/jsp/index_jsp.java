/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: Apache Tomcat/7.0.65
 * Generated at: 2017-12-14 20:40:05 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  private volatile javax.el.ExpressionFactory _el_expressionfactory;
  private volatile org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public javax.el.ExpressionFactory _jsp_getExpressionFactory() {
    if (_el_expressionfactory == null) {
      synchronized (this) {
        if (_el_expressionfactory == null) {
          _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
        }
      }
    }
    return _el_expressionfactory;
  }

  public org.apache.tomcat.InstanceManager _jsp_getInstanceManager() {
    if (_jsp_instancemanager == null) {
      synchronized (this) {
        if (_jsp_instancemanager == null) {
          _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
        }
      }
    }
    return _jsp_instancemanager;
  }

  public void _jspInit() {
  }

  public void _jspDestroy() {
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
        throws java.io.IOException, javax.servlet.ServletException {

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("<!doctype html>\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<meta charset=\"utf-8\">\r\n");
      out.write("<meta http-equiv=\"language\" content=\"en\">\r\n");
      out.write("<title>Twitter Recommender</title>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!-- Im JSP - dadurch wird doGet() vom Servlet aufgerufen -->\r\n");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "ConfigServlet", out, true);
      out.write("\r\n");
      out.write("<meta port=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${port}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null, false));
      out.write("\">\r\n");
      out.write("\r\n");
      out.write("<link rel=\"shortcut icon\" href=\"img/favicon.png\">\r\n");
      out.write("<link\r\n");
      out.write("\thref=\"https://fonts.googleapis.com/css?family=Amatic+SC|Roboto+Slab|PT+Serif:400i\"\r\n");
      out.write("\trel=\"stylesheet\">\r\n");
      out.write("<link rel=\"stylesheet\" href=\"style/format.css\" type=\"text/css\"\r\n");
      out.write("\tmedia=\"all\">\r\n");
      out.write("<script src=\"https://code.jquery.com/jquery-3.2.1.min.js\"\r\n");
      out.write("\tintegrity=\"sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=\"\r\n");
      out.write("\tcrossorigin=\"anonymous\"></script>\r\n");
      out.write("<script src=\"scripts/loadingbar.js\"></script>\r\n");
      out.write("<script src=\"scripts/fontscript.js\"></script>\r\n");
      out.write("<script src=\"scripts/tools.js\"></script>\r\n");
      out.write("<script src=\"scripts/cbrHandler.js\"></script>\r\n");
      out.write("<script src=\"scripts/script.js\"></script>\r\n");
      out.write("</head>\r\n");
      out.write("\r\n");
      out.write("<body>\r\n");
      out.write("\t<header>\r\n");
      out.write("\t\t<h1>&raquo; Twitter Recommender &laquo;</h1>\r\n");
      out.write("\t\t<nav id=\"font-size\">\r\n");
      out.write("\t\t\t<ul>\r\n");
      out.write("\t\t\t\t<li>A-</li>\r\n");
      out.write("\t\t\t\t<li>A</li>\r\n");
      out.write("\t\t\t\t<li>A+</li>\r\n");
      out.write("\t\t\t</ul>\r\n");
      out.write("\t\t</nav>\r\n");
      out.write("\t</header>\r\n");
      out.write("\t<main>\r\n");
      out.write("\t<section id=\"about\">\r\n");
      out.write("\t\t<h2>\r\n");
      out.write("\t\t\t<p class=\"section_id\">\r\n");
      out.write("\t\t\t\t<a name=\"n00\"></a><span>00.</span>\r\n");
      out.write("\t\t\t</p>\r\n");
      out.write("\t\t\t&nbsp;About\r\n");
      out.write("\t\t</h2>\r\n");
      out.write("\t\t<div class=\"content\">\r\n");
      out.write("\t\t\t<p class=\"columns\">\r\n");
      out.write("\t\t\t\t<b>Goals:</b> This project tries to find recommendations for whom to\r\n");
      out.write("\t\t\t\tfollow on Twitter. Twitter itself doesn't provide a proper\r\n");
      out.write("\t\t\t\trecommender. Therefore a system is needed to find better\r\n");
      out.write("\t\t\t\trecommendations that are not only based on existing social\r\n");
      out.write("\t\t\t\tconnections but on similarities between users. This recommender\r\n");
      out.write("\t\t\t\tanalyses the Twitter pages of users and extracts three features:\r\n");
      out.write("\t\t\t\t\"language\" (e.g. emoticon usage), \"content\" (e.g. keywords and\r\n");
      out.write("\t\t\t\thashtags) and \"behavior\" (e.g. number of tweets per day). These\r\n");
      out.write("\t\t\t\tfeatures are used to detect similarities using the case-based\r\n");
      out.write("\t\t\t\treasoning engine \"myCBR\".<br>\r\n");
      out.write("\t\t\t\t<b>Instructions:</b> (1) CBR engine loading - When this page loads\r\n");
      out.write("\t\t\t\tfor the first time, the CBR engine starts automatically. Please wait\r\n");
      out.write("\t\t\t\tfor your first input until the engine is running. This may take a\r\n");
      out.write("\t\t\t\twhile when the case base is large. You can check the state of the\r\n");
      out.write("\t\t\t\tCBR engine at the very bottom of the page in the footer. (2) User\r\n");
      out.write("\t\t\t\tInput - Go to section \"01. Who are you?\". Type in the Twitter user\r\n");
      out.write("\t\t\t\tname you want a recommendation for, e.g. \"@tonkuhle\" or \"tonkuhle\",\r\n");
      out.write("\t\t\t\tand hit \"submit\". If the user was found, the next section will be\r\n");
      out.write("\t\t\t\trevealed. (3) Feature extraction - Go to section \"02. Who do you\r\n");
      out.write("\t\t\t\tfollow?\" and hit \"Start feature extraction\". Wait until the loading\r\n");
      out.write("\t\t\t\tbar is finished and the last section is displayed. This step will\r\n");
      out.write("\t\t\t\tupdate the case base with the friends of the input Twitter user. In\r\n");
      out.write("\t\t\t\taddition a cluster of friends of the input user is found that\r\n");
      out.write("\t\t\t\tcontains people that are similar to each other concerning one of the\r\n");
      out.write("\t\t\t\tthree main features \"language\", \"behavior\" or \"content\". The results\r\n");
      out.write("\t\t\t\tof the cluster process are displayed. If the clusters are not\r\n");
      out.write("\t\t\t\tsatisfying, please press again \"Start feature extraction\" button. (4)\r\n");
      out.write("\t\t\t\tRecommendations - Go to section \"03. Find new people to follow!\"\r\n");
      out.write("\t\t\t\tand hit the \"Find new friends\" button. Wait again for the loading\r\n");
      out.write("\t\t\t\tbar to finish. After that, up to eight recommendations are\r\n");
      out.write("\t\t\t\tdisplayed. For every recommendation a link is provided that will\r\n");
      out.write("\t\t\t\ttarget you to the Twitter pager of the recommended user. The results\r\n");
      out.write("\t\t\t\tare bases on detected similarities between the input user, the found\r\n");
      out.write("\t\t\t\tclusters of section two and common friends of the input users\r\n");
      out.write("\t\t\t\tfriends.<br>\r\n");
      out.write("\t\t\t\t<b>Limitations:</b> The recommendations are based on a content\r\n");
      out.write("\t\t\t\tanalysis of the public Twitter page of the input user and of his or\r\n");
      out.write("\t\t\t\ther friends. Therefore the recommendation will only work if the\r\n");
      out.write("\t\t\t\tinput user a) is public, b) has tweeted something that are not\r\n");
      out.write("\t\t\t\tretweets and c) has at least one friend (one user the he or she\r\n");
      out.write("\t\t\t\tfollows) that matches the restrictions of a) and b). The\r\n");
      out.write("\t\t\t\trecommendations are limited to the known cases of the CBR case base.\r\n");
      out.write("\t\t\t\tFor every recommendation cycle the case base is extended with new\r\n");
      out.write("\t\t\t\tTwitter users and (some) existing cases are updated. As the case\r\n");
      out.write("\t\t\t\tbase grows, the recommendations will be more accurate.\r\n");
      out.write("\r\n");
      out.write("\t\t\t</p>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\r\n");
      out.write("\t</section>\r\n");
      out.write("\t<section id=\"who\">\r\n");
      out.write("\t\t<h2>\r\n");
      out.write("\t\t\t<p class=\"section_id\">\r\n");
      out.write("\t\t\t\t<a name=\"n01\"></a><span>01.</span>\r\n");
      out.write("\t\t\t</p>\r\n");
      out.write("\t\t\t&nbsp;Who are you?\r\n");
      out.write("\t\t</h2>\r\n");
      out.write("\t\t<div id=\"user\" class=\"content\">\r\n");
      out.write("\t\t\t<p>Please, type in your twitter user name or your twitter user\r\n");
      out.write("\t\t\t\tid:</p>\r\n");
      out.write("\t\t\t<form method=\"get\" id=\"get_user\">\r\n");
      out.write("\t\t\t\t<input type=\"text\" id=\"username\"> <input value=\"Submit\"\r\n");
      out.write("\t\t\t\t\ttype=\"submit\">\r\n");
      out.write("\t\t\t</form>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t</section>\r\n");
      out.write("\t<section id=\"clustering\">\r\n");
      out.write("\t\t<h2>\r\n");
      out.write("\t\t\t<p class=\"section_id\">\r\n");
      out.write("\t\t\t\t<a name=\"n02\"></a><span>02.</span>\r\n");
      out.write("\t\t\t</p>\r\n");
      out.write("\t\t\t&nbsp;Who do you follow?\r\n");
      out.write("\t\t</h2>\r\n");
      out.write("\t\t<div id=\"friends\" class=\"content\">\r\n");
      out.write("\t\t\t<p>During this step similarities between the people you follow\r\n");
      out.write("\t\t\t\tare extracted. For each feature - behavior, language and content - a\r\n");
      out.write("\t\t\t\tsubset of friends is found which represents the feature in a uniform\r\n");
      out.write("\t\t\t\tway.</p>\r\n");
      out.write("\t\t\t<p class=\"center\">\r\n");
      out.write("\t\t\t\t<button id=\"start_friends_analysis\">Start feature\r\n");
      out.write("\t\t\t\t\textraction</button>\r\n");
      out.write("\t\t\t</p>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t</section>\r\n");
      out.write("\t<section id=\"results\">\r\n");
      out.write("\t\t<h2>\r\n");
      out.write("\t\t\t<p class=\"section_id\">\r\n");
      out.write("\t\t\t\t<a name=\"n03\"></a><span>03.</span>\r\n");
      out.write("\t\t\t</p>\r\n");
      out.write("\t\t\t&nbsp;Find new people to follow!\r\n");
      out.write("\t\t</h2>\r\n");
      out.write("\t\t<div id=\"new-people\" class=\"content\">\r\n");
      out.write("\t\t\t<p>Finding new friends based on the people you already follow.</p>\r\n");
      out.write("\t\t\t<p class=\"center\">\r\n");
      out.write("\t\t\t\t<button id=\"get-results\">Find new friends</button>\r\n");
      out.write("\t\t\t</p>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t</section>\r\n");
      out.write("\r\n");
      out.write("\t</main>\r\n");
      out.write("\t<footer>\r\n");
      out.write("\t\t<div class=\"content\">\r\n");
      out.write("\t\t\t<p>\r\n");
      out.write("\t\t\t\t<span id=\"engine_state\">Engine is starting</span>&nbsp;&bull;&nbsp;\r\n");
      out.write("\t\t\t\tCopyright by Diana Lange 2017&nbsp;&bull;&nbsp; <a\r\n");
      out.write("\t\t\t\t\thref=\"http://www.diana-lange.de/content/impressum.html\"\r\n");
      out.write("\t\t\t\t\ttarget=\"_blank\">Imprint (German, external)</a>&nbsp;&bull;&nbsp;\r\n");
      out.write("\t\t\t\tColor theme found at <a\r\n");
      out.write("\t\t\t\t\thref=\"https://color.adobe.com/de/Kopi-af-vintage-card-color-theme-10067792/?showPublished=true\"\r\n");
      out.write("\t\t\t\t\ttarget=\"_blank\">Adobe Color (external)</a>\r\n");
      out.write("\t\t\t</p>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t</footer>\r\n");
      out.write("\t<script src=\"scripts/engineloader.js\"></script>\r\n");
      out.write("\r\n");
      out.write("</body>\r\n");
      out.write("</html>\r\n");
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try {
            if (response.isCommitted()) {
              out.flush();
            } else {
              out.clearBuffer();
            }
          } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
